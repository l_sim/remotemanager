{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8d1c1c4f-7302-4f0b-9070-8089e56c9638",
   "metadata": {},
   "source": [
    "# Changing the Transport\n",
    "\n",
    "Similar to how you are able to change the serialiser by importing a new one and passing it to Dataset, you are also able to do this with Transport.\n",
    "\n",
    "## Use Cases\n",
    "\n",
    "The main reason to swap transport, is if the default `rsync` does not work for your system. This can either be related to the remote machine, the connection, or an outdated version.\n",
    "\n",
    ".. important::\n",
    "    `remotemanager` requires `rsync --version >= 3.0.0`. MacOS devices may run an outdated version. To fix this, you can either update your install (slower, but permanent fix), or swap to `scp` (fast, but is required for each `Dataset`).\n",
    "\n",
    "Even if you have no issues, it is possible to customise the transport further by setting Flags directly. This is an alternative method to that shown in the [flags tutorial](./E5_Flags.html).\n",
    "\n",
    "## Importing\n",
    "Just like with `serialdill`, `serialjson`, etc., you may import from the available Transport methods:\n",
    "\n",
    "- `rsync`\n",
    "- `scp`\n",
    "- `cp`\n",
    "\n",
    "Of these, `cp` is less useful as it is unable to connect to external machines. It is provided for the edge case where you require no remote connection and the machine has no rsync or scp. And to provide a very simple template for [creating your own Transport.](./E7_Changing_Transport.html#Custom-Transport)\n",
    "\n",
    "To start, we can set up a run just as normal. The transport is a **drop in** replacement, having no effect on the Dataset other than the command that actually gets used to send/retrieve data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "b59c1150-3554-4285-8361-1bd0a73f8b1e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "21ca3108-6f4f-4cb8-8c16-bab5cfa2b06c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def function(x, y):\n",
    "    return x * y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1781b561-1a0d-492e-bf0a-3fb7b32c43c7",
   "metadata": {},
   "source": [
    "Since `rsync` is default, lets swap to `scp`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2bca56ae-6947-4f0c-91a3-9cf1004179d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager.transport import scp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "8ca8b367-ea21-462a-972c-7658c83715fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = Dataset(\n",
    "    function,\n",
    "    skip = False,\n",
    "    transport = scp(),  # new option!\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83b1b530-f673-4a50-9f80-28b04e3e21fa",
   "metadata": {},
   "source": [
    ".. note::\n",
    "    Like the serialiser, and URL, the transport object must be instantised (\"called\") at some point post-import."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "c1eec259-8b1f-41c9-9b91-3ce537594acd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "Running Dataset\n",
      "assessing run for runner dataset-991e1c92-runner-0... running\n",
      "Transferring 5 Files... Done\n"
     ]
    }
   ],
   "source": [
    "ds.append_run({\"x\": 21, \"y\": 2})\n",
    "ds.run()\n",
    "ds.wait(1, 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "8e5b69a7-0fb3-4b4e-99e1-3b9e52206b7d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 2 Files... Done\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[42]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.fetch_results()\n",
    "ds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f50738b6-cd16-47a4-907b-4fed8012c340",
   "metadata": {},
   "source": [
    "### Verification\n",
    "\n",
    "Right now, it looks like nothing has changed, we have to do some digging to see if it worked.\n",
    "\n",
    "A quick way is to check the `transport` property"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "94265582-0278-4d45-9318-209079b5ccd3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<remotemanager.transport.scp.scp at 0x706a7e176790>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.transport"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acf4297f-17d5-4c0e-90fb-c61c36825b00",
   "metadata": {},
   "source": [
    "That reads, `scp`, so it's the right module at least. But we want to see some *commands*. Lets search the cmd_history for commands containing `scp`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "178dce09-5d4f-440e-8f59-dbeedf5084f4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "scp -r /home/ljbeal/Work/Devel/remotemanager/docs/source/tutorials/temp_runner_local/{dataset-991e1c92-master.sh,dataset-991e1c92-repo.py,dataset-991e1c92-repo.sh,dataset-991e1c92-runner-0-jobscript.sh,dataset-991e1c92-runner-0-run.py} temp_runner_remote/\n"
     ]
    }
   ],
   "source": [
    "for cmd in ds.url.cmd_history:\n",
    "    if \"scp\" in cmd.sent:\n",
    "        print(cmd.sent)\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e74e2f0c-7003-4c74-8cbb-1c1dcb90e3bb",
   "metadata": {},
   "source": [
    "And there we have our first scp call, sending data from local to remtote dirs."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce18de3d-2d36-49d1-9c7e-098892501382",
   "metadata": {},
   "source": [
    "## Flags\n",
    "\n",
    "As mentioned at the top, it is possible to directly set the flags of the transport at the initialisation, using the `flags` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "e1dc6bfb-b344-4636-a03d-68cf95ab534e",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = Dataset(\n",
    "    function,\n",
    "    skip = False,\n",
    "    transport = scp(flags=\"-v\"),  # new option!\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "c2fdad28-c0f1-453d-8f1a-eaec9bd5fed6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-v"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.transport.flags"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc7cec0b-c019-4775-9536-26111ad689c6",
   "metadata": {},
   "source": [
    "## Custom Transport\n",
    "\n",
    "Just like with Serialiser, it is possible to create your own transport class.\n",
    "\n",
    "This can be done by subclassing the [transport module](../remotemanager.transport.transport.html) and adding the necessary overrides (usually just the `cmd` method).\n",
    "\n",
    "### `cmd`\n",
    "\n",
    "When overriding the `cmd` method of `Transport`, there is a pattern to follow.\n",
    "\n",
    "The docstring of the base level method explains this in detail. Found [here](../remotemanager.transport.transport.html#remotemanager.transport.transport.Transport.cmd).\n",
    "\n",
    "But in short, the function should return a valid command in string form, and accept two arguments `primary` and `secondary`. These are both strings.\n",
    "\n",
    "### `primary`\n",
    "\n",
    "This argument will come \"preformatted\" in bash-syntax. For example `directory_name/{file1,file2,file3,...,fileN}`"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
