{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "af374458-4590-432c-bf2e-22e300d3c8b3",
   "metadata": {},
   "source": [
    "# Deeper Customisation with Parser\n",
    "\n",
    "## Hidden Function\n",
    "\n",
    "The parser is the function that does the actual jobscript generation. It takes your resources, and spits out a list of lines that should go at the top of your script.\n",
    "\n",
    "Up until now, we have been secretly relying on the internal \"default\" parser. This works by iterating over the stored `Resource` objects, and then dumps them into a list according to the schema `\"{pragma}{tag}{flag}{separator}{value}\"`.\n",
    "\n",
    "..note::\n",
    "    `tag` and `separator` are configurable keyword args of `Resource`, defaulting to \"--\" and \"=\", respectively. You can also set them at the `Computer` level, as demonstrated below.\n",
    "\n",
    "But what if this doesn't work for your machine? It certainly doesn't work for PBS based machines.\n",
    "\n",
    "Well that's where we need to specify our own.\n",
    "\n",
    "This tutorial will explain the general concepts of defining a parser with the end goal of generating a PBS-friendly jobscript, but remember that these topics are general."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a5a3de1c-ee06-4eff-bbfb-631b25ac62af",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager.connection.computers import BaseComputer, Resource\n",
    "\n",
    "class Computer(BaseComputer):\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "\n",
    "        self.submitter = \"sbatch\"\n",
    "        self.shebang = \"#!/bin/bash\"\n",
    "        self.pragma = \"#SBATCH\"\n",
    "\n",
    "        self.mpi = Resource(name=\"mpi\", flag=\"ntasks\", min=1)\n",
    "        self.omp = Resource(name=\"omp\", flag=\"cpus-per-task\", min=1, max=64)\n",
    "        self.nodes = Resource(name=\"nodes\", flag=\"nodes\", optional=False)\n",
    "        self.time = Resource(name=\"time\", flag=\"walltime\", optional=False, format=\"time\", default=3600)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "6cc6d9c6-4cc1-4f33-adde-582a4448a777",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "#!/bin/bash\n",
      "#SBATCH --nodes=1\n",
      "#SBATCH --walltime=01:00:00\n",
      "\n"
     ]
    }
   ],
   "source": [
    "test = Computer()\n",
    "\n",
    "test.nodes = 1\n",
    "\n",
    "print(test.script())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce667f9f-613a-4c2f-8989-cf2bc3043838",
   "metadata": {},
   "source": [
    "In PBS based parsers, the actual resource line is expected to be something that follows the form:\n",
    "\n",
    "`#PBS -l nodes=1:ppn=4,walltime=01:00:00`\n",
    "\n",
    "Additionally, `OMP` is usually specified by an environment variable.\n",
    "\n",
    "With our default behaviour of putting one resource per line, this is obviously not going to work. \n",
    "\n",
    "So we need to specify our own `parser`. Lets begin by explicitly defining the _current_ behaviour:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ecf42097-2bf5-48b7-9475-59cc1f8d3fa3",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Computer(BaseComputer):\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "\n",
    "        self.submitter = \"sbatch\"\n",
    "        self.shebang = \"#!/bin/bash\"\n",
    "        self.pragma = \"#SBATCH\"\n",
    "        \n",
    "        self.mpi = Resource(name=\"mpi_per_node\", flag=\"ppn\", default=4)\n",
    "        self.omp = Resource(name=\"omp\", flag=\"cpus-per-task\", default=4)\n",
    "        self.nodes = Resource(name=\"nodes\", flag=\"nodes\", default=1)\n",
    "        self.time = Resource(name=\"time\", flag=\"walltime\", format=\"time\", optional=False, default=3600)\n",
    "\n",
    "    def parser(self, resources: \"Resources\") -> list:\n",
    "        output = []\n",
    "        for resource in resources:\n",
    "            if resource:\n",
    "                output.append(resource.resource_line)\n",
    "\n",
    "        return output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "6258cdb8-1659-4179-9393-d3030f287888",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "#!/bin/bash\n",
      "#SBATCH --ppn=4\n",
      "#SBATCH --cpus-per-task=4\n",
      "#SBATCH --nodes=1\n",
      "#SBATCH --walltime=01:00:00\n",
      "\n"
     ]
    }
   ],
   "source": [
    "test = Computer()\n",
    "print(test.script())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28a03069-dfe2-43e8-a070-ac2fadde9bdc",
   "metadata": {},
   "source": [
    "### Parser \"gotchas\"\n",
    "\n",
    "Theres a few things to note here.\n",
    "\n",
    "#### `resources`\n",
    "\n",
    "The `resources` keyword accepts a `Resources` object. As you'd imagine, this is a special carrier object that holds our `Resource` objects.\n",
    "\n",
    ".. note::\n",
    "    Despite the similar name, `Resources` and `Resource` are not the same object. The former is a collection of the latter.\n",
    "\n",
    "It functions like a list, with some extra functions. The important thing to note is that you can iterate over it, just like a list.\n",
    "\n",
    "`for resource in resource:` will give you each resource, one by one.\n",
    "\n",
    ".. note::\n",
    "    You can also access a `Resource` by its name, like you would a dictionary. See the [section below](legacy/L2_Parsers.html#Accessing-Resources) for info."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "231b7df6-be6c-474e-a480-c6791ad5be20",
   "metadata": {},
   "source": [
    "#### `if resource`\n",
    "\n",
    "`bool(resource)` will evaluate to `True` if the resource has both `value` _and_ `flag`. This `if resource` line prevents resources without a value being added to the jobscript.\n",
    "\n",
    "Omitting this usually results in a line like `#SBATCH --ntasks=None`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "9d83218b-0819-4104-bf63-0ed4a7947905",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A resource without a value will evaluate to False: False\n",
      "When given a value, it will then evaluate to True: True\n"
     ]
    }
   ],
   "source": [
    "a = Resource(name=\"a\", flag=\"resource\")\n",
    "print(\"A resource without a value will evaluate to False:\", bool(a))\n",
    "\n",
    "a.value = \"test\"\n",
    "print(\"When given a value, it will then evaluate to True:\", bool(a))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "92b8bb78-5c39-4aca-ad04-c18d68974cd5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A resource without a flag will always evaluate to False: False\n",
      "When given a value, it will still evaluate to False: False\n"
     ]
    }
   ],
   "source": [
    "b = Resource(name=\"a\")\n",
    "print(\"A resource without a flag will always evaluate to False:\", bool(b))\n",
    "\n",
    "b.value = \"test\"\n",
    "print(\"When given a value, it will still evaluate to False:\", bool(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f17a2969-44fc-40e7-ae41-f5d4a2286047",
   "metadata": {},
   "source": [
    "#### return type\n",
    "\n",
    "The `parser` should always return a `list` of lines, _not_ a string.\n",
    "\n",
    "### Making our PBS parser\n",
    "\n",
    "Okay now we've got that down, we can start thinking about how to edit our parser to work as we want.\n",
    "\n",
    "Since we want to combine the resources `mpi_per_node`, `nodes` and `time` into a single line, we should exclude those in the main loop.\n",
    "\n",
    "Lets also change the format to the right one too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "5949dcff-45a2-476f-87b3-9d0945e3e544",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "#!/bin/bash\n",
      "\n"
     ]
    }
   ],
   "source": [
    "class Computer(BaseComputer):\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "        # change our submitter and pragma to the right values\n",
    "        self.submitter = \"qsub\"\n",
    "        self.shebang = \"#!/bin/bash\"\n",
    "        self.pragma = \"#PBS\"\n",
    "        # PBS also uses a different format for the lines, lets set that too\n",
    "        self.resource_tag = \"-\"\n",
    "        self.resource_separator = \" \"\n",
    "        # same resources, we're changing the FORMAT, not the CONTENT\n",
    "        self.mpi = Resource(name=\"mpi_per_node\", flag=\"ppn\", default=4)\n",
    "        self.omp = Resource(name=\"omp\", flag=\"cpus-per-task\", default=4)\n",
    "        self.nodes = Resource(name=\"nodes\", flag=\"nodes\", default=1)\n",
    "        self.time = Resource(name=\"time\", flag=\"walltime\", format=\"time\", optional=False, default=3600)\n",
    "\n",
    "    def parser(self, resources: \"Resources\") -> list:\n",
    "        output = []\n",
    "        for resource in resources:\n",
    "            # exclude by name, we need to treat these separately\n",
    "            if resource and resource.name not in [\"mpi_per_node\", \"nodes\", \"time\", \"omp\"]:\n",
    "                output.append(resource.resource_line)\n",
    "\n",
    "        return output\n",
    "\n",
    "test = Computer()\n",
    "print(test.script())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b74a903e-3619-4fd9-98b3-8527e8f86a3c",
   "metadata": {},
   "source": [
    "That looks good, now there's no breaking lines in the output.\n",
    "\n",
    ".. important::\n",
    "    It looks like the loop is doing \"nothing\", since we're excluding everything that we've added. However it's still good practice to add it anyway, since it will catch any extra resources you choose to add in the future.\n",
    "\n",
    "Now we've cleaned the output, we can form the line we want and add it how we like.\n",
    "\n",
    "Lets additionally grab the `omp` resource and export its value.\n",
    "\n",
    "Oh, and add the `cd $PBS_O_WORKDIR` line for good measure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "e6972fed-1718-4a8a-9903-6b06e838d09b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "#!/bin/bash\n",
      "#PBS -cpus-per-task 4\n",
      "#PBS -l nodes=1:ppn=4,walltime=01:00:00\n",
      "\n",
      "cd $PBS_O_WORKDIR\n",
      "export OMP_NUM_THREADS=4\n",
      "\n"
     ]
    }
   ],
   "source": [
    "class Computer(BaseComputer):\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "\n",
    "        self.submitter = \"qsub\"\n",
    "        self.shebang = \"#!/bin/bash\"\n",
    "        self.pragma = \"#PBS\"\n",
    "\n",
    "        self.resource_tag = \"-\"\n",
    "        self.resource_separator = \" \"\n",
    "        \n",
    "        self.mpi = Resource(name=\"mpi_per_node\", flag=\"ppn\", default=4)\n",
    "        self.omp = Resource(name=\"omp\", flag=\"cpus-per-task\", default=4)\n",
    "        self.nodes = Resource(name=\"nodes\", flag=\"nodes\", default=1)\n",
    "        self.time = Resource(name=\"time\", flag=\"walltime\", format=\"time\", optional=False, default=3600)\n",
    "\n",
    "    def parser(self, resources: \"Resources\") -> list:\n",
    "        output = []\n",
    "        for resource in resources:\n",
    "            if resource and resource.name not in [\"mpi_per_node\", \"nodes\", \"time\"]:\n",
    "                output.append(resource.resource_line)\n",
    "        # extract the values and format them before adding\n",
    "        ppn = resources[\"mpi_per_node\"]\n",
    "        nodes = resources[\"nodes\"]\n",
    "        wtime = resources[\"time\"]\n",
    "        output.append(\n",
    "            f\"{resources.pragma} -l nodes={nodes}:\"\n",
    "            f\"ppn={ppn},\"\n",
    "            f\"walltime={wtime}\"\n",
    "        )\n",
    "        # We can add extra important lines in here, too\n",
    "        output.append(\"\\ncd $PBS_O_WORKDIR\")\n",
    "        output.append(f\"export OMP_NUM_THREADS={resources['omp']}\")\n",
    "\n",
    "        return output\n",
    "\n",
    "test = Computer()\n",
    "print(test.script())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dbbece32-38b8-4d82-a45c-fe82800f61fc",
   "metadata": {},
   "source": [
    ".. tip::\n",
    "    You can also use this to add resource lines that need no value. `#SBATCH --exclusive` for slurm, for example."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c37e3c9d-3f4e-457c-8909-65a32ee1d26d",
   "metadata": {},
   "source": [
    "### Accessing Resources\n",
    "\n",
    "While the `Resources` object functions primarily as a `list`, you can also access the `Resource` objects like you would a dict.\n",
    "\n",
    "To demonstrate this, lets go back to a simpler parser format for the sake of brevity.\n",
    "\n",
    "..note::\n",
    "    The key must be that of the `name` parameter, not the actual assignment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "6d9d6771-9d86-4849-83e1-863256ccf13c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "the value of the 'mpi' arg is 16\n",
      "#!/bin/bash\n",
      "#PBS --ppn=16\n",
      "#PBS --cpus-per-task=4\n",
      "#PBS --nodes=1\n",
      "#PBS --walltime=01:00:00\n",
      "\n"
     ]
    }
   ],
   "source": [
    "class Computer(BaseComputer):\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "\n",
    "        self.submitter = \"qsub\"\n",
    "        self.shebang = \"#!/bin/bash\"\n",
    "        self.pragma = \"#PBS\"\n",
    "        \n",
    "        self.mpi = Resource(name=\"mpi_per_node\", flag=\"ppn\", default=4)\n",
    "        self.omp = Resource(name=\"omp\", flag=\"cpus-per-task\", default=4)\n",
    "        self.nodes = Resource(name=\"nodes\", flag=\"nodes\", default=1)\n",
    "        self.time = Resource(name=\"time\", flag=\"walltime\", format=\"time\", optional=False, default=3600)\n",
    "\n",
    "    def parser(self, resources: \"Resources\") -> list:\n",
    "        output = []\n",
    "        for resource in resources:\n",
    "            if resource:\n",
    "                output.append(resource.resource_line)\n",
    "\n",
    "        print(f\"the value of the 'mpi' arg is {resources['mpi_per_node']}\")\n",
    "\n",
    "        return output\n",
    "\n",
    "test = Computer()\n",
    "test.mpi = 16\n",
    "print(test.script())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0212e1d3-62bc-44eb-b754-e01893261e2a",
   "metadata": {},
   "source": [
    "### run_args\n",
    "\n",
    "You can also access the `run_args` of the calling `Dataset` from within `resources`.\n",
    "\n",
    "This is useful for extracting info such as the remote directory. For this we should create a Dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "42c76ce8-7509-4c85-aa07-eff58b565b8f",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "Running Dataset\n",
      "assessing run for runner dataset-06a84b6d-runner-0... running\n",
      "launch command: cd temp_runner_remote && rm -f dataset-06a84b6d.manifest && sed -i -e \"s#{rootdir}#$(pwd)#\" dataset-06a84b6d-repo.sh && source dataset-06a84b6d-repo.sh && exec_and_log bash dataset-06a84b6d-master.sh\n",
      "\n",
      "jobscript:\n",
      "#!/bin/bash\n",
      "#PBS --ppn=4\n",
      "#PBS --cpus-per-task=4\n",
      "#PBS --nodes=1\n",
      "#PBS --walltime=01:00:00\n",
      "export WORKDIR=temp_runner_remote\n",
      "\n",
      "\n",
      "export DIR_e711be1e={run_rootdir}\n",
      "source {run_rootdir}/dataset-06a84b6d-repo.sh\n",
      "\n",
      "exec_and_log python dataset-06a84b6d-runner-0-run.py || write_to_log failed\n",
      "\n"
     ]
    }
   ],
   "source": [
    "from remotemanager import Dataset\n",
    "\n",
    "class Computer(BaseComputer):\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "\n",
    "        self.submitter = \"qsub\"\n",
    "        self.shebang = \"#!/bin/bash\"\n",
    "        self.pragma = \"#PBS\"\n",
    "        \n",
    "        self.mpi = Resource(name=\"mpi_per_node\", flag=\"ppn\", default=4)\n",
    "        self.omp = Resource(name=\"omp\", flag=\"cpus-per-task\", default=4)\n",
    "        self.nodes = Resource(name=\"nodes\", flag=\"nodes\", default=1)\n",
    "        self.time = Resource(name=\"time\", flag=\"walltime\", format=\"time\", optional=False, default=3600)\n",
    "\n",
    "    def parser(self, resources: \"Resources\") -> list:\n",
    "        output = []\n",
    "        for resource in resources:\n",
    "            if resource:\n",
    "                output.append(f\"{resources.pragma} --{resource.flag}={resource.value}\")\n",
    "\n",
    "        output.append(f\"export WORKDIR={resources['run_args']['remote_dir']}\")\n",
    "\n",
    "        return output\n",
    "        \n",
    "\n",
    "def f():\n",
    "    return\n",
    "\n",
    "ds = Dataset(f, url = Computer(), skip = False, mpi_per_node=32)\n",
    "\n",
    "ds.append_run()\n",
    "\n",
    "ds.run(dry_run=True)\n",
    "\n",
    "print(\"\\njobscript:\")\n",
    "print(ds.runners[0].jobscript.content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0108f773-6744-48fa-8adf-4f4e10aeb514",
   "metadata": {},
   "source": [
    "The the `run_args` of the Dataset are available at `run_args` as a dict. \n",
    "\n",
    "It may be safer in this case to use a `get(..., None)` as if the arg is not present it will cause your parser to Fail."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
