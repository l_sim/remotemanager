{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quickstart\n",
    "\n",
    "This notebook will cover all the basic and most useful functionality available to get a user up and running as fast as possible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Installation\n",
    "\n",
    "Installation can be done via a pip install:\n",
    "\n",
    "`pip install remotemanager` for the most recent stable version.\n",
    "\n",
    "However if you would like the bleeding edge version, you can clone the `devel` branch of the git [repository](https://gitlab.com/l_sim/remotemanager):\n",
    "\n",
    "`git clone --branch devel && pip install remotemanager`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Function Definition\n",
    "\n",
    "`remotemanager` executes user defined python functions at the location of choice. Below is a basic function example which will serve our purposes for this guide.\n",
    "\n",
    ".. important::\n",
    "    The function must stand by itself when running, so any imports or necessary functionality should be contained within."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def multiply(a, b):\n",
    "    import time\n",
    "    \n",
    "    time.sleep(1)\n",
    "    \n",
    "    return a * b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running Remotely\n",
    "\n",
    "This function would run just fine on any workstation, but to run something more complex we would need to connect to some more powerful resources for this.\n",
    "\n",
    "`remotemanager` provides the powerful `Computer` module for this purpose:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Computer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we must define a \"template\". This is the base from which a submission script will be generated.\n",
    "\n",
    "The easiest way to create one of these templates, is to acquire a jobscript that you know works for your machine. A few suggestions for this:\n",
    "\n",
    "- Machine documentation may have an example script to build from (or even a configurator!)\n",
    "- If you have already run jobs, your own scripts should suffice, otherwise a colleague may have a example for you\n",
    "- The helpdesk may be able to assist you in creating a jobscript for your use case\n",
    "\n",
    "In this example, we will be taking an existing jobscript that we know works.\n",
    "\n",
    "We will also parameterise just a single option, `#username#`. This syntax allows Computer to provide a \"dynamic\" input that can be changed.\n",
    "\n",
    "The basic syntax for parameterisation is that anything between double `#hashes#` will be treated as a parameter and added to the computer. Here, for example, a variable called \"hashes\" would be created.\n",
    "\n",
    ".. important::\n",
    "    Parameters will be sanitised to all lowercase. Therefore `#ARG#` == `#arg#`.\n",
    "\n",
    ".. important::\n",
    "    Parameters must not clash with internal names, an error will be raised in this case. For example, we have to choose `#username#` here instead of `#user#`, since `user` is already an internal argument.\n",
    "\n",
    ".. note::\n",
    "    This is covered in greater detail in the dedicated [tutorial](./C2_Templates.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "template = \"\"\"#!/bin/bash\n",
    "#SBATCH --nodes=1\n",
    "#SBATCH --ntasks-per-node=8\n",
    "#SBATCH --cpus-per-task=4\n",
    "#SBATCH --time=00:30:00\n",
    "\n",
    "#SBATCH --job-name=quickstart\n",
    "\n",
    "#SBATCH --account=#username#\n",
    "#SBATCH --partition=boost_usr_prod\n",
    "#SBATCH --qos=normal\n",
    "\n",
    "export OMP_NUM_THREADS=4\n",
    "\n",
    "module load python/3.10.8--gcc--11.3.0\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, create a Computer. At a minimum you should specify:\n",
    "\n",
    "- Host address (or `userhost=user@host`)\n",
    "- The submitter that your job system uses. (defaults to `bash`, which will run on the login node)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "connection = Computer(\n",
    "    user=\"user\", \n",
    "    host='remote.hpc.url', \n",
    "    submitter=\"sbatch\", \n",
    "    template=template\n",
    ")\n",
    "\n",
    "# note that template arguments must be specified after initialisation\n",
    "connection.username = \"myuser\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "editable": true,
    "nbsphinx": "hidden",
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "template = \"\"\"#!/bin/bash\n",
    "#SBATCH --nodes=1\n",
    "#SBATCH --ntasks-per-node=8\n",
    "#SBATCH --cpus-per-task=4\n",
    "#SBATCH --time=00:30:00\n",
    "\n",
    "#SBATCH --job-name=quickstart\n",
    "\n",
    "#SBATCH --account=#username#\n",
    "#SBATCH --partition=boost_usr_prod\n",
    "#SBATCH --qos=normal\n",
    "\n",
    "export OMP_NUM_THREADS=4\n",
    "\n",
    "# module load python/3.10.8--gcc--11.3.0\n",
    "\"\"\"\n",
    "\n",
    "connection = Computer(host='localhost', template=template)\n",
    "\n",
    "connection.username = \"myuser\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example connection is pointed at an imaginary `user@remote.hpc.url`. However, this uses your `ssh` configuration, so you are able to connect to a machine in the same way that you would from a command line.\n",
    "\n",
    "For example, if there existed a machine which you connected to with `ssh machine`, then you are able to create a computer using:\n",
    "\n",
    "`connection = Computer(\"machine\")`\n",
    "\n",
    ".. important::\n",
    "    `Computer` requires that you are able to ssh into the remote machine without any additional prompts from the remote. For connection difficulties regarding permssions, see the [relevant section](../Introduction.html#Connecting-to-a-Remote-Machine) of the introduction.\n",
    "\n",
    ".. tip::\n",
    "    The connection parameters inherit those from your ssh config. So if you are able to `ssh <host>`, you can create a `Computer` with `Computer(\"<host>\")`.\n",
    "\n",
    ".. tip::\n",
    "    Before using `Computer` for the first time on a machine, any immediate problems can be discovered by testing a basic command. Start with a simple `ssh user@remote \"ls\"` and see what comes back. If the terminal returns a sensible output without prompting for a password, a `Computer` should function as expected."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a connection ready to go, we can see an example of the script that would be produced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "#!/bin/bash\n",
      "#SBATCH --nodes=1\n",
      "#SBATCH --ntasks-per-node=8\n",
      "#SBATCH --cpus-per-task=4\n",
      "#SBATCH --time=00:30:00\n",
      "\n",
      "#SBATCH --job-name=quickstart\n",
      "\n",
      "#SBATCH --account=myuser\n",
      "#SBATCH --partition=boost_usr_prod\n",
      "#SBATCH --qos=normal\n",
      "\n",
      "export OMP_NUM_THREADS=4\n",
      "\n",
      "# module load python/3.10.8--gcc--11.3.0\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(connection.script())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Remote Commands\n",
    "\n",
    "With the concept of this remote `connection`, we can excecute commands and (more importantly) our function on this machine.\n",
    "\n",
    "For commands, url provides a `cmd` method, which will execute any strings given"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "this command is executed on the remote"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "connection.cmd('echo \"this command is executed on the remote\"')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running Functions\n",
    "\n",
    "For function execution, we require a `Dataset`. \n",
    "\n",
    ".. note::\n",
    "    Think a `Dataset` as a container for a _function_.\n",
    "\n",
    "Like `URL`, this can be imported directly from `remotemanager`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To create a dataset, pass your function to the `Dataset` constructor.\n",
    "\n",
    ".. note::\n",
    "    When passing a function to the dataset, do not call it within the assigment. For example, call `Dataset(function=multiply)` _not_ `Dataset(function=multiply())`\n",
    "\n",
    "Here we are additionally specifying the `local_dir` and the `remote_dir`, which tells the Dataset where to put all relevant files on the local and remote machines, respectively.\n",
    "\n",
    ".. note::\n",
    "    We will use `skip=False` in the Dataset creation, otherwise the `Dataset` will see the dataset we previously created and import its data rather than create itself anew."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = Dataset(function=multiply,\n",
    "             url=connection,\n",
    "             local_dir='temp_local',  # Location where files will be \"staged\", before sending to the remote\n",
    "             remote_dir='temp_remote', # Location on the remote server where the run will be executed\n",
    "             skip=False\n",
    "            )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ".. important::\n",
    "    This dataset has no runs, as it is just a container for the function `multiply`. For this, we must add runners.\n",
    "\n",
    "## Creating runs\n",
    "\n",
    "To add runs, we use the `Dataset.append_run()` method. This will take the arguments in `dict` format, and store them for later.\n",
    "\n",
    "You may do this in any way you see fit, the important part is to pass a dictionary which contains all ncessary arguments for the running of your function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n",
      "appended run runner-2\n"
     ]
    }
   ],
   "source": [
    "runs = [[21, 2],\n",
    "        [64, 8],\n",
    "        [10, 7]]\n",
    "\n",
    "for run in runs:\n",
    "    \n",
    "    a = run[0]\n",
    "    b = run[1]\n",
    "    \n",
    "    arguments = {'a': a, 'b': b}\n",
    "    \n",
    "    ds.append_run(arguments=arguments)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running and Retrieving your results\n",
    "\n",
    "Now we have created a dataset and appended some runs, we can launch the calculations. This is done via the Dataset.run() method\n",
    "\n",
    "Once the runs have completed, you can retrieve your results with `ds.fetch_results()`, and access them via `ds.results` once this is done\n",
    "\n",
    ".. important::\n",
    "    `fetch_results()` does not _return_ your results, but collects the files and stores them within the runners."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running Dataset\n",
      "assessing run for runner dataset-62eb4971-runner-0... running\n",
      "assessing run for runner dataset-62eb4971-runner-1... running\n",
      "assessing run for runner dataset-62eb4971-runner-2... running\n",
      "Transferring 8 Files... Done\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Wait\n",
    "\n",
    "Calculations can take time, we can add an optional `wait` call here to await the dataset completion.\n",
    "\n",
    "The first number is the check `interval`, the second is the maximum wait time (set to `None` for an indefinite wait)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.wait(1, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the run has completed, we must fetch the results before they are made available:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 6 Files... Done\n"
     ]
    }
   ],
   "source": [
    "ds.fetch_results()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Results have been fetched from the remote, now we can access them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[42, 512, 70]\n"
     ]
    }
   ],
   "source": [
    "print(ds.results)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[None, None, None]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.errors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this, you have all of the basic tools available to run python functions on a remote machine. See the other tutorials for more advanced usage\n",
    "\n",
    ".. warning::\n",
    "    Be aware that on MacOS, you may receive some errors when transferring data. This is most likely due to MacOS natively using an old `rsync` version (<3.0.0). More information is available on [this page](./E7_Changing_Transport.html)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
