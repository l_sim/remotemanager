{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5f969cc3-70ce-4260-a3a6-b02db348560c",
   "metadata": {},
   "source": [
    "# Decorators\n",
    "\n",
    "## Intro to Decorators\n",
    "\n",
    "Decorators are a feature of python that allows extra functionality to be added to objects. A simple example of this is a decorator which formats the output for you"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "493037ea-9f16-4688-bfe8-d825dbb0d725",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"The result is: '28'\""
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# actual decorator name, takes a function as an argument\n",
    "def format_to_string(func):\n",
    "\n",
    "    # internal decorator parser, this does the \"work\"\n",
    "    def formatter(*args, **kwargs):\n",
    "        # create this as a simple parser, returning the result in a string\n",
    "        return f\"The result is: '{func(*args, **kwargs)}'\"\n",
    "\n",
    "    return formatter\n",
    "\n",
    "# the @ syntax calls our function as a decorator\n",
    "@format_to_string\n",
    "def multiply(x, y):\n",
    "    return x * y\n",
    "\n",
    "# lets see what it does to the output\n",
    "multiply(4, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6a8a9e9-88e8-4fa6-b183-b9f6bef7e0bc",
   "metadata": {},
   "source": [
    "This example shows a basic use of a decorator. Python has internal decorators, a good example is the `@classmethod` which is used to modify a class method to return an instance of that `class`.\n",
    "\n",
    "Likewise, `remotemanager` also provides some decorators for usage:\n",
    "\n",
    "- `RemoteFunction` allows users to \"tag\" extra functions to be brought along with a main Dataset\n",
    "- `SanzuFunction` operates similar to [sanzu](D2_Jupyter_Magic.html), but can be used directly within scripts\n",
    "\n",
    "## RemoteFunction\n",
    "\n",
    "It is a fundamental idea of programming to convert repetitive sections of code into functions, turning whole blocks into single lines. \n",
    "\n",
    "Lets imagine a two stage workflow for processing a number:\n",
    "\n",
    "#. input is processed according to the rules: if num > 100 -> x0.5, else x2\n",
    "#. format the output into a string format for easy reading\n",
    "\n",
    ".. note::\n",
    "    This is an extremely basic example, designed to show the limitations of a base `Dataset`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "d1189b90-503e-41f8-9739-a76bb7044e22",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The result is 100.0\n"
     ]
    }
   ],
   "source": [
    "num = 200\n",
    "if num > 100:\n",
    "    num = num / 2\n",
    "else:\n",
    "    num = num * 2\n",
    "    \n",
    "num = float(num)\n",
    "print('The result is', num)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cf69991-5314-49da-90db-c4365edcbc89",
   "metadata": {},
   "source": [
    "### Using Functions\n",
    "\n",
    "While this _works_, it does not scale well to large sets of inputs. A standard way of increasing the scalability is to define functions which can be called on any inputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "9fbc8425-01cf-4459-a07b-eb6031c6eb77",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The result is 148.0\n"
     ]
    }
   ],
   "source": [
    "# define the formatter\n",
    "def formatted(temp):\n",
    "    return f'The result is {float(temp)}'\n",
    "\n",
    "# and now the processing function\n",
    "def process(number):\n",
    "    if number > 100:\n",
    "        return formatted(number / 2)\n",
    "    return formatted(number * 2)\n",
    "\n",
    "print(process(74))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3297987-dcba-433e-aabc-8eff338d2ae5",
   "metadata": {},
   "source": [
    "### Single Function Limits\n",
    "\n",
    "It is here where we run into an issue with remotemanager as we can only define a single function for the Dataset to hold.\n",
    "\n",
    "There are two paths we can take here using native python:\n",
    "\n",
    "1. Refactor the workflow to be contained within a single function\n",
    "2. Use an inner function\n",
    "\n",
    "Obviously refactoring here is trivial to do, however that approach can get cumbersome very quickly with even small increases in complexity. Lets start with option 2, inner functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "6744a185-e57c-4191-9a01-efaff6a8358f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The result is 100.0!\n",
      "The result is 84.0!\n"
     ]
    }
   ],
   "source": [
    "def process(number):\n",
    "    \"\"\"\n",
    "    This function halves any number above 100, \n",
    "    doubling otherwise.\n",
    "    \"\"\"\n",
    "    \n",
    "    def formatted(temp):\n",
    "        return f'The result is {float(temp)}!'\n",
    "    \n",
    "    if number > 100:\n",
    "        return formatted(number / 2)\n",
    "    return formatted(number * 2)\n",
    "\n",
    "print(process(200))\n",
    "print(process(42))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0bf7261-afc5-40a5-bee2-51eb254550fa",
   "metadata": {},
   "source": [
    "### The Third Option\n",
    "\n",
    "Obviously this tutorial wouldn't exist if there wasn't some way around this limitation so `remotemanager` takes this a step further and gives you a third option: Allowing you to mark extra functions for sending. These functions are added in addition to the one placed within `Dataset`. For this we use the `RemoteFunction` decorator.\n",
    "\n",
    "This is useful in a situation where you have multiple datasets holding different functions, but want a single formatting function for all jobs, for example.\n",
    "\n",
    "For this workflow, we would be adding `process` to the `Dataset`, which means we should also indicate that we need `formatted` in this workflow:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a794c642-afda-4756-b3b6-3deee3cb3739",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The result is 100.0!\n",
      "The result is 84.0!\n"
     ]
    }
   ],
   "source": [
    "from remotemanager import Dataset, URL, RemoteFunction\n",
    "\n",
    "@RemoteFunction\n",
    "def formatted(temp):\n",
    "        return f'The result is {float(temp)}!'\n",
    "    \n",
    "def process(number):\n",
    "    if number > 100:\n",
    "        return formatted(number / 2)\n",
    "    return formatted(number * 2)\n",
    "\n",
    "print(process(200))\n",
    "print(process(42))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "23be1332-ae21-444b-9c96-9a7b3d5e62dd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n"
     ]
    }
   ],
   "source": [
    "url = URL()\n",
    "\n",
    "ds = Dataset(process,\n",
    "             url = url,\n",
    "             skip = False)\n",
    "\n",
    "ds.append_run({'number': 200})\n",
    "ds.append_run({'number': 42})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "7339cc68-43d0-4661-9ba2-102df1bea8f3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running Dataset\n",
      "assessing run for runner dataset-7d7936c0-runner-0... running\n",
      "assessing run for runner dataset-7d7936c0-runner-1... running\n",
      "Transferring 6 Files... Done\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 2 Files... Done\n",
      "['The result is 100.0!', 'The result is 84.0!']\n"
     ]
    }
   ],
   "source": [
    "ds.run()\n",
    "ds.wait(2)\n",
    "ds.fetch_results()\n",
    "print(ds.results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4b990ce-18ac-4595-b1df-5b0b71ff0fe2",
   "metadata": {},
   "source": [
    "### Expandability\n",
    "\n",
    "You are not limited to a single extra function, so go wild! All stored functions are also available to _all_ Datasets within the notebook in which they are defined, so you can further reduce boilerplate code in complex workflows where function sharing would be beneficial.\n",
    "\n",
    ".. warning::\n",
    "    The functions that are cached are not stored within the databases themselves, so must always be defined within your notebook.\n",
    "\n",
    "## SanzuFunction\n",
    "\n",
    ".. versionadded:: 0.10.9\n",
    "\n",
    "Similar to [sanzu](D2_Jupyter_Magic.html), however instead of executing a cell, you can designate a function to be remote callable.\n",
    "\n",
    ".. note::\n",
    "    You can pass any `run_args` you may need directly to the decorator, and they will be attributed to the `Dataset` that is created."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "2c14c4f5-7945-4303-824d-beeee829b168",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "Running Dataset\n",
      "assessing run for runner dataset-776c1365-runner-0... running\n",
      "Transferring 4 Files... Done\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 1 File... Done\n",
      "90\n"
     ]
    }
   ],
   "source": [
    "from remotemanager import URL, SanzuFunction\n",
    "\n",
    "url = URL(\"localhost\")\n",
    "\n",
    "@SanzuFunction(url=url)\n",
    "def execute_remotely(x, y):\n",
    "    return x * y\n",
    "\n",
    "print(execute_remotely(x=10, y=9))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcbe39fe-378c-441a-93c0-27a86e87bb88",
   "metadata": {},
   "source": [
    "### Choosing a Remote\n",
    "\n",
    "Passing a `URL` at the decorator level will \"bake in\" that url to the function, causing it to always be called on that remote. Without passing a url, however, the Dataset will use its `default_url` property if set.\n",
    "\n",
    ".. note::\n",
    "    To set the `default_url` property you can import `Dataset` then update with `Dataset.default_url = URL(...)`\n",
    "\n",
    ".. versionadded:: 0.10.10\n",
    "Now SanzuFunctions can be called with non keyword args."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "8b1884b8-760f-4c39-b737-adf8c6d69c86",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-1\n",
      "Running Dataset\n",
      "assessing run for runner dataset-776c1365-runner-1... running\n",
      "Transferring 4 Files... Done\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 1 File... Done\n",
      "21\n"
     ]
    }
   ],
   "source": [
    "print(execute_remotely(7, 3))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
