{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6d9cf290-dd20-4d6a-9c44-82ea55eaf377",
   "metadata": {},
   "source": [
    "# Run Args\n",
    "\n",
    "## meta arguments\n",
    "\n",
    "Now you have a concept of the `Function`, `Dataset` and `Runners`, it's time to talk about `run_args`. These are \"extra\" arguments that go alongside your function, but do not interact directly with it.\n",
    "\n",
    "This can create some confusing terminology, so lets be explicit. Whenever `args` are discussed, this is referring to the actual `Function` arguments, i.e. what the `Runner` is storing. `run_args` deal with things like the remote directory and resource requests."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e826743a-a7ef-4d37-9f1f-811644237a49",
   "metadata": {},
   "source": [
    "## Native Arguments\n",
    "\n",
    "Here we will cover the `run_args` that are natively understood by a run. \n",
    "\n",
    "While you can implement your own functionality (more on that later), the following arguments are common to all runs.\n",
    "\n",
    "### Skip & Force\n",
    "\n",
    "This is a special \"contextual\" run arg. There are three situations where there args are relevant. \n",
    "\n",
    "#### Dataset init\n",
    "\n",
    "By default, when defining `Dataset(...)`, a search is done to see if a matching Dataset has already been created. If this is the case, the current creation will be \"skipped\", and the Dataset will instead be unpacked from the previous state.\n",
    "\n",
    "Setting `skip=False` will ensure a new `Dataset` is created, deleting the old database in the process.\n",
    "\n",
    "`force=True` is ignored here, only `skip` has any function.\n",
    "\n",
    ".. note::\n",
    "    It is advised to use `Dataset(..., skip=False)` while testing, as it ensures consistent behaviour. Only once you care about the result should you drop this argument (or change it to `True`).\n",
    "\n",
    "#### Run append\n",
    "\n",
    "Any `Runner` that already exists cannot be added to a `Dataset`. \n",
    "\n",
    "With `skip=False` runner will be appended anyway. This does not overwrite the existing runner, and allows for multiple copies of the same run.\n",
    "\n",
    "`force=True` acts as an inverted alias of `skip`. i.e. `skip=False` == `force=True`\n",
    "\n",
    "#### Run()\n",
    "\n",
    "When running a `Dataset`, `is_finished` is called to get the states of any runners. Any that are already running or have completed will not be submitted.\n",
    "\n",
    "`skip=False` allows runners which are already submitted to be resubmitted\n",
    "\n",
    "In general `force=True` functions as an inverted alias of `skip`. However there is an additional keyword argument `force_ignores_success` which is required to resubmit runners considered as \"succeeded\". This is an extra safeguard against overwriting data.\n",
    "\n",
    ".. important::\n",
    "    `force_ignores_success` is required for `skip=False`/`force=True` to function on runners which are considered to have succeeded. This is a runner which has successfully returned a result file.\n",
    "\n",
    "### Dirs\n",
    "\n",
    "The most commonly set run_args are the `*_dir` family. These designate where your run files will end up and it is recommended to change these from defaults when doing a full run. `remotemanger` can create a lot of small files, which can make directory navigation cumbersome, even with proper segmentation.\n",
    "\n",
    "#### local_dir\n",
    "\n",
    "This directory is on _your_ machine, and dictates where the runners will \"stage\" from. When running, files are first written to this directory then sent to the remote.\n",
    "\n",
    "#### remote_dir\n",
    "\n",
    "This directory is the main one on the _remote_ machine, and is where all the main run files are copied to.\n",
    "\n",
    "#### run_dir\n",
    "\n",
    "This directory is not always used, it exists _within_ the `remote_dir`, and is where the run will actually be executed.\n",
    "\n",
    ".. warning::\n",
    "    Be careful using run_dir with runs where the file system needs to be interacted with. A good example is when sending extra files, you will need to access them using `../file`, for example.\n",
    "\n",
    "### Run modifiers\n",
    "\n",
    "#### Asynchronous\n",
    "\n",
    "True by default, ensures that runs are executed in parallel. Set to False to force a dataset to execute its runners one after another (only functions when `submitter=\"bash\"`)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8868f33d-4c05-4b98-ab09-b306b92973f3",
   "metadata": {},
   "source": [
    "## Argument Hierarchy\n",
    "\n",
    "`run_args` can be set at multiple levels.\n",
    "\n",
    "- `Dataset` - This is the \"top level\" storage, all runners inherit from this dictionary\n",
    "- `Runner` - Runners can have their own \"local\" run_args, just for that run\n",
    "- `Run`/`Temporary` - when running a Dataset, you can also pass arguments into the run. These are considered \"temporary\" arguments, and will be dropped after the run completes.\n",
    "\n",
    "Lets demonstrate what this looks like, starting with the defaults:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "03dddb07-d064-4ce0-bce9-e98003cae5f1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n"
     ]
    }
   ],
   "source": [
    "from remotemanager import Dataset\n",
    "\n",
    "def function(inp):\n",
    "    return inp\n",
    "\n",
    "# skip=False will be used heavily throughout the tutorials\n",
    "# it is recommended that you also do so when experimenting\n",
    "ds = Dataset(function, skip=False)\n",
    "\n",
    "ds.append_run({\"inp\": 1})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "36178d67-8167-4c2c-8373-ea460efbb9a7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'skip': True, 'force': False, 'asynchronous': True, 'local_dir': 'temp_runner_local', 'remote_dir': 'temp_runner_remote'}\n"
     ]
    }
   ],
   "source": [
    "print(ds.run_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bb9ff43-b5e4-40e3-9904-9347a29500d7",
   "metadata": {},
   "source": [
    "The defaults here mean:\n",
    "\n",
    "- Runs will try to skip (if they already have results)\n",
    "- Runs will not be forced\n",
    "- Jobs will be run asynchronously\n",
    "- The local staging directory is `temp_runner_local`\n",
    "- The remote running directory is `temp_runner_remote`\n",
    "\n",
    "By comparison, the `Runner` object will appear to have no run_args, since these are \"overrides\" that are set at the runner level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "14cc79d9-82cc-4f7a-984c-a23ef4db3f13",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{}\n"
     ]
    }
   ],
   "source": [
    "print(ds.runners[0].run_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cf24b9a-1828-4266-8be9-d3d2cc66143f",
   "metadata": {},
   "source": [
    "When running a job, these arguments are combined into a single dictionary. \n",
    "\n",
    "This can be seen at `derived_run_args`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "35a65753-bd73-42ff-a783-349ed9cd2c84",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'skip': True, 'force': False, 'asynchronous': True, 'local_dir': 'temp_runner_local', 'remote_dir': 'temp_runner_remote'}\n"
     ]
    }
   ],
   "source": [
    "print(ds.runners[0].derived_run_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42a098b2-5f41-46d7-9855-c4dee420d7eb",
   "metadata": {},
   "source": [
    "## Setting run_args\n",
    "\n",
    "Now we know the default values, how do we change them?\n",
    "\n",
    "Firstly, any argument passed to `Dataset`, `append_run`, or `run()` that is _not_ part of those functions will be treated as a `run_arg`. However you can update them after initialisation.\n",
    "\n",
    "There are multiple ways to update or set args. The most obvious way is to directly update the `run_arg` dictionaries, but there also functions that can do this more \"explicity\".\n",
    "\n",
    ".. note::\n",
    "    These functions exist on both `Dataset` and `Runner`.\n",
    "\n",
    "Lets start by demonstrating a direct method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d3a71ea9-258e-4dc4-83fe-0a5f04b45535",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "skip True\n",
      "force False\n",
      "asynchronous True\n",
      "local_dir temp_runner_local\n",
      "remote_dir temp_runner_remote\n",
      "direct True\n"
     ]
    }
   ],
   "source": [
    "ds.run_args[\"direct\"] = True\n",
    "\n",
    "for k, v in ds.run_args.items():\n",
    "    print(k, v)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "316b60e6-d213-4c03-ae7d-4daa9c4c8cdf",
   "metadata": {},
   "source": [
    "### set_run_args\n",
    "\n",
    "This function can take a list of keys and values, and set them. You can also pass a single (key, val) pair."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "28bf3e7e-1679-42bd-97bf-6a39f6f791e6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "skip True\n",
      "force False\n",
      "asynchronous True\n",
      "local_dir temp_runner_local\n",
      "remote_dir temp_runner_remote\n",
      "direct True\n",
      "a 1\n",
      "b 2\n",
      "c 3\n",
      "d 4\n"
     ]
    }
   ],
   "source": [
    "ds.set_run_args([\"a\", \"b\", \"c\"], [1, 2, 3])\n",
    "\n",
    "ds.set_run_args(\"d\", 4)\n",
    "\n",
    "for k, v in ds.run_args.items():\n",
    "    print(k, v)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d7af633-7dc9-49c5-ae0d-b8edb018ae5e",
   "metadata": {},
   "source": [
    "### update_run_args\n",
    "\n",
    "This function takes a dictionary of arguments and updates the inner run_args with it. Useful for setting a large set of arguments at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "96dbe5e8-88b3-41dc-a7a5-316d1bedeb64",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "skip True\n",
      "force False\n",
      "asynchronous True\n",
      "local_dir temp_runner_local\n",
      "remote_dir temp_runner_remote\n",
      "direct True\n",
      "a 10\n",
      "b 11\n",
      "c 12\n",
      "d 13\n"
     ]
    }
   ],
   "source": [
    "ds.update_run_args({\"a\": 10, \"b\": 11, \"c\": 12, \"d\": 13})\n",
    "\n",
    "for k, v in ds.run_args.items():\n",
    "    print(k, v)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2174818c-56b2-441b-8a28-a81597210bea",
   "metadata": {},
   "source": [
    "## Custom run_args\n",
    "\n",
    "Unhandled `run_args` will be ignored by a run. However if you are using a Computer that accepts arguments for its `script()` method, they can be used there.\n",
    "\n",
    "The main use for this dynamic ability is for scheduler resources, and this is covered in depth within the [Scheduler Tutorial](C0_Intro.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca7379ea-cf9d-495f-852f-2bd28023975c",
   "metadata": {},
   "source": [
    "## Runner overrides\n",
    "\n",
    "The `run_args` of `Runner` act as \"local\" overrides for whatever is set in `Dataset`.\n",
    "\n",
    "We can demonstrate this by setting a value on the runner."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "185d306f-1572-4a61-bb02-66ee0ad155e2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Dataset args: 13\n",
      "Runner args: foo\n",
      "Derived args: foo\n"
     ]
    }
   ],
   "source": [
    "ds.runners[0].run_args[\"d\"] = \"foo\"\n",
    "\n",
    "print(\"Dataset args:\", ds.run_args.get(\"d\", None))\n",
    "\n",
    "print(\"Runner args:\", ds.runners[0].run_args.get(\"d\", None))\n",
    "\n",
    "print(\"Derived args:\", ds.runners[0].derived_run_args.get(\"d\", None))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "309e7bfb-0b93-4b0b-a4fe-d73044df655c",
   "metadata": {},
   "source": [
    "At the `Dataset` level, the value of `d` is still 13. However, on the runner on which we override the value, it is now \"foo\".\n",
    "\n",
    "Any _other_ runners, will retain the `Dataset` level value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "0404b1ba-dc81-4f9a-9842-fa55d5256418",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-1\n",
      "Derived args: 13\n"
     ]
    }
   ],
   "source": [
    "ds.append_run({\"inp\": 2})\n",
    "\n",
    "print(\"Derived args:\", ds.runners[1].derived_run_args.get(\"d\", None))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d75d271-a329-4fdf-b8d5-5c7e10969c0d",
   "metadata": {},
   "source": [
    "## Temporary Run() Args\n",
    "\n",
    "As was mentioned previously, you can also pass the same args to the `Run()` call of a Dataset. While difficult to demonstrate here, you can verify it by setting a `remote_dir` to something and then updating that arg within `Run()`.\n",
    "\n",
    "Args set this way are discarded after the run, and are considered \"temporary\" by the `Dataset`, whereas args set any other way are saved when the Dataset is."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
