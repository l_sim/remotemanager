{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e575c38f-450d-4241-82b3-ef6450c98ca8",
   "metadata": {},
   "source": [
    "# Dataset Cleaning\n",
    "\n",
    "## Starting Anew\n",
    "\n",
    "By default, a `Dataset` will attempt to reinitialise at launch. In short, this means that it looks for a file that looks like itself. If it finds such a file, it will recreate itself from it.\n",
    "\n",
    "For the user, this brings the benefits that your workflow is robust against restarts and data loss. However this does mean that datasets act like \"accumulators\", constantly gaining attributes and runners as you test.\n",
    "\n",
    "There can come a point where you realise that something you set earlier could be causing problems (or simply isn't needed), this tutorial will run through some methods of dealing with these situations.\n",
    "\n",
    "Lets start with the most basic case, `skip`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "6867867c-9a9c-442d-ba56-6a78073d2865",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset\n",
    "\n",
    "def f(inp):\n",
    "    return inp\n",
    "\n",
    "ds = Dataset(f,\n",
    "             skip=False  # new option!\n",
    "            )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a42a5d1-429f-4a0e-acd3-375a7d8c041d",
   "metadata": {},
   "source": [
    "When set to `False`, the `skip` argument will force the Dataset to start anew, and thus any variables that were stored are lost. This can also be done by deleting the database file (and in fact, this is what is done here internally, though then a new one is created). The filename is a combination of `name`-`dataset`-`uuid`.yaml. However if no `name` is set, it is omitted.\n",
    "\n",
    "Deleting this file has the same effect as `skip`, though only once. It will be created along with the dataset.\n",
    "\n",
    "The database filename can be seen using `Dataset.dbfile`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "2fd587cb-da57-46c5-aeb1-5e91dd893432",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'dataset-9ebf1589.yaml'"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.dbfile"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e216e733-6924-4c9b-a745-00e71985c4e6",
   "metadata": {},
   "source": [
    "You can also force this value to be whatever you want, but only at the dataset initialisation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "3837eaa7-967f-44cb-aeea-64323c6c3f91",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "the database file for this dataset is now set_a_name_here.yaml\n"
     ]
    }
   ],
   "source": [
    "ds_unique = Dataset(f,\n",
    "                    dbfile = 'set_a_name_here',  # new option!\n",
    "                    )\n",
    "\n",
    "print(f'the database file for this dataset is now {ds_unique.dbfile}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "785287c6-5f74-4da2-8074-3f14b78f11ea",
   "metadata": {
    "tags": []
   },
   "source": [
    "Now whenever you initialise a dataset with this filename, it will attempt to connect with that file.\n",
    "\n",
    "## Finer options\n",
    "\n",
    "This is all well and good, but what if you don't want to blow up your dataset and start again? For example, you know that one of your runners is causing issues and needs to be removed. Well there are options for this too.\n",
    "\n",
    "Lets append some runs, and experiment with removing them. \n",
    "\n",
    "Lets also create a function to show us some information about our runners."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "de984d46-165f-4388-b855-2a04caafd124",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n",
      "appended run runner-2\n",
      "appended run runner-3\n",
      "appended run runner-4\n",
      "appended run runner-5\n",
      "appended run runner-6\n"
     ]
    }
   ],
   "source": [
    "for run in range(7):\n",
    "    ds.append_run(args={'inp': run})\n",
    "    \n",
    "def print_runs():\n",
    "    for r_id, runner in ds.runner_dict.items():\n",
    "        print(f'{r_id}: {runner.short_uuid} | {runner.args}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "f067bd3e-e1ee-465c-9477-883966c4e6e4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "runner-0: 655dd314 | {'inp': 0}\n",
      "runner-1: 1360fc34 | {'inp': 1}\n",
      "runner-2: e008c421 | {'inp': 2}\n",
      "runner-3: eb3e73e3 | {'inp': 3}\n",
      "runner-4: 2e945aa8 | {'inp': 4}\n",
      "runner-5: e5e355b4 | {'inp': 5}\n",
      "runner-6: 7082ed01 | {'inp': 6}\n"
     ]
    }
   ],
   "source": [
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dffd4eb7-9970-4d8c-b7d8-58326c1d20dc",
   "metadata": {},
   "source": [
    "Now, we can look at all the ways of removing a run. We do this with `ds.remove_run(id)`. Here, `id` is a \"smart\" value, and can be `int`, `str` or `dict`, the function will perform slightly differently based on the input type:\n",
    "\n",
    "- An `int` will be treated like a list index, and the runner at that id will be removed.\n",
    "- A `dict` will be treated like arguments, and the runner with those args will be searched for.\n",
    "- `str` is first checked against the runner names, and and is then checked against the `uuid` of each runner.\n",
    "    - short and long uuids can be used (8 or 64 chars)\n",
    "\n",
    "## Runner Removal\n",
    "\n",
    "Firstly, if you know the index of the runner within `ds.runners`, you can pass that id:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "f3e901a5-3d10-44d0-942b-8b64cca91852",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "removed runner dataset-9ebf1589-runner-0\n",
      "runner-1: 1360fc34 | {'inp': 1}\n",
      "runner-2: e008c421 | {'inp': 2}\n",
      "runner-3: eb3e73e3 | {'inp': 3}\n",
      "runner-4: 2e945aa8 | {'inp': 4}\n",
      "runner-5: e5e355b4 | {'inp': 5}\n",
      "runner-6: 7082ed01 | {'inp': 6}\n"
     ]
    }
   ],
   "source": [
    "ds.remove_run(0)\n",
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f75d3f4-745e-42df-a148-f6bef064ce2f",
   "metadata": {},
   "source": [
    "Runner 0 has dissappeared!\n",
    "\n",
    ".. Note::\n",
    "    This function always removes the runner at that index. So in this case if we call again with index 0, runner-1 would be removed, as it is the first.\n",
    "\n",
    "Next, is the uuid. This can be found by printing the uuid of a runner you have access to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "8403544c-054e-4459-85b8-380c0c35f0a1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "eb3e73e3dfaa213b1f58893de0340220cd39876c36ff7684a280e22bf51b85b6\n",
      "2e945aa8\n"
     ]
    }
   ],
   "source": [
    "r_uuid = ds.runners[2].uuid\n",
    "\n",
    "print(r_uuid)\n",
    "\n",
    "r_short_uuid = ds.runners[3].short_uuid\n",
    "\n",
    "print(r_short_uuid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "7684f74e-eb96-422f-962e-e65a87252838",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "removed runner dataset-9ebf1589-runner-3\n",
      "removed runner dataset-9ebf1589-runner-4\n",
      "runner-1: 1360fc34 | {'inp': 1}\n",
      "runner-2: e008c421 | {'inp': 2}\n",
      "runner-5: e5e355b4 | {'inp': 5}\n",
      "runner-6: 7082ed01 | {'inp': 6}\n"
     ]
    }
   ],
   "source": [
    "ds.remove_run(r_uuid)\n",
    "ds.remove_run(r_short_uuid)\n",
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5eace168-3e13-4320-8a84-75805915a616",
   "metadata": {},
   "source": [
    "We grabbed the uuids of runners at id 2 and 3, which in the runner list would be runner numbers 3 and 4 (as we removed 0). These two have also dissappeared.\n",
    "\n",
    "If you don't know the id of the runner and don't have their uuids stored, you can remove by args. This attempts to match passed args with those that the runners have stored and will attempt to remove them. This is arguably the most flexible (and useful) method, though is less efficient than other approaches.\n",
    "\n",
    "For example, if you append run with `{'inp': 6}`, you may remove that runner by calling:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "d3e32ed9-e0af-4f52-b3d8-a4b66790fee3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "removed runner dataset-9ebf1589-runner-6\n",
      "runner-1: 1360fc34 | {'inp': 1}\n",
      "runner-2: e008c421 | {'inp': 2}\n",
      "runner-5: e5e355b4 | {'inp': 5}\n"
     ]
    }
   ],
   "source": [
    "ds.remove_run({'inp': 6})\n",
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b88bc30-47cf-4028-a7d5-b1d95e0de36f",
   "metadata": {},
   "source": [
    "Looks like runner 6 (who had `inp: 6`) has also gone.\n",
    "\n",
    "Finally, removing via `id` may be confusing if runs have already been removed, (i.e. you don't have a continuous, zero-indexed list). Thus, you can remove by the actual id by passing `remove_run(\"runner-{n}\")`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "4bd30942-0f90-484d-8fed-7f7378a92cbd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "removed runner dataset-9ebf1589-runner-5\n",
      "runner-1: 1360fc34 | {'inp': 1}\n",
      "runner-2: e008c421 | {'inp': 2}\n"
     ]
    }
   ],
   "source": [
    "ds.remove_run('runner-5')\n",
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "162b8884-020c-497b-8508-6a3c7d24991b",
   "metadata": {},
   "source": [
    "Leaving us with just runners 1 and 2.\n",
    "\n",
    "This function also returns True or False depending on if it removed a runner or not:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "4b26ae8f-4b2f-4501-86b3-cc64fddff8c0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "removed runner dataset-9ebf1589-runner-2\n",
      "removed runner-2?: True\n",
      "removed runner-3?: False\n",
      "\n",
      "final runner list:\n",
      "runner-1: 1360fc34 | {'inp': 1}\n"
     ]
    }
   ],
   "source": [
    "print('removed runner-2?:', ds.remove_run(1))\n",
    "print('removed runner-3?:', ds.remove_run(2))\n",
    "print('\\nfinal runner list:')\n",
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02532526-2c31-40be-87f0-1eb780313a21",
   "metadata": {},
   "source": [
    "#### Clearing Runners\n",
    "\n",
    "There is one additional option for removing runners, and that's `wipe_runs`. This removes _all_ runs from the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "aaa67bcc-42f3-4272-847a-c6490ca9cd18",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[]\n"
     ]
    }
   ],
   "source": [
    "ds.wipe_runs()\n",
    "print(ds.runners)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c92659e4-f66b-463c-9771-16350f06d2fe",
   "metadata": {},
   "source": [
    "### Persistence\n",
    "\n",
    "All these changes are of course, saved to the database when performed, so be careful when using them. If we simulate restarting this notebook (or a different notebook that also uses this dataset), we will see no runners:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "0965590d-8d3c-4b12-9989-68665fb89069",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[]\n",
      "appended run runner-0\n",
      "[dataset-9ebf1589-runner-0]\n"
     ]
    }
   ],
   "source": [
    "ds = Dataset(f)\n",
    "print(ds.runners)\n",
    "\n",
    "ds.append_run({'inp': 2})\n",
    "print(ds.runners)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d17f1dc0-546e-4a3a-a61f-f0875329542c",
   "metadata": {},
   "source": [
    "### re-adding runners\n",
    "\n",
    "Adding runners back to a dataset that has \"holes\" within its runner storage will cause no harm. Runners will be added to fill any missing spaces then continue as normal after that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "ee9cc8af-e14e-46b5-9c4a-8477e5b11db6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-1\n",
      "appended run runner-2\n",
      "runner runner-0 already exists\n",
      "appended run runner-3\n",
      "appended run runner-4\n",
      "appended run runner-5\n",
      "appended run runner-6\n",
      "appended run runner-7\n",
      "appended run runner-8\n",
      "appended run runner-9\n"
     ]
    }
   ],
   "source": [
    "for run in range(10):\n",
    "    ds.append_run({'inp': run})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "7f213b8c-0208-49ac-bce5-3932942ac1fc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "runner-0: e008c421 | {'inp': 2}\n",
      "runner-1: 655dd314 | {'inp': 0}\n",
      "runner-2: 1360fc34 | {'inp': 1}\n",
      "runner-3: eb3e73e3 | {'inp': 3}\n",
      "runner-4: 2e945aa8 | {'inp': 4}\n",
      "runner-5: e5e355b4 | {'inp': 5}\n",
      "runner-6: 7082ed01 | {'inp': 6}\n",
      "runner-7: 863094b9 | {'inp': 7}\n",
      "runner-8: 4bfe2419 | {'inp': 8}\n",
      "runner-9: 64d5a225 | {'inp': 9}\n"
     ]
    }
   ],
   "source": [
    "print_runs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92c4e880-56ef-4133-8475-8603703c1122",
   "metadata": {},
   "source": [
    "Note here how we now have 10 runners as expected. Runner with `inp: 2` has been skipped, as it already exists.\n",
    "\n",
    "## Run Args\n",
    "\n",
    "Now you know how to remove runners, what about run args? If you want to update a value, in most cases you can simply overwrite the value. Though if a run argument is causing issues, you can also delete it with the usual python syntax.\n",
    "\n",
    ".. note::\n",
    "    Starting in version `0.10.0`, run_args are no longer accessible at the dataset level (i.e. `ds.mpi`), and must be accessed via the `run_args` property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "51048cec-5b39-41ca-acc9-0f314db79ddb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "16\n"
     ]
    }
   ],
   "source": [
    "ds.set_run_arg(\"mpi\", 16)\n",
    "\n",
    "print(ds.run_args[\"mpi\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "fe87a274-bbf9-4f0d-a705-7e732466b15c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'skip': True, 'force': False, 'asynchronous': True, 'local_dir': 'temp_runner_local', 'remote_dir': 'temp_runner_remote', 'mpi': 16}\n",
      "{'skip': True, 'force': False, 'asynchronous': True, 'local_dir': 'temp_runner_local', 'remote_dir': 'temp_runner_remote'}\n"
     ]
    }
   ],
   "source": [
    "print(ds.global_run_args)\n",
    "\n",
    "del ds.run_args[\"mpi\"]\n",
    "\n",
    "print(ds.global_run_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "550088c0-7023-4e92-90a0-c2144a285162",
   "metadata": {},
   "source": [
    "## Cleaning Directories\n",
    "\n",
    ".. versionadded:: 0.5.9\n",
    "\n",
    "Too much clutter from testing? Dataset has some functions which help with deleting unwanted data:\n",
    "\n",
    "- `dataset.wipe_local()` will attempt to delete any local directories\n",
    "- `dataset.wipe_remote()` will attempt to delete any remote and run directories\n",
    "\n",
    "If you really want to reset, dataset also provdes a `dataset.hard_reset()` function, which will do all of the above, delete the database file and then clear any runners. This essentially gives you a like-new dataset."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
