{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "825377ee-6c31-45df-ac54-3c4959e1405d",
   "metadata": {},
   "source": [
    "# Database and Backup\n",
    "\n",
    "## Database Files\n",
    "\n",
    "When using Datasets, you may have noticed that a file of the form `dataset-abcd1234.yaml` is created in the working directory where the dataset is being run. This is what's known as a `Database` file, and is where the `Dataset` stores info.\n",
    "\n",
    "This functionality allows the `skip` options to function across notebook restarts.\n",
    "\n",
    "However, the `Database` is more powerful than just a storage of important data, in fact it actually stores almost _all_ data.\n",
    "\n",
    "### Dataset Permanence\n",
    "\n",
    "It is because of this total info store that we can recreate a `Dataset` from a `Database` file at any time. This is in fact what happens when you restart your notebook with skip=True in the Dataset initialisation (the default).\n",
    "\n",
    "But this allows us to use this functionality in reverse, \"packing\" a dataset into a file that can be transferred or backed up.\n",
    "\n",
    "Lets create a dataset to play with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "600657a0-6a02-4d65-81cc-9d7185e22728",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset\n",
    "\n",
    "def function(a, b):\n",
    "    return a * b\n",
    "\n",
    "ds = Dataset(function, skip=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e7642eb-e487-4fb5-88d6-7ebca96dd0a4",
   "metadata": {},
   "source": [
    ".. note::\n",
    "    It is important to note here the behaviour of the `skip` parameter. When a dataset is created, it will search for a `Database` file that matches the parameters given. If found, it will unpack itself from that file by default. If `skip=False`, it will instead delete that file and recreate itself in place."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "029437a3-f37c-41c6-b0c4-939667ee7b94",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n",
      "appended run runner-2\n",
      "Running Dataset\n",
      "assessing run for runner dataset-d8ecb370-runner-0... running\n",
      "assessing run for runner dataset-d8ecb370-runner-1... running\n",
      "assessing run for runner dataset-d8ecb370-runner-2... running\n",
      "Transferring 8 Files... Done\n",
      "Fetching results\n",
      "Transferring 6 Files... Done\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[10, 35, 36]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "runs = [\n",
    "    [1, 10],\n",
    "    [7, 5],\n",
    "    [12, 3]\n",
    "]\n",
    "\n",
    "for run in runs:\n",
    "    ds.append_run({\"a\": run[0], \"b\": run[1]})\n",
    "# run the dataset\n",
    "ds.run()\n",
    "# wait for the completion, checking every 1 second, up to a maximum of 10 seconds\n",
    "ds.wait(1, 10)\n",
    "# collect the results\n",
    "ds.fetch_results()\n",
    "# check the results\n",
    "ds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65244e24-02fc-4736-94fa-c5f11327da62",
   "metadata": {},
   "source": [
    "Now we have a completed run, lets explore some situations where the `Database` helps us.\n",
    "\n",
    "#### Notebook Restarts\n",
    "\n",
    "The most common use of these files is done automatically for you if a notebook is killed and restarted. If a Dataset is created without `skip=False`, it will recreate itself if it can. Lets simulate a restart here by deleting the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "703f40bd-2c67-4387-92a2-c3321e67ad74",
   "metadata": {},
   "outputs": [],
   "source": [
    "del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6896f9f-56f0-4c12-9feb-28053fa0e475",
   "metadata": {},
   "source": [
    "Now, the dataset no longer exists within the notebook, exactly as if we had killed the notebook and restarted. Lets recreate it as we are rerunning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "cfd0bc2c-7de2-4060-b933-2df391361a4b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[10, 35, 36]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds = Dataset(function)\n",
    "\n",
    "ds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "299d34e8-2b7f-4262-b3e8-6c2e0b53a782",
   "metadata": {},
   "source": [
    "Since the dataset was recreated, it still contains everything necessary to continue as if it was never deleted. If we tried to run, it will skip, since the runs have already succeeded:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "e852f2ba-cd84-4541-83bd-f52e9891dfb3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running Dataset\n",
      "assessing run for runner dataset-d8ecb370-runner-0... ignoring run for successful runner\n",
      "assessing run for runner dataset-d8ecb370-runner-1... ignoring run for successful runner\n",
      "assessing run for runner dataset-d8ecb370-runner-2... ignoring run for successful runner\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bff8fa9-e113-4b10-9ed5-9d88e298ecc4",
   "metadata": {},
   "source": [
    "In short, this means that the often intensive and long calculations are independent of the notebook. You do not risk resubmitting a large job if you accidentally close your notebook and rerun."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2213c81-d70a-4b5f-a0c1-5b430f97a83c",
   "metadata": {},
   "source": [
    "#### Notebook Transfers\n",
    "\n",
    "Since the notebook and the database are just files, this also allows you to transfer your datasets to another machine or person. Simply copy across the notebook, along with the database, and `remotemanager` will attempt to run as if nothing has changed.\n",
    "\n",
    ".. important::\n",
    "    Note that while the Dataset will attempt to run as normal, outside factors such as the python environment can still affect the runtime.\n",
    "\n",
    ".. important::\n",
    "    If your Dataset requires (or creates) extra files that are needed for your workflow, be sure to have these at the same relative location to the new working directory. Later in this tutorial we will cover `Dataset.backup`, which automates more of this process for you.\n",
    "\n",
    "## Renaming the File\n",
    "\n",
    "The automatically generated filename for the dataset can be complicated to remember. If you have multiple datasets running, even impossible to distinguish. It is possible to influence this file name in many different ways:\n",
    "\n",
    "- Give the Dataset a name\n",
    "- Set the `dbfile` parameter\n",
    "- Pack to a custom file\n",
    "\n",
    "Lets go through these now.\n",
    "\n",
    "### Naming the Dataset\n",
    "\n",
    "Datasets can be given a `name` parameter, which makes their files easier to identify. First, lets take a look at the filename for the dataset created earlier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "1cc99a26-b18e-48ff-980d-3cecc65d1a43",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'dataset-d8ecb370.yaml'"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.dbfile"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "367a61da-b6cd-42d5-8e68-010b44868cae",
   "metadata": {},
   "source": [
    "Not exactly memorable. Lets recreate with `skip=False` and give the new Dataset a name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "e4decfae-6288-45ea-8208-ac425d2d7f24",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'dataset-functiontest-291a69ef.yaml'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds = Dataset(function, name=\"functiontest\", skip=False)\n",
    "\n",
    "ds.dbfile"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd81b3bf-4a62-4822-aac4-f5537d244d65",
   "metadata": {},
   "source": [
    "Now our name has been added to the filename, making it somewhat easier to find.\n",
    "\n",
    "### Specifying the filename\n",
    "\n",
    "If you want to go one step further and customise the filename, the `dbfile` parameter that we've been checking can also be set at initialisation. This sets the filename that is used, so it can be whatever you want."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "11acf9b4-b107-4f47-97d9-04f68e5dbb3b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'dataset_custom_filename.yaml'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds = Dataset(function, dbfile=\"dataset_custom_filename\", skip=False)\n",
    "\n",
    "ds.dbfile"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62506fee-3f11-477e-a78e-082a44b77314",
   "metadata": {},
   "source": [
    ".. note::\n",
    "    Since Databases are in yaml format, if you omit this ending from your dbfile, it will add it for you.\n",
    "\n",
    "### Packing to a Custom File\n",
    "\n",
    "The functionalities that are used for the Database file are open for use by the user, and they do not always have to target the same file. You can pack and recreate from a file of your choosing, without touching the Database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "5f3525ca-e67c-451e-94ea-daf9fd1f98f2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "dumping payload to temporary_dataset_pack\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.pack(file=\"temporary_dataset_pack\")\n",
    "\n",
    "import os\n",
    "os.path.isfile(\"temporary_dataset_pack\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46633556-0642-4bf4-ba7f-37c59e02c815",
   "metadata": {},
   "source": [
    "This method of storage does not enforce the yaml file extension, though the actual file content is still of the yaml format internally.\n",
    "\n",
    "We can recreate from this file using `Dataset.from_file()`\n",
    "\n",
    ".. versionadded:: 0.13.4\n",
    "    After the changes to how Computer is serialised, `from_file` now requires a `url` to be passed. Otherwise, a default (localhost) one will be created."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "dfb323db-c656-4d24-b4cc-a6c9240ab1e8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'dataset_custom_filename.yaml'"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "del ds\n",
    "\n",
    "ds = Dataset.from_file(\"temporary_dataset_pack\")\n",
    "\n",
    "ds.dbfile"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42f4fc16-4085-4636-8b04-7f7d18fc6d63",
   "metadata": {},
   "source": [
    "Note how the dbfile has not changed, as this pack/recreate is considered a \"temporary\" method of transfer.\n",
    "\n",
    "## Backup and Restore\n",
    "\n",
    ".. versionadded:: 0.9.16\n",
    "\n",
    "It was mentioned earlier that there is a more advanced method for backup and restore than that which we have just covered. This system automatically handles returned files in addition to the dataset itself, so is more robust in the face of a Dataset which also uses files.\n",
    "\n",
    "You should keep in mind that this method only handles _returned_ files. This only includes:\n",
    "    \n",
    "    - result\n",
    "    - error\n",
    "    - extra_files_recv\n",
    "\n",
    "To demonstrate this, it is best to create a dataset that _does_ return files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "0468290b-13ea-4f25-ba6b-ace735063373",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "Running Dataset\n",
      "assessing run for runner dataset-34bf7acd-runner-0... running\n",
      "Transferring 4 Files... Done\n",
      "Fetching results\n",
      "Transferring 3 Files... Done\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[None]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def to_file(inp, fname):\n",
    "    with open(fname, \"w+\") as o:\n",
    "        o.write(str(inp))\n",
    "\n",
    "ds = Dataset(to_file, skip=False)\n",
    "\n",
    "ds.append_run({\"inp\": \"test\", \"fname\": \"test.out\"}, extra_files_recv = \"test.out\")\n",
    "\n",
    "ds.run()\n",
    "ds.wait(1, 10)\n",
    "ds.fetch_results()\n",
    "ds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "539695b6-0b98-4386-9cd3-8a612a635f31",
   "metadata": {},
   "source": [
    "Our function here does not return anything, so the `results` property holds no data. The real information is within the file that is returned.\n",
    "\n",
    ".. note::\n",
    "    It is considered good practice to have your functions returns something. This can make it much easier to fix problems. In this case, it would be wise to have the function return `fname` at the least, so we know that the function has completed as expected.\n",
    "\n",
    "To access our \"result\", we should read the content of the returned file. The extra files are a special `TrackedFile` class which can help with this.\n",
    "\n",
    "Lets get the runner that we want to see (index 0), then check its list of extra files to recieve. Since there's only one, we take the first index again, and print the `content` property of the `TrackedFile` that is there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "64d3c518-0f66-4d7a-b9ed-e1a647cbe576",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "test\n"
     ]
    }
   ],
   "source": [
    "print(ds.runners[0].extra_files_recv[0].content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "843ee5d1-5d5f-4abe-a2d2-5bd68b69cdae",
   "metadata": {},
   "source": [
    "### Limitations of the Database\n",
    "\n",
    "Since the database only handles the properties of the Dataset and its runners directly, these extra files are only \"tracked\". So if they were to be deleted, moved, or renamed, the Dataset is essentially broken. If we delete the local file, even if we restore from a pack, the file contents will be gone:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "104b5446-60cb-4f26-8bd7-1fd136e1b999",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "dumping payload to dataset_with_files_backup.yaml\n",
      "None\n"
     ]
    }
   ],
   "source": [
    "ds.pack(file=\"dataset_with_files_backup.yaml\")\n",
    "\n",
    "try:\n",
    "    os.remove(ds.runners[0].extra_files_recv[0].local)\n",
    "except FileNotFoundError:\n",
    "    print(\"could not remove file\")\n",
    "\n",
    "ds = Dataset.from_file(file=\"dataset_with_files_backup.yaml\")\n",
    "\n",
    "print(ds.runners[0].extra_files_recv[0].content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf503f46-e7c2-4c2a-b307-3a547b31eb04",
   "metadata": {},
   "source": [
    "### Backup and Restore\n",
    "\n",
    "So in this situation, if we wanted to ensure the safety of our data, we should use the `backup` method. Lets fetch the results again to repopulate the files and demonstrate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "c401c849-e5f7-4cea-88f8-ef0a78524ec7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 2 Files... Done\n",
      "test\n"
     ]
    }
   ],
   "source": [
    "ds.fetch_results()\n",
    "\n",
    "print(ds.runners[0].extra_files_recv[0].content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b0efb2d-d24b-49e7-ad13-4961c86dacce",
   "metadata": {},
   "source": [
    "Now, do as before, using backup and restore."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "59946894-5270-4aec-b52b-ff608fcc878b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "test\n"
     ]
    }
   ],
   "source": [
    "ds.backup(file=\"full_backup.zip\", full=True, force=True)\n",
    "\n",
    "ds.hard_reset(files_only=True)\n",
    "\n",
    "ds = Dataset.restore(file=\"full_backup.zip\")\n",
    "\n",
    "print(ds.runners[0].extra_files_recv[0].content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1382d57-5602-4e42-bcb0-26113067a631",
   "metadata": {},
   "source": [
    "#### Usage Details\n",
    "\n",
    "There is a few things to note here, the first of which being that the filetype has to be `.zip`. If this is not the case, you'll get an error.\n",
    "\n",
    ".. note::\n",
    "    This limitation is due to using the Python inbuilt ZipFile module.\n",
    "\n",
    "Secondly, here we're using `force=True`. `backup` by default will not overwrite a file if it already exists, again raising an error. If you want to overwrite anyway, you can use `force=True` to overwrite the backup."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
