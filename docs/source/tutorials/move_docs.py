import glob
import os
import shutil
import sys


class File:
    __slots__ = ["_path", "_file"]

    def __init__(self, target: str):
        # get path and full filename by splitting target
        self._path, filename = os.path.split(target)
        # just get the actual name without extension, as it's forced to be .ipynb
        self._file = os.path.splitext(filename)[0]

    def __repr__(self):
        return self.path

    @property
    def directory(self):
        return self._path

    @property
    def path(self):
        return os.path.join(self._path, self.filename)

    @property
    def filename(self):
        return f"{self._file}.ipynb"

    @property
    def html(self):
        return f"{os.path.join(self._path, self._file)}.html"


if __name__ == "__main__":
    source = File(sys.argv[1])
    dest = File(sys.argv[2])

    docs = glob.glob("*.ipynb")

    if not os.path.exists(source.path):
        raise ValueError(f"input file {source} does not exist")
    if os.path.exists(dest.path):
        raise ValueError(f"output file {dest} already exists")

    print(f"moving:\n{source}\n{dest}")

    print("replacing within files:")
    for file in docs:
        print("\t" + file)
        with open(file, "r", encoding="utf8") as o:
            content = o.read()

        content = content.replace(source.html, dest.html)

        with open(file, "w", encoding="utf8") as o:
            o.write(content)

    if not os.path.exists(dest.directory):
        os.makedirs(dest.directory)

    shutil.move(source.path, dest.path)
