{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import remotemanager\n",
    "\n",
    "remotemanager.Logger.path = 'Dataset_Tutorial'\n",
    "remotemanager.Logger.level = 'debug'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dataset Usage\n",
    "\n",
    "## The Dataset Object\n",
    "\n",
    "`Dataset` is the primary class of the package. It is a general purpose \"container\" which stores your function, runs and their results/errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the purposes of this tutorial, we will bring back our basic `multiply` function you may have seen in the quickstart guide. Though we will amend it such that the delay time is adjustable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def multiply(a, b, t=1):\n",
    "    import time\n",
    "    \n",
    "    time.sleep(t)\n",
    "    \n",
    "    return a * b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now set up a `Dataset`. The only _required_ argument is the function, though there are many other optional arguments, most of which we shall also cover in this tutorial. See the [Dataset API documentation](../remotemanager.dataset.dataset.html) for full details.\n",
    "\n",
    "Again for this tutorial we will be using a local url, this enables the functions to run anywhere and be tested. \n",
    "\n",
    ".. note:: \n",
    "    At a basic level , the `URL` is a connection to your machine, and can be swapped out at any time to change machines. In theory any function which runs on `URL('machine.a')` will also run just the same on `URL('machine.b')`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "from remotemanager import Dataset, URL\n",
    "\n",
    "url = URL('localhost')\n",
    "\n",
    "ds = Dataset(function=multiply,\n",
    "             url=url,\n",
    "             script='#!/bin/bash',\n",
    "             submitter='bash',\n",
    "             local_dir='temp_ds_staging',\n",
    "             remote_dir='temp_ds_remote',\n",
    "             name='tutorial_dataset',\n",
    "             skip=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arguments shown here are likely to be the ones used the most. So in short:\n",
    "\n",
    "\n",
    "- `url`: The remote connection, if it is not given, a default `localhost` \"connection\" will be created for you.\n",
    "- `local_dir`: This is the directory that will be used to \"stage\" your files before sending to the remote. Defaults to `temp_runner_local`.\n",
    "- `remote_dir`: Remote directory where files will be sent to. Defaults to `temp_runner_remote`.\n",
    "- `name`: Datasets can be named, which makes their files easier to locate. By default, and files will simply use the uuid of the dataset/runner to differentiate.\n",
    "- `skip`: Contextual argument, if set to `False`, will disable the Dataset init \"skip\", forcing it to delete the existing database and start anew.\n",
    "\n",
    "### Extra Variables\n",
    "\n",
    "If you wish to run on a machine which has a scheduler system, you can use the `script` variable to pass your jobscript. Though there are more advanced features in place to generate dynamic jobscripts, see the [Scheduler Tutorial](C0_Intro.html) for more info.\n",
    "\n",
    "You can also specify a `run_dir`, which will be an internal directory within `remote_dir`. By default this is not specified and runs will run within the `remote_dir`.\n",
    "\n",
    "`dbfile` allows you to force the dataset to store its database within a specific filename, should you wish to keep track of this. Otherwise, it defaults to `{self.name}-{self.short_uuid}.yaml`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Appending Runs\n",
    "\n",
    "Before running your function you must append runs containing any arguments.\n",
    "    \n",
    "`Dataset.append_run()` allows for run creation, and at minimum requires a `dict` containing the required arguments for your function.\n",
    "\n",
    "So in our case, a dictionary containing arguments for `a` and `b` are necessary for a run to begin. As `t` has a default value of 1, it is optional. The structure below will append 3 runs displaying this behaviour:\n",
    "\n",
    ".. note::\n",
    "    This is also true for runs that take no arguments, simply call `append_run()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n",
      "appended run runner-2\n"
     ]
    }
   ],
   "source": [
    "runs = [{'a': 10, 'b': 5},\n",
    "        {'a': 5.7, 'b': 8.4},\n",
    "        {'a': 4, 'b': 4, 't': 6}]\n",
    "\n",
    "for run in runs:\n",
    "    ds.append_run(args=run)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ".. note:: There is also the alias `arguments` for `args`\n",
    "\n",
    "Additonally, if you wish to run scripts within unique folders, you can specify a `run_dir` when appending runs. If this attribute is present, this folder will be created within the remote dir and the function will be run from within. You may need to adjust your scripts and additional files to suit this run behaviour."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The Runner object\n",
    "\n",
    "Now we have a dataset which is able to be run and return our results. Before we do this, it is worth stepping through some useful debugging tools.\n",
    "\n",
    "Firstly, how to query what runs you already have. This can be done by accessing the property `Dataset.runners`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[tutorial_dataset-a2c088ba-runner-0,\n",
       " tutorial_dataset-a2c088ba-runner-1,\n",
       " tutorial_dataset-a2c088ba-runner-2]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.runners"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also the `runner_dict` property, which returns the same information in dict(append id: runner) format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'runner-0': tutorial_dataset-a2c088ba-runner-0,\n",
       " 'runner-1': tutorial_dataset-a2c088ba-runner-1,\n",
       " 'runner-2': tutorial_dataset-a2c088ba-runner-2}"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.runner_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Lazy Append\n",
    "\n",
    ".. versionadded:: 0.8.4\n",
    "\n",
    "If you have a lot of runners to append (especially ones with large arguments), the base `append_run` can begin to slow down drastically. For such situations, you can call a context manager to wrap your run appends.\n",
    "\n",
    "Here we copy the dataset (so as not to add too much bloat to the tutorial), then add 10 more runs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Of 13 appends: 13 appended\n",
      "See append_log for more info\n",
      "13\n"
     ]
    }
   ],
   "source": [
    "import copy\n",
    "example_ds = copy.deepcopy(ds)\n",
    "\n",
    "with example_ds.lazy_append() as la:\n",
    "    for i in range(10):\n",
    "        la.append_run({'a': i, 'b': 0})\n",
    "\n",
    "print(len(example_ds.runners))\n",
    "\n",
    "del example_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a `lazy` option which can be used, which does the same thing. However there is a requirement that once you are done appending runs, you must add a `finish_append()` call, which finalises the appends all at once as though they were called normally.\n",
    "\n",
    ".. warning::\n",
    "    Omitting the `finish_append()` after using a lazy append will not raise an error, but can cause strange behaviour."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the Dataset\n",
    "\n",
    "Running of the datasets is done via the `Dataset.run()` method. This gives you one final opportunity to override any run arguments, as it provides another `run_args` catch for extra keyword args.\n",
    "\n",
    ".. note::\n",
    "    Be aware of the argument expansion limitation that exists with rsync versions below `3.0.0`. If you get errors during transfer, be sure to check `rsync --version` >= 3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running Dataset\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-0... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-1... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-2... running\n",
      "Transferring 8 Files... Done\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're following along on your machine you may have noticed that this call completed instantly, yet our function has a `time.sleep` line in it. We would expect to have to wait 8s for this (1+1+6s delays).\n",
    "\n",
    "This is because the dataset run defaults to be asynchronous, and as you can imagine, this can be updated by passing this as a `run_arg` wherever you wish.\n",
    "\n",
    "### Waiting for Completion\n",
    "\n",
    "Calculations can take time. You have two (non exclusive) options for dealing with this:\n",
    "\n",
    "- Leave the notebook for a while and rerun when you think the jobs have finished\n",
    "- Use `wait`\n",
    "\n",
    "Rerunning the notebook at any time will cause the inbuilt `skip` methods to kick in and make sure that any running or completed jobs are not resubmitted. This means that you can submit and leave the notebook. At rerun, and any `fetch_results` which failed before will grab the results this time.\n",
    "\n",
    ".. note::\n",
    "    Rerunning the notebook works fine provided you have not specified `skip=False` of `force=True` anywhere.\n",
    "\n",
    "You can also use the `wait` keyword. This is a one line wrapper for a block that looks similar to this:\n",
    "\n",
    "```\n",
    "interval = 2\n",
    "timeout = 10\n",
    "\n",
    "t0 = time.time()\n",
    "while not ds.all_finished():\n",
    "    time.sleep(interval)\n",
    "\n",
    "    if time.time() - t0 > timeout:\n",
    "        break\n",
    "```\n",
    "\n",
    "This periodically checks for completed runs every `interval` seconds. It is also a blocking call until `ds.all_finished` returns True, or more time than `timeout` has passed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.wait(interval=2, timeout=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The call here means to check every 2 seconds, and raise a timeout error after 10 total seconds have passed.\n",
    "\n",
    ".. note::\n",
    "    By default, `wait` waits for _any_ completion, including failures. You can restrict this to wait for a total success (timing out if there are failures) by passing `success_only=True`.\n",
    "\n",
    "### Asynchronous\n",
    "\n",
    "Asynchronous behaviour also means that each runner is running simultaneously, this can put excess load on machines not designed for it, or simply may not be what you want for your workflow. To avoid this, we can use `asynchronous=False`\n",
    "\n",
    "Additionally here, we must use the `force=True` keyword to ensure that the runs go through, as the previous runs are marked as `complete`. Be careful using this keyword in your workflows with long jobs, as if they are still running and complete before your more recent run, it wil cause the results to be \"injected\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "from remotemanager.utils import integer_time_wait\n",
    "\n",
    "integer_time_wait()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running Dataset\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-0... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-1... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-2... running\n",
      "Transferring 8 Files... Done\n",
      "run completed in 8.11s\n"
     ]
    }
   ],
   "source": [
    "ds.reset_runs(wipe=True)\n",
    "\n",
    "t0 = time.perf_counter()\n",
    "\n",
    "ds.run(asynchronous=False)\n",
    "\n",
    "dt = time.perf_counter() - t0\n",
    "\n",
    "# we expect that the synchronous run will take around 1+1+6=8s\n",
    "expected_time = 8\n",
    "# the test suite can take extra time here, need to leave ~2s of room\n",
    "assert abs(dt - expected_time) < 2, f\"run completed in {dt}s\"\n",
    "    \n",
    "print(f\"run completed in {dt:.2f}s\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While not particularly useful in a wide range of use cases, there may be a situation case where you want to _wait_ for a short run to complete, and this also displays the amending of run variables nicely.\n",
    "\n",
    "One final way you are able to set the run args is via the `set_run_arg` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "value!\n"
     ]
    }
   ],
   "source": [
    "ds.set_run_arg('asynchronous', True)\n",
    "print(ds.run_args[\"asynchronous\"])\n",
    "\n",
    "ds.set_run_arg('new_option', 'value!')\n",
    "print(ds.run_args[\"new_option\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Collecting Results\n",
    "\n",
    "There are functions indended to be used after a run has been called, to interact with the run, or the results.\n",
    "\n",
    "We shall cover:\n",
    "\n",
    "- is_finished\n",
    "- all_finished\n",
    "- fetch_results\n",
    "- results\n",
    "- errors\n",
    "\n",
    "### `Dataset.is_finished`\n",
    "\n",
    "This property will return a boolean list of the `is_finished` method of the runners. Runners are considered `finished` when they have either returned a result, or failed with an error.\n",
    "\n",
    "### `Dataset.all_finished`\n",
    "\n",
    "This property returns the all() of `Dataset.is_finished`\n",
    "\n",
    "To demonstrate these, we shall re-run and see what the state looks like at a few time intervals. But first, we must make sure that the results are not already present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "wiping result files...\n"
     ]
    }
   ],
   "source": [
    "print('wiping result files...')\n",
    "\n",
    "# this function will clear any runner results and optionally wipe local files\n",
    "ds.reset_runs(wipe=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets add a run that will fail, to demonstrate how errors are handled"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-3\n"
     ]
    }
   ],
   "source": [
    "# we can't multiply an int by None, so this should fail\n",
    "ds.append_run({'a': 0, 'b': None})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "calcs launched, waiting before checking completion\n",
      "Running Dataset\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-0... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-1... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-2... running\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-3... running\n",
      "Transferring 10 Files... Done\n",
      "\n",
      "after 2s, state is now:\n",
      "[True, True, False, True]\n",
      "all_finished: False\n",
      "\n",
      "after 7s, state is now:\n",
      "[True, True, True, True]\n",
      "all_finished: True\n"
     ]
    }
   ],
   "source": [
    "time.sleep(1)  # this short sleep prevents earlier runs getting in the way\n",
    "\n",
    "print('calcs launched, waiting before checking completion')\n",
    "ds.run(asynchronous=True)\n",
    "\n",
    "time.sleep(2)\n",
    "\n",
    "print('\\nafter 2s, state is now:')\n",
    "print(ds.is_finished)\n",
    "print('all_finished:', ds.all_finished)\n",
    "\n",
    "time.sleep(5)\n",
    "\n",
    "print('\\nafter 7s, state is now:')\n",
    "print(ds.is_finished)\n",
    "print('all_finished:', ds.all_finished)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It may seem counter-intuitive that the runs are all completed at 7s, but if we recall that they were launched asynchronously by default, the whole run would take around 6s (our maximum delay time).\n",
    "\n",
    "#### The remaining functions\n",
    "\n",
    "#### `Dataset.fetch_results()`\n",
    "\n",
    "This function will attempt to grab any results from files or function objects that are attached to the dataset, storing them in the `results` property\n",
    "\n",
    "#### `Dataset.results`\n",
    "\n",
    "This property allows optimised access to the results of the previous run. When `results` is queried, it also checks to see if there are any errors, and warns you if any are found.\n",
    "\n",
    "#### `Dataset.errors`\n",
    "\n",
    "Similar to `results`, this stores a list of the error content if available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fetching results\n",
      "Transferring 4 Files... Done\n"
     ]
    }
   ],
   "source": [
    "ds.fetch_results()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Warning! Found 1 error(s), also check the `errors` property!\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[50,\n",
       " 47.88,\n",
       " 16,\n",
       " RunnerFailedError('TypeError: unsupported operand type(s) for *: 'int' and 'NoneType'')]"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[None,\n",
       " None,\n",
       " None,\n",
       " \"TypeError: unsupported operand type(s) for *: 'int' and 'NoneType'\"]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.errors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further features\n",
    "\n",
    "While we touched on the runner availability earlier, we skipped over a feature which may be helpful for debugging purposes. The `Runner` object has a `history` property which prints a {time: state} dict that contains information about all state changes the runner has experienced.\n",
    "\n",
    "This runner has been run and rerun a few times now, so the history will be quite full. On a fresh Dataset, a flag will be set to wipe this history."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'2024-07-26 15:03:30/0': 'created',\n",
       " '2024-07-26 15:03:30/1': 'staged',\n",
       " '2024-07-26 15:03:30/2': 'submit pending',\n",
       " '2024-07-26 15:03:30/3': 'submitted',\n",
       " '2024-07-26 15:03:30/4': 'started',\n",
       " '2024-07-26 15:03:31/0': 'completed',\n",
       " '2024-07-26 15:03:38/0': 'reset',\n",
       " '2024-07-26 15:03:38/1': 'staged',\n",
       " '2024-07-26 15:03:38/2': 'submit pending',\n",
       " '2024-07-26 15:03:38/3': 'submitted',\n",
       " '2024-07-26 15:03:38/4': 'started',\n",
       " '2024-07-26 15:03:39/0': 'completed',\n",
       " '2024-07-26 15:03:46/0': 'reset',\n",
       " '2024-07-26 15:03:47/0': 'staged',\n",
       " '2024-07-26 15:03:47/1': 'submit pending',\n",
       " '2024-07-26 15:03:47/2': 'submitted',\n",
       " '2024-07-26 15:03:47/3': 'started',\n",
       " '2024-07-26 15:03:48/0': 'completed',\n",
       " '2024-07-26 15:03:49/0': 'completed',\n",
       " '2024-07-26 15:03:54/0': 'satisfied'}"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.runners[0].history"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "here you can see the state history for the first runner in the list, showing the three runs, the creation time of the resultfile on the remote, and the final completion state where the results were loaded back into the runner\n",
    "\n",
    "If you just require a list of states (for example, checking if a runner has passed through a state), there is the property `Runner.status_list`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Swapping out the serialiser\n",
    "This is now covered in more depth in the [dedicated tutorial](D4_Complex_Serialisation.html).\n",
    "\n",
    "### Access to the commands used to execute the runs\n",
    "\n",
    "Once you have run a dataset, you can access the command used to execute the bash scripts. This can be useful for debugging purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "raw command: cd temp_ds_remote && bash tutorial_dataset-a2c088ba-master.sh\n",
      "returned stdout: \n",
      "returned stderr: \n"
     ]
    }
   ],
   "source": [
    "print('raw command:', ds.run_cmd.sent)\n",
    "print('returned stdout:', ds.run_cmd.stdout)\n",
    "print('returned stderr:', ds.run_cmd.stderr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running a single runner\n",
    "\n",
    "While it was mentioned previously that the runners themselves should ideally not be touched, and all interaction should be done via the `Dataset`, it _is_ possible to run a single runner if necessary.\n",
    "\n",
    ".. warning::\n",
    "    this process is inefficient and should only be used if absolutely required. It may be preferable to clear the results of the offending runner using `reset_runs()` and rerunning with `skip=True`\n",
    "    \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# store what the current last submission time is\n",
    "last_submitted_initial = ds.runners[0].last_submitted"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running Dataset\n",
      "assessing run for runner tutorial_dataset-a2c088ba-runner-0... running\n",
      "Transferring 4 Files... Done\n",
      "Fetching results\n",
      "Transferring 1 File... Done\n"
     ]
    }
   ],
   "source": [
    "ds.reset_runs()  # clear results to demonstrate\n",
    "\n",
    "ds.runners[0].run(asynchronous=False)\n",
    "time.sleep(1)\n",
    "ds.fetch_results()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get the new last submission time\n",
    "last_submitted_after = ds.runners[0].last_submitted"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This quick assertion makes sure that the runner that was resubmitted actually has a different submission time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert last_submitted_initial != last_submitted_after"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can again here demonstrate the use of `check_all_runner_states`, as we have only run one, checking for full completion will return False. Obviously in this case, `all_finished` will do the job, but you can query here for any state, such as `submitted`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "print(ds.check_all_runner_states('completed'))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
