.. remotemanager documentation master file, created by
   sphinx-quickstart on Tue Jun  7 09:51:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to remotemanager's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

``remotemanager`` is a flexible and modular package dedicated to facilitating
running python functions on remote systems.

This documentation will cover the functionality and utilities that are made available by this package.
You may also wish to browse the source code, which can be `found here <https://gitlab.com/l_sim/remotemanager>`_.

See the introduction page covering the very basics:

.. toctree::
   :maxdepth: 1
   :caption: Introduction

   Introduction
   tutorials/A1_Quickstart
   FAQ

Or see the tutorials for usage help:

.. toctree::
   :maxdepth: 1
   :caption: Main Tutorials

   tutorials/B1_Datasets
   tutorials/B2_Run_Args
   tutorials/B3_Backing_Up_Datasets
   tutorials/B4_Failures
   tutorials/B5_Extra_Files

.. toctree::
   :maxdepth: 1
   :caption: HPC Interfacing

   tutorials/C0_Intro
   tutorials/C1_Schedulers
   tutorials/C2_Templates
   tutorials/C3_Dynamic_Templates
   tutorials/C4_URL
   tutorials/C5_Commands

.. toctree::
   :maxdepth: 1
   :caption: Advanced Tutorials

   tutorials/D1_Dependencies
   tutorials/D2_Jupyter_Magic
   tutorials/D3_Decorators
   tutorials/D4_Complex_Serialisation
   tutorials/D5_Functionless_Datasets

.. toctree::
   :maxdepth: 1
   :caption: Misc Tutorials

   tutorials/E1_Removing_Attributes
   tutorials/E2_Transport
   tutorials/E3_Logging
   tutorials/E4_Verbosity
   tutorials/E5_Flags
   tutorials/E6_Function_Tools
   tutorials/E7_Changing_Transport

.. toctree::
   :maxdepth: 1
   :caption: Interoperability

   tutorials/F1_JUBETemplate

The following page contains a compilation of patch notes:

.. toctree::
   :maxdepth: 1
   :caption: Version History

   versions

.. toctree::
   :maxdepth: 1
   :caption: Legacy Tutorials

   tutorials/legacy/L1_BaseComputer
   tutorials/legacy/L1a_Updating_Computers
   tutorials/legacy/L2_Parsers
   tutorials/legacy/L3_Serialisation

See the generated API docs for specific queries and advanced usage:

.. toctree::
   :maxdepth: 4
   :caption: API Documentation

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
