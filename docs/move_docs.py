import glob
import shutil
from typing import Union

import click
import os


@click.command()
@click.argument("i")
@click.argument("o")
@click.option("-f", "force", is_flag=True, help="Force operation")
def move(i, o, force=False):
    """
    Simple script to move a documentation page,
    updating all links across the tutorials dir

    Call with python move_docs.py <inpfile> <outputfile>

    For example, to move "Docs Page.ipynb"

    >>> python move.py "Docs Page.ipynb" Output_location

    Args:
        i: input file
        o: target location
        force: ignore checks

    Returns:
        None
    """
    sourcedir = "source/tutorials"

    i_clean = i.split(".")[0]
    o_clean = o.split(".")[0]

    i_file = os.path.join(sourcedir, i_clean + ".ipynb")
    o_file = os.path.join(sourcedir, o_clean + ".ipynb")

    i_html = (i_clean + ".html").replace(" ", "%20")
    o_html = (o_clean + ".html").replace(" ", "%20")

    print(f"moving {i_file} > {o_file}")
    print(f"exchanging html string {i_html} > {o_html}")

    if not os.path.exists(i_file) and not force:
        raise ValueError(f"file {i} does not exist")

    if os.path.exists(o_file) and not force:
        raise ValueError(f"target {o} already exists")

    replace_in_file(i_clean, o_clean, "source/index.rst")

    files = glob.glob(sourcedir + "/*.ipynb")
    files += ["source/FAQ.ipynb", "source/Introduction.ipynb"]
    for f in files:
        print(f" {f}")
        replace_in_file(i_html, o_html, f)

    shutil.move(i_file, o_file)


def replace_in_file(a: str, b: str, f_in: str, f_out: Union[str, None] = None):
    # print(f"replacing {a} > {b}")
    with open(f_in) as o:
        inp = o.read()

    out = inp.replace(a, b)

    outfile = f_out or f_in
    with open(outfile, "w+") as o:
        o.write(out)


if __name__ == "__main__":
    move()
