import glob
import os
import shutil


def removefile(fname):
    try:
        os.remove(fname)
        return True
    except FileNotFoundError:
        return False


def _wipe_testing_areas(clear_coverage: bool = False, verbose=True):
    nfiles = 0
    if clear_coverage:
        # remove any lingering --cov-append files
        for file in glob.glob(".coverage*"):
            if file != ".coveragerc":
                os.remove(file)
                nfiles += 1
                if verbose:
                    print(f"removed coverage file {file}")

    rundir = os.getcwd()

    for root, dirs, files in os.walk(rundir):
        if "tests" not in root and "tutorials" not in root:
            continue

        for dir in dirs:
            if dir.startswith("temp_"):
                fullpath = os.path.join(root, dir)

                if verbose:
                    print(f"removing dir {fullpath}")

                shutil.rmtree(fullpath)

        wipe_patterns = {
            "start": ["temp_", "tmp_", "dataset-"],
            "end": [".zip", ".in", ".out", ".pack", ".old", ".log"],
        }
        for file in files:
            wipe = False
            for criteria, terms in wipe_patterns.items():
                for pattern in terms:
                    if criteria == "start" and file.startswith(pattern):
                        wipe = True
                    elif criteria == "end" and file.endswith(pattern):
                        wipe = True

            if wipe:
                fullpath = os.path.join(root, file)

                if verbose:
                    print(f"removing file {fullpath}")
                removefile(fullpath)
                nfiles += 1

        removefile(".passfile")

    if not verbose:
        print(f"cleaned up {nfiles} files")


def pytest_sessionstart(session):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.

    Important to clean out any testing folders
    """
    print("Pre-testing setup begins")
    _wipe_testing_areas(clear_coverage=True)


def pytest_sessionfinish(session, exitstatus):
    """
    Clear out folders after the testing sessions finishes
    """
    print("\nPost-testing cleanup begins")
    _wipe_testing_areas()


def pytest_collectstart(collector):
    if collector.fspath and collector.fspath.ext == ".ipynb":
        collector.skip_compare += (
            "text/html",
            "application/javascript",
            "stderr",
        )


if __name__ == "__main__":
    print("testing pytest init")
    pytest_sessionstart(None)
