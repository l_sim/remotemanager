"""
Basic test to ensure that a version bump does not break a run
"""

import sys
import time

from remotemanager import Dataset


def f(x):
    """Function stub"""
    # pylint: disable=import-outside-toplevel, reimported, redefined-outer-name
    import time

    time.sleep(5)
    return x


def run():
    """Perform a simple run"""
    ds = Dataset(f)

    for i in range(5):
        ds.append_run({"x": i})

    t0 = time.time()
    ds.run()
    ds.wait(1, 10)
    ds.fetch_results()
    dt = t0 - time.time()
    return ds.results, dt, ds


if __name__ == "__main__":
    results, dt, ds = run()
    assert results == [0, 1, 2, 3, 4]
    # on the 2nd check, make sure that we're restoring from the database
    if sys.argv[1] == "recheck":
        # clean the files first
        ds.hard_reset()
        assert dt < 1
