"""
Basic test to ensure that a version bump does not break a run
"""

import sys

from remotemanager import Computer

template = """
a = #a:default=36#
b = #b:default={a*100}#

c = #c:default={b}:format=time#

nodefault = #nodefault:optional=False#
check_dynamic = #count:default={nodefault*2}#
"""


def run(recheck=False):
    test = Computer(template=template)

    if not recheck:
        with open("test_version_bump_script.txt", "w+") as o:
            o.write(test.script(nodefault=21))

    else:
        with open("test_version_bump_script.txt", "w+") as o:
            old = o.read()

        assert old == test.script(nodefault=21)


if __name__ == "__main__":
    run(recheck=False)
    if sys.argv[1] == "recheck":
        run(recheck=True)
