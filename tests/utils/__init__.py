import psutil


def pid_exists(PID):
    return psutil.pid_exists(PID)
