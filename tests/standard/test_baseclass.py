import os.path
import time

from tests.utils.base_class import BaseTestClass


def foo(inp):
    """test_baseclass"""
    return inp


def bar(b):
    """test_baseclass"""
    return b


class TestMeta(BaseTestClass):

    def test_recreate_ds(self):
        self.create_dataset(foo)
        self.ds.append_run({"inp": 1})
        self.run_ds()

        t_run = self.ds.runners[0].last_submitted

        new = self.recreate_previous_dataset()

        print(new.runners)

        assert self.ds == new

        time.sleep(0.5)

        new.run()
        assert new.runners[0].last_submitted == t_run

    def test_dependent_ressurect(self):
        """
        Tests for a recurring bug where dependent datasets could leak files
        """
        a, b = self.create_datasets([foo, bar])

        dbfile_a = a.dbfile
        dbfile_b = b.dbfile

        self.tearDown()

        assert not os.path.isfile(dbfile_a)
        assert not os.path.isfile(dbfile_b)
