from tests.utils.base_class import BaseTestClass


def func(x, y):
    # test_run.py
    return x * y


class TestRuns(BaseTestClass):
    def test_basic(self):
        self.create_dataset(func)
        self.ds.append_run({"x": 10, "y": 7})

        results = self.run_ds()

        assert results == [70]

    def test_multi(self):
        self.create_dataset(func)

        for i in range(5):
            self.ds.append_run({"x": i, "y": 10})

        results = self.run_ds()

        assert results == [0, 10, 20, 30, 40]
