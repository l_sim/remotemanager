import re

import pytest

from remotemanager import URL
from tests.utils.base_class import BaseTestClass


def f(inp):
    # test_force_collect
    return inp


class TestForceFinished(BaseTestClass):
    def test_is_finished(self):
        url = URL(shell="foo")

        self.create_dataset(f, url=url)

        self.ds.append_run({"ino": 1})

        self.ds.run()

        match = re.compile(r"Dataset encountered an issue:\n.*foo: command not found", re.MULTILINE)
        with pytest.raises(RuntimeError, match=match):
            self.ds.wait(0.1, 1)

        with pytest.raises(RuntimeError, match=r".*wait timed out.*"):
            self.ds.wait(0.1, 0.5, force=True)

        assert not self.ds._is_finished(force=True)[0]
        assert not self.ds.is_finished_force[0]
        with pytest.raises(RuntimeError, match=match):
            assert not self.ds.is_finished[0]
