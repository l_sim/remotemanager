import os.path

from tests.utils.base_class import BaseTestClass


def func(x, y):
    # test_dirs.py
    return x * y


class TestRemoteDir(BaseTestClass):
    def test_basic(self):
        self.create_dataset(func, remote_dir="test_remote_basic")
        self.ds.append_run({"x": 10, "y": 7})

        results = self.run_ds()

        assert results == [70]
        assert os.path.exists(self.ds.remote_dir)

    def test_multi(self):
        self.create_dataset(func, remote_dir="test_remote_multi")

        for i in range(5):
            self.ds.append_run({"x": i, "y": 10})

        results = self.run_ds()

        assert results == [0, 10, 20, 30, 40]
        assert os.path.exists(self.ds.remote_dir)


class TestRunDir(BaseTestClass):
    def test_basic(self):
        self.create_dataset(func, run_dir="test_run_basic")
        self.ds.append_run({"x": 10, "y": 7})

        results = self.run_ds()

        assert results == [70]
        assert os.path.exists(os.path.join(self.ds.remote_dir, self.ds.run_dir))

    def test_multi(self, remote_dir="test_remote"):
        self.create_dataset(func, run_dir="test_run_multi")

        for i in range(5):
            self.ds.append_run({"x": i, "y": 10})

        results = self.run_ds()

        assert results == [0, 10, 20, 30, 40]
        assert os.path.exists(os.path.join(self.ds.remote_dir, self.ds.run_dir))
