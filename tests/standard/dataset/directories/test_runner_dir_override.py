from tests.utils.base_class import BaseTestClass


def mult(x, y):
    # test_runner_dir_override
    return x * y


class TestRunnerDirOverrides(BaseTestClass):

    def test_remote_override(self):
        self.create_dataset(mult)

        remote_dir = self.random_string()
        print(f"remote_dir={remote_dir}")

        self.ds.append_run({"x": 1, "y": 1}, remote_dir=remote_dir)

        assert self.ds.runners[0].remote_dir == remote_dir

        result = self.run_ds()
        assert result[0] == 1

    def test_run_override(self):
        self.create_dataset(mult)

        run_dir = self.random_string()
        print(f"run_dir={run_dir}")

        self.ds.append_run({"x": 1, "y": 1}, run_dir=run_dir)

        assert self.ds.runners[0].run_dir == run_dir

        result = self.run_ds()
        assert result[0] == 1

    def test_both_override(self):
        self.create_dataset(mult)

        remote_dir = self.random_string()
        run_dir = self.random_string()
        print(f"remote_dir={remote_dir}")
        print(f"run_dir={run_dir}")

        self.ds.append_run({"x": 1, "y": 1}, remote_dir=remote_dir, run_dir=run_dir)

        assert self.ds.runners[0].remote_dir == remote_dir
        assert self.ds.runners[0].run_dir == run_dir

        result = self.run_ds()
        assert result[0] == 1
