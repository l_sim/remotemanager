from remotemanager.connection.url import URL
from tests.utils.base_class import BaseTestClass


def f(a, b):
    # test_submitter_with_flags
    return a + b


class TestSubmitterWithFlags(BaseTestClass):
    def test_submitter_with_flags(self):
        url = URL(submitter="oarsub -S")

        self.create_dataset(function=f, url=url)

        self.ds.append_run({"a": 35, "b": 7})

        self.ds.run(dry_run=True)

        repo_content = self.ds.bash_repo.content
        mstr_content = self.ds.master_script.content
        assert "oarsub -S" in repo_content
        assert "oarsub -S" not in mstr_content
        assert "submit_job_oarsub ()" in repo_content
