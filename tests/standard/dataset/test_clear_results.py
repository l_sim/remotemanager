from tests.utils.base_class import BaseTestClass


def fn(inp):
    # test_clear_results.py
    return inp


class TestClearResults(BaseTestClass):

    def test_clear_single(self):
        self.create_dataset(fn)

        self.ds.append_run({"inp": "foo"})

        results = self.run_ds()

        assert results == ["foo"]

        self.ds.reset_runs(wipe=True)

        assert self.ds.results == [None]

    def test_clear_multi(self):
        self.create_dataset(fn)

        self.ds.append_run({"inp": "a"})
        self.ds.append_run({"inp": "b"})

        results = self.run_ds()

        assert results == ["a", "b"]

        self.ds.reset_runs(wipe=True)

        assert self.ds.results == [None, None]
