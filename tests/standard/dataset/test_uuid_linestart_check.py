"""
Checks the manifest parsing commodity function
"""

import random

from remotemanager.dataset.dataset import line_starts_with_uuid
from remotemanager.utils.uuid import generate_uuid


def test_positive():
    uuid = generate_uuid(str(random.randint(0, 128)))[:8]

    string = f"{uuid} foo"

    print(f"checking with string: {string}")

    assert line_starts_with_uuid(string)


def test_negative():
    uuid = generate_uuid(str(random.randint(0, 128)))[:8]

    string = f"foo {uuid}"

    print(f"checking with string: {string}")

    assert not line_starts_with_uuid(string)
