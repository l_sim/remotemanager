"""
test to ensure that ds.copy_runners creates new runners
"""
import sys

import pytest

from tests.utils.base_class import BaseTestClass

versionskip = sys.version_info.major <= 3 and sys.version_info.minor <= 7


def add(a, b):
    # test_copy_runner
    return a + b


@pytest.mark.skipif(versionskip, reason="These tests are unreliable in the 3.7 suite")
class TestCopyRunner(BaseTestClass):

    def test_basic(self):
        # create 2 datasets
        self.create_dataset(add, name="1")
        self.create_dataset(add, name="2")

        ds1 = self.datasets[0]
        ds2 = self.datasets[1]

        ds1.append_run({"a": 1, "b": 1})

        ds2.copy_runners(ds1)

        assert ds1.runners[0] is not ds2.runners[0]

        ds2.run()

        ds2.wait(0.1, 2)
        ds2.fetch_results()

        assert ds2.results == [2]

    def test_run(self):
        # create 2 datasets
        self.create_dataset(add, name="1")
        self.create_dataset(add, name="2")

        ds1 = self.datasets[0]
        ds2 = self.datasets[1]

        ds1.append_run({"a": 1, "b": 1})

        ds1.run()

        ds2.copy_runners(ds1)

        assert ds1.runners[0] is not ds2.runners[0]

        ds2.run(force=True, force_ignores_success=True)

        ds2.wait(0.1, 2)
        ds2.fetch_results()

        assert ds2.results == [2]
