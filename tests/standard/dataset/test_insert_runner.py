from tests.utils.base_class import BaseTestClass


def broken(inp):
    raise ValueError("foo")
    return inp


def fixed(inp):
    return inp


class TestInsertRunner(BaseTestClass):
    def test_insert_many(self):
        self.create_dataset(broken)

        for i in range(5):
            self.ds.append_run({"inp": i})

        self.run_ds()

        assert self.ds.failed

        self.create_dataset(fixed, remote_dir=self.ds.remote_dir)

        for runner in self.datasets[0].runners:
            self.ds.insert_runner(runner)

        assert len(self.datasets[0].runners) == len(self.datasets[1].runners)

        results = self.run_ds(force=True, force_ignores_success=True)

        assert results == [0, 1, 2, 3, 4]

    def test_copy_many(self):
        self.create_dataset(broken)

        for i in range(5):
            self.ds.append_run({"inp": i})

        self.run_ds()

        assert self.ds.failed

        self.create_dataset(fixed, remote_dir=self.ds.remote_dir)

        self.ds.copy_runners(self.datasets[0])

        assert len(self.datasets[0].runners) == len(self.datasets[1].runners)

        results = self.run_ds(force=True, force_ignores_success=True)

        assert results == [0, 1, 2, 3, 4]
