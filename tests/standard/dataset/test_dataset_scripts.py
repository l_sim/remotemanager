"""
Collection Dataset script based tests
"""

from remotemanager import URL
from tests.utils.base_class import BaseTestClass


def add(a, b):
    # test_dataset_scripts
    return a + b


class TestDatasetScripts(BaseTestClass):

    def test_multiple_submitters(self):
        url = URL(submitter="sub_cmd")
        self.create_dataset(add, url=url)

        self.ds.append_run({"a": 40, "b": 1})
        self.ds.append_run({"a": 40, "b": 2}, avoid_nodes=True)

        self.ds.run(dry_run=True)

        assert self.ds.url.submitter in self.ds.master_script.content
        assert self.ds.url.shell in self.ds.master_script.content

    def test_newline_disable(self):
        """Check that we can disable the newline"""
        self.create_dataset(add)
        self.ds.append_run({"a": 40, "b": 1})
        self.ds.run(dry_run=True)

        assert self.ds.master_script.content.endswith("\n")

        self.create_dataset(add, add_newline=False)
        self.ds.append_run({"a": 40, "b": 1})
        self.ds.run(dry_run=True)

        assert not self.ds.master_script.content.endswith("\n")
