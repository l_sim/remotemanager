from remotemanager.connection.url import URL
from remotemanager.dataset.dataset import Dataset
from tests.utils.base_class import BaseTestClass


def f(i):
    # test_default_url

    return i


class TestURLDefaulting(BaseTestClass):

    def test_change_default_url(self):
        stored_url = Dataset.default_url
        try:
            url = URL("user@host")

            Dataset.default_url = url
            self.create_dataset(f)
            assert self.ds.url.host == url.host
        finally:
            Dataset.default_url = stored_url
