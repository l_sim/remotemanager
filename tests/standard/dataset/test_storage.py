import pytest

from remotemanager.connection.url import URL
from remotemanager.dataset.dataset import Dataset
from tests.utils.base_class import BaseTestClass


def func(a):
    return a


def newfn():
    return


class TestStorage(BaseTestClass):

    def test_from_file(self):
        self.create_dataset(func, url=URL())

        new = Dataset.from_file(self.ds.dbfile, url=URL())

        assert new == self.ds

    def test_from_file_no_url(self):
        self.create_dataset(func)

        new = Dataset.from_file(self.ds.dbfile)

        assert new == self.ds

    def test_store_run_args(self):
        self.create_dataset(func, foo="arg")

        new = Dataset.from_file(self.ds.dbfile)

        assert new.run_args["foo"] == "arg"

    def test_recreate(self):
        self.create_dataset(func)
        old_id = hex(id(self.ds))

        new = Dataset.recreate(func, **self.previous_ds_kwargs)
        new_id = hex(id(new))

        self.destroy_dataset(new)

        assert old_id == new_id

    def test_recreate_new_noraise(self):
        self.create_dataset(func)
        old_id = hex(id(self.ds))

        new = Dataset.recreate(newfn, raise_if_not_found=False, **self.previous_ds_kwargs)
        new_id = hex(id(new))

        self.destroy_dataset(new)

        assert old_id != new_id

    @pytest.mark.skip(reason="The gc iteration initialises dataset objects that it finds, which leaks files")
    def test_recreate_new(self):
        self.create_dataset(func)

        with pytest.raises(ValueError, match="Dataset with args not found!"):
            Dataset.recreate(newfn, raise_if_not_found=True, **self.previous_ds_kwargs)
