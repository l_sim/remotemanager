import os
import shutil

from tests.utils.base_class import BaseTestClass


def move(i: str, o: str) -> str:
    # test_abspaths
    with open(i, "r") as ofile:
        data = ofile.read()
    with open(o, "w+") as ofile:
        ofile.write(data)

    return f"copied {i} to {o}"


class TestAbsPaths(BaseTestClass):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        home = os.path.expandvars("$HOME")
        self.testing_root = os.path.join(home, "test_abspaths")

        self.directories = [
            os.path.join(self.testing_root, "0"),
            os.path.join(self.testing_root, "1"),
            os.path.join(self.testing_root, "2"),
            os.path.join(self.testing_root, "3"),
        ]

    def setUp(self):
        for dir in self.directories:
            os.makedirs(dir)

        super().setUp()

    def tearDown(self, *args, **kwargs):
        try:
            shutil.rmtree(self.testing_root)
        except FileNotFoundError:
            pass

        super().tearDown()

    def test_abspath(self):
        """
        Test moving a file across 4 directories using abspaths

        - 1: Source
        - 2: run target
        - 3: "remote" copy to
        - 4: retrieval
        """

        self.create_dataset(move)

        paths = [os.path.join(d, "file.txt") for d in self.directories]

        for path in paths:
            exists = os.path.isfile(path)
            print(f"path {path} exists?: {exists}")
            assert not exists

        with open(paths[0], "w+") as o:
            o.write("content")

        self.ds.append_run(
            {"i": paths[0], "o": paths[2]},
            extra_files_send=[{paths[0]: self.directories[1]}],
            extra_files_recv=[{paths[3]: self.directories[2]}]
        )

        print(self.run_ds())

        assert self.ds.runners[0].extra_files_send[0].local == paths[0]
        assert self.ds.runners[0].extra_files_send[0].remote == paths[1]
        assert self.ds.runners[0].extra_files_recv[0].remote == paths[2]
        assert self.ds.runners[0].extra_files_recv[0].local == paths[3]

        for path in paths:
            exists = os.path.isfile(path)
            print(f"path {path} exists?: {exists}")
            assert exists

        with open(paths[-1]) as o:
            assert o.read() == "content"
