import os

from remotemanager.storage import TrackedFile
from tests.utils.base_class import BaseTestClass


def read(fname):
    # test_extra_files
    with open(fname) as o:
        return o.read()


class TestExtraFiles(BaseTestClass):

    def test_on_dataset(self):
        f_rel = self.create_random_file(content="1")
        f_abs = os.path.abspath(self.create_random_file(content="2"))

        self.create_dataset(read, extra_files_send=[f_rel, f_abs])

        self.ds.append_run({"fname": f_rel})
        self.ds.append_run({"fname": f_abs})

        assert self.run_ds() == ["1", "2"]

    def test_on_runner(self):
        f_rel = self.create_random_file(content="1")
        f_abs = os.path.abspath(self.create_random_file(content="2"))

        self.create_dataset(read)

        self.ds.append_run({"fname": f_rel}, extra_files_send=[f_rel])
        self.ds.append_run({"fname": f_abs}, extra_files_send=[f_abs])

        assert self.run_ds() == ["1", "2"]

    def test_on_run(self):
        f_rel = self.create_random_file(content="1")
        f_abs = os.path.abspath(self.create_random_file(content="2"))

        self.create_dataset(read)

        self.ds.append_run({"fname": f_rel})
        self.ds.append_run({"fname": f_abs})

        assert self.run_ds(extra_files_send=[f_rel, f_abs]) == ["1", "2"]


class TestExtraFilesDict(BaseTestClass):

    def test_on_dataset(self):
        f_rel = self.create_random_file(content="1")
        f_abs = os.path.abspath(self.create_random_file(content="2"))

        self.create_dataset(read, extra_files_send=[{f_rel: ""}, {f_abs: ""}])

        self.ds.append_run({"fname": f_rel})
        self.ds.append_run({"fname": f_abs})

        assert self.run_ds() == ["1", "2"]

    def test_on_runner(self):
        f_rel = self.create_random_file(content="1")
        f_abs = os.path.abspath(self.create_random_file(content="2"))

        self.create_dataset(read)

        self.ds.append_run({"fname": f_rel}, extra_files_send=[{f_rel: ""}])
        self.ds.append_run({"fname": f_abs}, extra_files_send=[{f_abs: ""}])

        assert self.run_ds() == ["1", "2"]

    def test_on_run(self):
        f_rel = self.create_random_file(content="1")
        f_abs = os.path.abspath(self.create_random_file(content="2"))

        self.create_dataset(read)

        self.ds.append_run({"fname": f_rel})
        self.ds.append_run({"fname": f_abs})

        assert self.run_ds(extra_files_send=[{f_rel: ""}, {f_abs: ""}]) == ["1", "2"]

    def test_inner_on_runner(self):
        f1 = self.create_random_file(content="1")
        f2 = self.create_random_file(content="2")
        f2_abspath = os.path.abspath(f2)

        self.create_dataset(read)

        self.ds.append_run({"fname": "inner_dir/" + f1}, extra_files_send=[{f1: "inner_dir"}])
        self.ds.append_run({"fname": "inner_dir/" + f2}, extra_files_send=[{f2_abspath: "inner_dir"}])

        assert self.run_ds() == ["1", "2"]

    def test_inner_on_run(self):
        f1 = self.create_random_file(content="1")
        f2 = self.create_random_file(content="2")
        f2_abspath = os.path.abspath(f2)

        self.create_dataset(read)

        self.ds.append_run({"fname": "inner_dir/" + f1})
        self.ds.append_run({"fname": "inner_dir/" + f2})

        assert self.run_ds(extra_files_send=[{f1: "inner_dir/"}, {f2_abspath: "inner_dir/"}]) == ["1", "2"]

    def test_changedir_on_run(self):
        f1 = self.create_random_file(content="1")
        f2 = self.create_random_file(content="2")
        f2_abspath = os.path.abspath(f2)

        self.create_dataset(read)

        newdir = f"temp_remote_{self.random_string()}"
        print(f"changing dirs from {self.ds.remote_dir} to {newdir}")

        self.ds.append_run({"fname": f1})
        self.ds.append_run({"fname": f2_abspath})

        assert self.run_ds(remote_dir=newdir, extra_files_send=[{f1: ""}, {f2_abspath: ""}]) == ["1", "2"]

        assert f1 in os.listdir(newdir)
        assert f2 in os.listdir(newdir)

    def test_changedir_inner_on_run(self):
        f1 = self.create_random_file(content="1")
        f2 = self.create_random_file(content="2")
        f2_abspath = os.path.abspath(f2)

        self.create_dataset(read)

        newdir = f"temp_remote_{self.random_string()}"
        print(f"changing dirs from {self.ds.remote_dir} to {newdir}")

        self.ds.append_run({"fname": "inner_dir/" + f1})
        self.ds.append_run({"fname": "inner_dir/" + f2})

        assert self.run_ds(remote_dir=newdir, extra_files_send=[{f1: "inner_dir/"}, {f2_abspath: "inner_dir/"}]) == ["1", "2"]

        assert f1 in os.listdir(os.path.join(newdir, "inner_dir"))
        assert f2 in os.listdir(os.path.join(newdir, "inner_dir"))

    def test_ignored_paths(self):
        """
        Checks for a bug where specifying paths and adding without a list
        would cause ensure_list to mangle the path
        """
        self.create_dataset(read, extra_files_send={"test.txt": "inner_dir"})

        assert self.ds.extra_files_send[0].remote == os.path.join(self.ds.remote_dir, "inner_dir/test.txt")


class TestExtraTrackedFiles(BaseTestClass):

    def test_on_runner(self):

        self.create_dataset(read)

        f1 = TrackedFile(".", self.ds.remote_dir, self.create_random_file())
        f2 = TrackedFile(".", os.path.abspath(self.ds.remote_dir), self.create_random_file())

        f1.write("1", add_newline=False)
        f2.write("2", add_newline=False)

        self.ds.append_run({"fname": f1.name}, extra_files_send=[f1])
        self.ds.append_run({"fname": f2.name}, extra_files_send=[f2])

        assert self.run_ds() == ["1", "2"]

    def test_on_run(self):

        self.create_dataset(read)

        f1 = TrackedFile(".", self.ds.remote_dir, self.create_random_file())
        f2 = TrackedFile(".", os.path.abspath(self.ds.remote_dir), self.create_random_file())

        f1.write("1", add_newline=False)
        f2.write("2", add_newline=False)

        self.ds.append_run({"fname": f1.name})
        self.ds.append_run({"fname": f2.name})

        assert self.run_ds(extra_files_send=[f1, f2]) == ["1", "2"]

    def test_inner_on_runner(self):

        self.create_dataset(read)

        f1 = TrackedFile(".", os.path.join(self.ds.remote_dir, "inner_dir"), self.create_random_file())
        f2 = TrackedFile(".", os.path.abspath(os.path.join(self.ds.remote_dir, "inner_dir")), self.create_random_file())

        f1.write("1", add_newline=False)
        f2.write("2", add_newline=False)

        self.ds.append_run({"fname": "inner_dir/" + f1.name}, extra_files_send=[f1])
        self.ds.append_run({"fname": "inner_dir/" + f2.name}, extra_files_send=[f2])

        assert self.run_ds() == ["1", "2"]

    def test_inner_on_run(self):

        self.create_dataset(read)

        f1 = TrackedFile(".", os.path.join(self.ds.remote_dir, "inner_dir"), self.create_random_file())
        f2 = TrackedFile(".", os.path.abspath(os.path.join(self.ds.remote_dir, "inner_dir")), self.create_random_file())

        f1.write("1", add_newline=False)
        f2.write("2", add_newline=False)

        self.ds.append_run({"fname": "inner_dir/" + f1.name})
        self.ds.append_run({"fname": "inner_dir/" + f2.name})

        assert self.run_ds(extra_files_send=[f1, f2]) == ["1", "2"]
