import os

from remotemanager.storage import TrackedFile
from tests.utils.base_class import BaseTestClass


def write(fname):
    # test_extra_files
    with open(fname, "w+") as o:
        return o.write("foo")


class TestExtraFiles(BaseTestClass):

    def test_on_dataset(self):
        f_rel = self.create_random_file()
        f_abs = os.path.abspath(self.create_random_file())
        self.create_dataset(write, extra_files_recv=[f_rel, f_abs])

        self.ds.append_run({"fname": f_rel})
        self.ds.append_run({"fname": f_abs})

        self.run_ds()

        assert self.ds.runners[0].extra_files_recv[0].content == "foo"
        assert self.ds.runners[1].extra_files_recv[0].content == "foo"

    def test_on_runner(self):
        f_rel = self.create_random_file()
        f_abs = os.path.abspath(self.create_random_file())
        self.create_dataset(write)

        self.ds.append_run({"fname": f_rel}, extra_files_recv=[f_rel])
        self.ds.append_run({"fname": f_abs}, extra_files_recv=[f_abs])

        self.run_ds()

        assert self.ds.runners[0].extra_files_recv[0].content == "foo"
        assert self.ds.runners[1].extra_files_recv[0].content == "foo"

    def test_on_run(self):
        f_rel = self.create_random_file()
        f_abs = os.path.abspath(self.create_random_file())
        self.create_dataset(write)

        self.ds.append_run({"fname": f_rel})
        self.ds.append_run({"fname": f_abs})

        self.run_ds(extra_files_recv=[f_rel, f_abs])

        assert self.ds.runners[0].extra_files_recv[0].content == "foo"
        assert self.ds.runners[1].extra_files_recv[0].content == "foo"


class TestExtraTrackedFiles(BaseTestClass):

    def test_on_runner(self):
        self.create_dataset(write)

        f_rel = TrackedFile(".", self.ds.remote_dir, self.create_random_file())
        f_abs = TrackedFile(".", os.path.abspath(self.ds.remote_dir), self.create_random_file())

        self.ds.append_run({"fname": f_rel.name}, extra_files_recv=[f_rel])
        self.ds.append_run({"fname": f_abs.name}, extra_files_recv=[f_abs])

        self.run_ds()

        assert self.ds.runners[0].extra_files_recv[0].content == "foo"
        assert self.ds.runners[1].extra_files_recv[0].content == "foo"

    def test_on_run(self):
        self.create_dataset(write)

        f_rel = TrackedFile(".", self.ds.remote_dir, self.create_random_file())
        f_abs = TrackedFile(".", os.path.abspath(self.ds.remote_dir), self.create_random_file())

        self.ds.append_run({"fname": f_rel.name})
        self.ds.append_run({"fname": f_abs.name})

        self.run_ds(extra_files_recv=[f_rel, f_abs])

        assert self.ds.runners[0].extra_files_recv[0].content == "foo"
        assert self.ds.runners[1].extra_files_recv[0].content == "foo"
