from remotemanager.storage import TrackedFile
from tests.utils.base_class import BaseTestClass


def f():
    # test_extra_file_conversion
    return


class TestFileConversion(BaseTestClass):

    def test_dataset_add(self):
        file = self.create_random_file()

        self.create_dataset(f, extra_files_send=file)
        assert isinstance(self.ds.extra_files["send"][0], TrackedFile)

    def test_runner_add(self):
        file = self.create_random_file()

        self.create_dataset(f)
        self.ds.append_run(extra_files_send=file)
        assert isinstance(self.ds.runners[0].extra_files_send[0], TrackedFile)

    def test_dataset_remote(self):
        file = self.create_random_file()

        self.create_dataset(f, extra_files_send=file)
        assert self.ds.extra_files["send"][0].remote_dir == self.ds.remote_dir

    def test_dataset_remote_change(self):
        file = self.create_random_file()

        self.create_dataset(f, extra_files_send=file)
        assert self.ds.extra_files["send"][0].remote_dir == self.ds.remote_dir

        self.ds.remote_dir = "new_remote"
        assert self.ds.extra_files["send"][0].remote_dir == self.ds.remote_dir
