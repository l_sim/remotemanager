from remotemanager.connection.computers.computer import Computer
from remotemanager.script.script import Script
from tests.utils.base_class import BaseTestClass


class TestScriptRun(BaseTestClass):

    def test_echo(self):
        run = "echo #foo#"

        self.create_dataset(function=Script(template=run))
        self.ds.append_run({"foo": "hello world"})

        results = self.run_ds()

        assert results[0] == "hello world"

    def test_many(self):
        run = "echo #foo#"

        self.create_dataset(function=Script(template=run))

        for i in range(5):
            self.ds.append_run({"foo": i})

        results = self.run_ds()

        assert results == [str(num) for num in range(5)]

    def test_combo(self):
        run = "echo $(( env_mult * #val# ))"
        job = "export env_mult=#env_mult#"

        self.create_dataset(function=Script(template=run), url=Computer(template=job))
        self.ds.append_run({"val": 4})
        results = self.run_ds(env_mult=4)

        assert results[0] == "16"

    def test_combo_append(self):
        run = "echo $(( #val# * env_mult ))"
        job = "export env_mult=#env_mult#"

        self.create_dataset(function=Script(template=run), url=Computer(template=job), env_mult=4)
        self.ds.append_run({"val": 4})
        results = self.run_ds()

        assert results[0] == "16"

    def test_combo_dataset(self):
        run = "echo $(( #val# * env_mult ))"
        job = "export env_mult=#env_mult#"

        self.create_dataset(function=Script(template=run), url=Computer(template=job))
        self.ds.append_run({"val": 4}, env_mult=4)
        results = self.run_ds()

        assert results[0] == "16"
