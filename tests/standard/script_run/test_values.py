import pytest

from remotemanager.script.script import Script
from tests.utils.base_class import BaseTestClass


class TestScriptRun(BaseTestClass):

    def test_missing(self):
        run = "echo #foo#\necho #bar#"

        self.create_dataset(function=Script(template=run))
        self.ds.append_run({"foo": "hello world"})

        results = self.run_ds()

        assert results[0] == "hello world"

    def test_required(self):
        run = "echo #foo#\necho #bar:optional=False#"

        self.create_dataset(function=Script(template=run))
        self.ds.append_run({"foo": "hello world"})

        with pytest.raises(ValueError):
            self.run_ds()

    def test_default(self):
        run = "echo #foo#\necho #bar:default=default#"

        self.create_dataset(function=Script(template=run))
        self.ds.append_run({"foo": "hello world"})

        results = self.run_ds()

        assert results[0] == "hello world\ndefault"
