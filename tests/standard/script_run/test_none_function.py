"""
Collection Dataset script based tests
"""

from remotemanager import Computer
from tests.utils.base_class import BaseTestClass


class TestDatasetScripts(BaseTestClass):

    def test_none_function(self):
        template = """
echo "#FOO#"
"""

        url = Computer(template=template)

        self.create_dataset(function=None, url=url)

        self.ds.append_run({"foo": "bar"})

        results = self.run_ds()

        assert results[0] == "bar"
