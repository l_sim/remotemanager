from tests.utils.base_class import BaseTestClass


def f(a):
    return a


class TestDirectScript(BaseTestClass):
    def test_direct_script(self):
        self.create_dataset(f, script="script content")
        self.ds.append_run({"a": 1})
        self.ds.run(dry_run=True)

        print(self.ds.runners[0].jobscript.content)
        assert self.ds.script == "#!/bin/bash\nscript content"

        self.ds.script = "new script"
        assert self.ds.script == "#!/bin/bash\nnew script"
        print(self.ds.runners[0].jobscript.content)
        self.ds.run(dry_run=True)
        assert "new script" in self.ds.runners[0].jobscript.content

    def test_direct_script_after(self):
        self.create_dataset(f)
        self.ds.append_run({"a": 1})
        self.ds.run(dry_run=True)

        self.ds.script = "new script"
        assert self.ds.script == "#!/bin/bash\nnew script"
        print(self.ds.runners[0].jobscript.content)
        self.ds.run(dry_run=True)
        assert "new script" in self.ds.runners[0].jobscript.content
