import time

from tests.utils.base_class import BaseTestClass


def fn_a(x):
    return x


def fn_b(y):
    return loaded + y


def fn_c(z):
    return loaded + z


class TestDependentRuns(BaseTestClass):

    def test_basic(self):
        a, b = self.create_datasets([fn_a, fn_b])

        assert a.remote_dir == b.remote_dir

        self.ds.append_run({"x": 4, "y": 7})
        self.ds.append_run({"x": 8, "y": 7})

        results = self.run_ds()

        assert results == [11, 15]

    def test_triple(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c])

        assert a.remote_dir == b.remote_dir == c.remote_dir

        self.ds.append_run({"x": 4, "y": 7, "z": 1})
        self.ds.append_run({"x": 4, "y": 7, "z": 4})

        results = self.run_ds()

        assert results == [12, 15]

    def test_no_rerun(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c])

        self.ds.append_run({"x": 4, "y": 7, "z": 1})
        self.ds.append_run({"x": 4, "y": 7, "z": 4})

        self.run_ds()

        last_run = a.last_run

        time.sleep(1)

        a.run()
        assert a.last_run == last_run
        assert b.last_run == last_run
        assert c.last_run == last_run

        time.sleep(1)

        b.run()
        assert a.last_run == last_run
        assert b.last_run == last_run
        assert c.last_run == last_run

        time.sleep(1)

        c.run()
        assert a.last_run == last_run
        assert b.last_run == last_run
        assert c.last_run == last_run
