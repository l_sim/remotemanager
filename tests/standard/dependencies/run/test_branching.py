from tests.utils.base_class import BaseTestClass


def fn_a(x):
    return x


def fn_b(y):
    return loaded + y


def fn_c(z):
    return loaded + z


class TestBranchingRuns(BaseTestClass):

    def test_branch(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c], link=False)

        a.set_downstream(b)
        a.set_downstream(c)

        assert a.remote_dir == b.remote_dir == c.remote_dir

        self.ds.append_run({"x": 4, "y": 7, "z": 1})
        self.ds.append_run({"x": 5, "y": 8, "z": 2})

        self.run_ds()

        a.fetch_results()
        b.fetch_results()
        c.fetch_results()

        assert a.results == [4, 5]
        assert b.results == [11, 13]
        assert c.results == [5, 7]
