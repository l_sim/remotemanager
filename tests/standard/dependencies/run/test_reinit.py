from tests.utils.base_class import BaseTestClass


def fn_a(inp_a):
    # dependencies test_reinit
    return inp_a

def fn_b(inp_b):
    # dependencies test_reinit
    return inp_b + loaded


class TestDependencyReinit(BaseTestClass):

    def test_run_after(self):
        nrunners = 5

        a, b = self.create_datasets([fn_a, fn_b])

        for i in range(nrunners):
            b.append_run({"inp_a": i, "inp_b": 15})

        del a
        del b
        self.datasets = []

        a_kwargs = self.kwarg_list[0]
        a_kwargs["skip"] = True
        a = self.create_dataset(self.fn_list[0], **a_kwargs)

        b_kwargs = self.kwarg_list[1]
        b_kwargs["skip"] = True
        b = self.create_dataset(self.fn_list[1], **b_kwargs)

        a.set_downstream(b)
        assert a.dependency is b.dependency

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

        results = self.run_ds()
        assert results == [n + 15 for n in range(nrunners)]

    def test_result_persistence(self):
        nrunners = 5

        a, b = self.create_datasets([fn_a, fn_b])

        for i in range(nrunners):
            b.append_run({"inp_a": i, "inp_b": 15})

        results = self.run_ds()
        assert results == [n + 15 for n in range(nrunners)]

        runner_a_state = a.runners[0].state
        runner_b_state = b.runners[0].state

        del a
        del b
        self.datasets = []

        a_kwargs = self.kwarg_list[0]
        a_kwargs["skip"] = True
        a = self.create_dataset(self.fn_list[0], **a_kwargs)

        b_kwargs = self.kwarg_list[1]
        b_kwargs["skip"] = True
        b = self.create_dataset(self.fn_list[1], **b_kwargs)
        
        a.set_downstream(b)
        
        assert a.dependency is b.dependency

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

        assert self.ds.results == results

        assert a.runners[0].state == runner_a_state
        assert b.runners[0].state == runner_b_state
