from tests.utils.base_class import BaseTestClass


def fn_a(inp):
    return inp


def fn_b(inp):
    return loaded + inp


class TestDependentRuns(BaseTestClass):
    def test_run(self):
        a, b = self.create_datasets([fn_a, fn_b])

        self.ds.append_run({"inp": 4})
        self.ds.append_run({"inp": 5})

        assert self.run_ds() == [8, 10]
