from tests.utils.base_class import BaseTestClass


def first(a):
    # dependencies/test_run_dir
    return a


def second(b):
    # dependencies/test_run_dir
    return b + loaded


class TestRunDirUse(BaseTestClass):

    def test_in_dataset(self):
        run_dir = f"run_{self.random_string()}"
        remote_dir = f"run_{self.random_string()}"

        a, b = self.create_datasets([first, second], remote_dir=remote_dir, run_dir=run_dir)

        b.append_run({"a": 15, "b": 27})
        assert self.run_ds() == [42]

    def test_in_runner(self):
        run_dir = f"run_{self.random_string()}"
        remote_dir = f"run_{self.random_string()}"

        a, b = self.create_datasets([first, second], remote_dir=remote_dir)

        b.append_run({"a": 15, "b": 27}, run_dir=run_dir)
        assert self.run_ds() == [42]

    def test_in_run(self):
        run_dir = f"run_{self.random_string()}"
        remote_dir = f"run_{self.random_string()}"

        a, b = self.create_datasets([first, second], remote_dir=remote_dir)

        a.set_downstream(b)

        b.append_run({"a": 15, "b": 27})
        assert self.run_ds(run_dir=run_dir) == [42]
