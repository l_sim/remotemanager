import os

from tests.utils.base_class import BaseTestClass


def double(a: int) -> int:
    return a * 2


def add(b: int) -> int:
    return loaded + b


class TestSourcedirApplication(BaseTestClass):

    def test_rel_remote(self):
        path = "temp_remote_rel_remote"
        a, b = self.create_datasets([double, add], remote_dir=path)

        b.append_run({"a": 10, "b": 22})

        results = self.run_ds()

        assert results == [42]

        passed = False
        for line in b.runners[-1].jobscript.content.split("\n"):
            if "parent_result" in line and "$sourcedir" in line:
                passed = True
                break

        assert passed

    def test_abs_remote(self):
        path = os.path.join(os.getcwd(), "temp_remote_abs_remote")
        a, b = self.create_datasets([double, add], remote_dir=path)

        b.append_run({"a": 10, "b": 22})

        results = self.run_ds()

        assert results == [42]

        passed = False
        for line in b.runners[-1].jobscript.content.split("\n"):
            if "parent_result" in line and "$sourcedir" not in line:
                passed = True
                break

        assert passed
