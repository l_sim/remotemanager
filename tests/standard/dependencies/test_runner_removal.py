import pytest

from tests.utils.base_class import BaseTestClass


def fn_a(x):
    return x


def fn_b(y):
    return loaded + y


class SetupClass(BaseTestClass):

    def start(self, nrunners: int = 10, run: bool = False):
        a, b = self.create_datasets([fn_a, fn_b])

        for i in range(nrunners):
            b.append_run({"x": i, "y": i})

        if run:
            self.run_ds()

        return a, b

class TestDependentRemoval(SetupClass):

    def test_control(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_by_name(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        a.remove_run(f"runner-{nrunners-2}")

        assert len(a.runners) == nrunners - 1
        assert len(b.runners) == nrunners - 1

    def test_by_name_oob(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        with pytest.raises(RuntimeError, match=".*name, there may be an error.*"):
            a.remove_run(f"runner-{nrunners+2}")

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_by_arg(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        a.remove_run({"x": nrunners - 2, "y": nrunners - 2})

        assert len(a.runners) == nrunners - 1
        assert len(b.runners) == nrunners - 1

    def test_by_arg_nonexistent(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        a.remove_run({"x": nrunners - 1, "y": nrunners - 3})

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_by_arg_oob(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        a.remove_run({"x": nrunners + 2, "y": nrunners + 2})

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_by_uuid(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        with pytest.raises(RuntimeError, match=".*cannot be removed by uuid.*"):
            a.remove_run(a.runners[nrunners - 2].uuid)

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_by_uuid_oob(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        with pytest.raises(RuntimeError, match=".*cannot be removed by uuid.*"):
            a.remove_run(self.random_string())

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_by_short_uuid(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        with pytest.raises(RuntimeError, match=".*cannot be removed by uuid.*"):
            a.remove_run(a.runners[nrunners - 2].short_uuid)

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

    def test_hard_reset(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners)

        a.hard_reset()

        assert len(a.runners) == 0
        assert len(b.runners) == 0


class TestResetDependency(SetupClass):

    def test_run_hard_reset(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners, run=True)

        assert b.results == [n + n for n in range(nrunners)]

        a.hard_reset()

        assert len(a.runners) == 0
        assert len(b.runners) == 0

        assert b.results == []

    def test_run_reset_runs(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners, run=True)

        assert b.results == [n + n for n in range(nrunners)]

        a.reset_runs()

        assert len(a.runners) == nrunners
        assert len(b.runners) == nrunners

        assert b.results == [None] * nrunners

    def test_run_zipe_runs(self):
        nrunners = 5
        a, b = self.start(nrunners=nrunners, run=True)

        assert b.results == [n + n for n in range(nrunners)]

        a.wipe_runs()

        assert len(a.runners) == 0
        assert len(b.runners) == 0
