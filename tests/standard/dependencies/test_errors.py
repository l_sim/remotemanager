import pytest

from tests.utils.base_class import BaseTestClass


def fn_a(x, raise_a: bool = False):
    if raise_a:
        raise RuntimeError("raise_a is True")
    return x


def fn_b(y, raise_b: bool = False):
    if raise_b:
        raise RuntimeError("raise_b is True")
    return loaded + y


def fn_c(z, raise_c: bool = False):
    if raise_c:
        raise RuntimeError("raise_c is True")
    return loaded + z


class TestDependentErrors(BaseTestClass):
    def test_first(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c])

        self.ds.append_run({"x": 3, "y": 4, "z": 1, "raise_a": True})

        with pytest.raises(RuntimeError, match=".*Detected a failure in dataset .*"):
            self.run_ds()

        self.ds.fetch_results()
        assert "raise_a is True" in a.errors[0]

    def test_second(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c])

        self.ds.append_run({"x": 3, "y": 4, "z": 1, "raise_b": True})

        with pytest.raises(RuntimeError, match=".*Detected a failure in dataset .*"):
            self.run_ds()

        self.ds.fetch_results()
        assert "raise_b is True" in b.errors[0]

    def test_third(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c])

        self.ds.append_run({"x": 3, "y": 4, "z": 1, "raise_c": True})

        with pytest.raises(RuntimeError, match=".*Detected a failure in dataset .*"):
            self.run_ds()

        self.ds.fetch_results()
        assert "raise_c is True" in c.errors[0]


class TestDependentBranchingErrors(BaseTestClass):

    def test_first(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c], link=False)

        a.set_downstream(b)
        a.set_downstream(c)

        self.ds.append_run({"x": 3, "y": 4, "z": 1, "raise_a": True})

        with pytest.raises(RuntimeError, match=f".*Detected a failure in dataset.*{a.name}.*"):
            self.run_ds()

        self.ds.fetch_results()
        assert "raise_a is True" in a.errors[0]

    def test_second(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c], link=False)

        a.set_downstream(b)
        a.set_downstream(c)

        self.ds.append_run({"x": 3, "y": 4, "z": 1, "raise_b": True})

        with pytest.raises(RuntimeError, match=f".*Detected a failure in dataset.*{b.name}.*"):
            self.run_ds()

        self.ds.fetch_results()
        assert "raise_b is True" in b.errors[0]

    def test_third(self):
        a, b, c = self.create_datasets([fn_a, fn_b, fn_c], link=False)

        a.set_downstream(b)
        a.set_downstream(c)

        self.ds.append_run({"x": 3, "y": 4, "z": 1, "raise_c": True})

        with pytest.raises(RuntimeError, match=f".*Detected a failure in dataset.*{c.name}.*"):
            self.run_ds()

        self.ds.fetch_results()
        assert "raise_c is True" in c.errors[0]
