from tests.utils.base_class import BaseTestClass


def init(offset):
    return offset


def mult(x, y):
    offset = loaded
    return (x * y) + offset


def post():
    return f'result is {loaded}'


class TestDependencyProperties(BaseTestClass):
    def test_properties(self):
        a, b, c = self.create_datasets([init, mult, post])

        assert a.is_parent
        assert not a.is_child

        assert b.is_parent
        assert b.is_child

        assert not c.is_parent
        assert c.is_child

        assert a.dependency is b.dependency
        assert b.dependency is c.dependency

        assert a.dependency is not None

        assert a.dependency.get_children(a) == [b]
        assert a.dependency.get_children(b) == [c]

        assert a.dependency.get_parents(b) == [a]
        assert a.dependency.get_parents(c) == [b]

    def test_no_dependency(self):
        self.create_dataset(init)

        assert not self.ds.is_parent
        assert not self.ds.is_child
