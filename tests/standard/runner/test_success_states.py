from tests.utils.base_class import BaseTestClass


def f(fail):
    # test_success_states
    if fail:
        raise RuntimeError("fail!")


class TestSuccessStates(BaseTestClass):

    def test_duality(self):
        self.create_dataset(f)

        self.ds.append_run({"fail": False})
        self.ds.append_run({"fail": True})

        self.run_ds()

        assert self.ds.failed

        assert str(self.ds.runners[0].state) == "satisfied (success)"
        assert str(self.ds.runners[1].state) == "satisfied (failed)"

        assert self.ds.runners[0].is_success
        assert not self.ds.runners[1].is_success

        assert not self.ds.runners[0].is_failed
        assert self.ds.runners[1].is_failed
