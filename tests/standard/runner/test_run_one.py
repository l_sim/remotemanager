from tests.utils.base_class import BaseTestClass


def fn(inp):
    # test_run_one.py
    return inp


class TestClearResults(BaseTestClass):

    def test_run_one(self):
        self.create_dataset(fn)

        for i in range(5):
            self.ds.append_run({"inp": i})

        self.ds.runners[2].run()

        self.ds.wait(0.1, 2)
        self.ds.fetch_results()

        assert self.ds.results == [None, None, 2, None, None]
