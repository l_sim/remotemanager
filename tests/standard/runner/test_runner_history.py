import datetime

import pytest

from tests.utils.base_class import BaseTestClass


def f():
    # test_runner_history
    return


class TestRunnerHistory(BaseTestClass):

    def setUp(self):

        self.create_dataset(f)
        self.ds.append_run()

        self.runner = self.ds.runners[0]

        self.insert_time = datetime.datetime.now()

    def test_zero_time(self):
        with pytest.raises(ValueError):
            self.runner.insert_history(0, "test")

    def test_zero_float_time(self):
        with pytest.raises(ValueError):
            self.runner.insert_history(0.0, "test")

    def test_insert_at_time(self):
        self.runner.insert_history(self.insert_time.timestamp(), "test_insert")

        timekey = self.insert_time.strftime("%Y-%m-%d %H:%M:%S") + "/1"
        assert self.runner.history[timekey] == "test_insert"
