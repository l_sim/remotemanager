import unittest

from remotemanager.dataset.runnerstates import RunnerState


class TestInvalid(unittest.TestCase):
    def test_invalid_none(self):
        with self.assertRaises(ValueError):
            RunnerState()
    def test_invalid(self):
        with self.assertRaises(ValueError):
            RunnerState("foo")

    def test_compare_against(self):
        assert RunnerState("created") != "not_a_state"


class TestFailed(unittest.TestCase):
    def setUp(self):
        self.state = RunnerState("failed")

    def test_print(self):
        assert str(self.state) == "failed"
        assert repr(self.state) == 'RunnerState("failed")'

    def test_success(self):
        assert not self.state.success

    def test_failed(self):
        assert self.state.failed

    def test_ge(self):
        assert self.state >= RunnerState("created")
        assert self.state >= RunnerState("completed")
        assert self.state >= RunnerState("failed")
        assert not self.state >= RunnerState("satisfied")

    def test_gt(self):
        assert self.state > RunnerState("created")
        assert not self.state > RunnerState("completed")
        assert not self.state > RunnerState("failed")
        assert not self.state > RunnerState("satisfied")

    def test_le(self):
        assert not self.state <= RunnerState("created")
        assert self.state <= RunnerState("completed")
        assert self.state <= RunnerState("failed")
        assert self.state <= RunnerState("satisfied")

    def test_lt(self):
        assert not self.state < RunnerState("created")
        assert not self.state < RunnerState("completed")
        assert not self.state < RunnerState("failed")
        assert self.state < RunnerState("satisfied")

    def test_str_compare(self):
        assert self.state == RunnerState("failed")


class TestCompleted(unittest.TestCase):
    def setUp(self):
        self.state = RunnerState("completed")

    def test_print(self):
        assert str(self.state) == "completed"
        assert repr(self.state) == 'RunnerState("completed")'

    def test_success(self):
        assert self.state.success

    def test_failed(self):
        assert not self.state.failed

    def test_ge(self):
        assert self.state >= RunnerState("created")
        assert self.state >= RunnerState("completed")
        assert self.state >= RunnerState("failed")
        assert not self.state >= RunnerState("satisfied")

    def test_gt(self):
        assert self.state > RunnerState("created")
        assert not self.state > RunnerState("completed")
        assert not self.state > RunnerState("failed")
        assert not self.state > RunnerState("satisfied")

    def test_le(self):
        assert not self.state <= RunnerState("created")
        assert self.state <= RunnerState("completed")
        assert self.state <= RunnerState("failed")
        assert self.state <= RunnerState("satisfied")

    def test_lt(self):
        assert not self.state < RunnerState("created")
        assert not self.state < RunnerState("completed")
        assert not self.state < RunnerState("failed")
        assert self.state < RunnerState("satisfied")

    def test_str_compare(self):
        assert self.state == RunnerState("completed")


class TestSubmitted(unittest.TestCase):
    def setUp(self):
        self.state = RunnerState("submitted")

    def test_print(self):
        assert str(self.state) == "submitted"
        assert repr(self.state) == 'RunnerState("submitted")'

    def test_success(self):
        assert not self.state.success

    def test_failed(self):
        assert not self.state.failed

    def test_ge(self):
        assert self.state >= RunnerState("created")
        assert not self.state >= RunnerState("completed")
        assert not self.state >= RunnerState("failed")
        assert not self.state >= RunnerState("satisfied")

    def test_gt(self):
        assert self.state > RunnerState("created")
        assert not self.state > RunnerState("completed")
        assert not self.state > RunnerState("failed")
        assert not self.state > RunnerState("satisfied")

    def test_le(self):
        assert not self.state <= RunnerState("created")
        assert self.state <= RunnerState("completed")
        assert self.state <= RunnerState("failed")
        assert self.state <= RunnerState("satisfied")

    def test_lt(self):
        assert not self.state < RunnerState("created")
        assert self.state < RunnerState("completed")
        assert self.state < RunnerState("failed")
        assert self.state < RunnerState("satisfied")

    def test_forced(self):
        self.state.extra = "forced"
        assert str(self.state) == "submitted (forced)"

    def test_str_compare(self):
        assert self.state == RunnerState("submitted")


class TestSatisfied(unittest.TestCase):
    def setUp(self):
        self.state = RunnerState("satisfied")

    def test_print(self):
        print(self.state)
        assert str(self.state) == "satisfied"
        assert repr(self.state) == 'RunnerState("satisfied")'

    def test_success(self):
        assert self.state.success is None
        self.state.success = True
        assert self.state.success
        assert str(self.state) == "satisfied (success)"

    def test_failed(self):
        assert self.state.success is None
        self.state.success = False
        assert not self.state.success
        assert str(self.state) == "satisfied (failed)"

    def test_ge(self):
        assert self.state >= RunnerState("created")
        assert self.state >= RunnerState("completed")
        assert self.state >= RunnerState("failed")
        assert self.state >= RunnerState("satisfied")

    def test_gt(self):
        assert self.state > RunnerState("created")
        assert self.state > RunnerState("completed")
        assert self.state > RunnerState("failed")
        assert not self.state > RunnerState("satisfied")

    def test_le(self):
        assert not self.state <= RunnerState("created")
        assert not self.state <= RunnerState("completed")
        assert not self.state <= RunnerState("failed")
        assert self.state <= RunnerState("satisfied")

    def test_lt(self):
        assert not self.state < RunnerState("created")
        assert not self.state < RunnerState("completed")
        assert not self.state < RunnerState("failed")
        assert not self.state < RunnerState("satisfied")

    def test_str_compare(self):
        assert self.state == RunnerState("satisfied")
