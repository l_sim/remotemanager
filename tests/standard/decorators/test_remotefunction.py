from remotemanager.decorators.remotefunction import RemoteFunction
from tests.utils.base_class import BaseTestClass


@RemoteFunction
def run_me(a, b):
    return a * b


def check(a, b):
    return run_me(a, b)


class TestRemoteFunction(BaseTestClass):

    def test_basic(self):
        self.create_dataset(check)

        self.ds.append_run({"a": 10, "b": 5})

        assert self.run_ds() == [50]

    def test_nonremote(self):
        assert run_me(10, 3) == 30
