from remotemanager.connection.computers.computer import Computer
from remotemanager.decorators.sanzufunction import SanzuFunction
from remotemanager.utils import random_string
from tests.utils.base_class import BaseTestClass


template = "# foo=#foo#"


class TestSanzuFunction(BaseTestClass):

    def test_computer(self):

        name = random_string()
        @SanzuFunction(
            name=name,
            local_dir=f"{name}_local",
            remote_dir=f"{name}_remote",
            url=Computer(template=template, foo=10),
        )
        def run_me(a, b):
            return a * b

        assert run_me(10, 4) == 40

        assert "foo=10" in run_me.dataset.runners[0].jobscript.content

        self.datasets.append(run_me.dataset)
