import time

import pytest

from remotemanager.connection.url import URL
from remotemanager.decorators.sanzufunction import SanzuFunction
from remotemanager.utils import random_string
from tests.utils.base_class import BaseTestClass


class TestSanzuFunction(BaseTestClass):

    def test_create_no_args(self):
        @SanzuFunction
        def test(foo):
            return foo

        test.dataset
        self.datasets.append(test.dataset)

    def test_basic(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def basic(a, b):
            return a * b

        assert basic(10, 4) == 40
        self.datasets.append(basic.dataset)

    def test_run_args(self):

        name = random_string()
        @SanzuFunction(
            name=name,
            local_dir=f"{name}_local",
            remote_dir=f"{name}_remote",
            url=URL(host="foo"),
        )
        def test_args(a, b):
            return a * b

        assert test_args.dataset.url.host == "foo"
        self.datasets.append(test_args.dataset)

    def test_type_hints(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def test_hints(a: int, b: int) -> int:
            return a * b

        assert test_hints(10, 4) == 40
        self.datasets.append(test_hints.dataset)

    def test_kwargs(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def test_kwargs(a, b):
            return a * b

        assert test_kwargs(a = 10, b = 4) == 40
        self.datasets.append(test_kwargs.dataset)

    def test_default(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def test_default(a, b = 10):
            return a * b

        assert test_default(a = 10) == 100
        self.datasets.append(test_default.dataset)

    def test_default_with_hints(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def run_me_default_hint(a: int, b: int = 10) -> int:
            return a * b

        assert run_me_default_hint(a = 10) == 100
        self.datasets.append(run_me_default_hint.dataset)

    def test_too_many_args(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def run_me_overload(a, b):
            return a * b

        with pytest.raises(ValueError):
            run_me_overload(10, 4, a=6)
        self.datasets.append(run_me_overload.dataset)


class TestRerun(BaseTestClass):

    def test_rerun(self):

        name = random_string()
        @SanzuFunction(name=name, local_dir=f"{name}_local", remote_dir=f"{name}_remote")
        def test_rerun(a, b):
            return a * b

        assert test_rerun(10, 4) == 40

        t_run = test_rerun.dataset.runners[0].last_submitted

        time.sleep(0.2)

        assert test_rerun(10, 4) == 40
        assert test_rerun.dataset.runners[0].last_submitted == t_run
        assert len(test_rerun.dataset.runners) == 1

        assert test_rerun(5, 4) == 20
        assert len(test_rerun.dataset.runners) == 2

        self.datasets.append(test_rerun.dataset)
