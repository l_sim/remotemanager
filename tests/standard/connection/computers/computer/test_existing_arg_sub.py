import pytest

from remotemanager import Computer


def test_python():
    template = "#python#"

    with pytest.raises(ValueError, match='Variable "python" already exists.*'):
        Computer(template=template)
