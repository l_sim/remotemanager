"""
Collection of URL Computer script based tests
"""

from remotemanager import Computer
from tests.utils.base_class import BaseTestClass


def function_sum(a: int, b: int) -> int:
    """testing function: test_scripting.py"""
    return a + b


template = """#SBATCH --nodes=#nodes#
#SBATCH --tasks-per-node=#mpi#
#SBATCH --cpus-per-task=#omp#
#SBATCH --queue=#queue#
#SBATCH --walltime=#time:format=time#
"""


class TestOffnodeRun(BaseTestClass):

    def setUp(self):
        url = Computer(host="localhost", template=template, submitter="sbatch")

        self.create_dataset(
            function_sum,
            url=url,
            skip=False,
        )

        self.ds.append_run({"a": 40, "b": 2})

    def test_control(self):
        """Test basic behaviour"""
        self.ds.run(omp=1, mpi=1, nodes=1, queue="standard", time=3600, dry_run=True)

        assert " bash" in self.ds.run_cmd  # master script still needs a bash sub
        assert "submit_job_sbatch" in self.ds.master_script.content
        assert "submit_job_bash" not in self.ds.master_script.content
        assert "#SBATCH" in self.ds.runners[0].jobscript.content

    def test_avoid_nodes(self):
        """Test avoid_nodes=True"""
        self.ds.run(
            omp=1,
            mpi=1,
            nodes=1,
            queue="standard",
            time=3600,
            avoid_nodes=True,
        )

        assert " bash" in self.ds.run_cmd  # master script still needs a bash sub
        assert "submit_job_sbatch" not in self.ds.master_script.content
        assert "submit_job_bash" in self.ds.master_script.content
        assert "#SBATCH" not in self.ds.runners[0].jobscript.content

        self.ds.wait(1, 10)
        self.ds.fetch_results()

        assert self.ds.results == [42]

    def test_avoid_nodes_with_shell_update(self):
        """Test avoid_nodes=True"""
        self.ds.shell = "sh"
        self.ds.run(
            omp=1,
            mpi=1,
            nodes=1,
            queue="standard",
            time=3600,
            avoid_nodes=True,
        )

        assert " sh" in self.ds.run_cmd  # update the shell
        assert "submit_job_sbatch" not in self.ds.master_script.content
        assert "submit_job_sh" in self.ds.master_script.content
        assert "#SBATCH" not in self.ds.runners[0].jobscript.content

        # note: can't actually run this since sh doesn't support source on ubuntu...
