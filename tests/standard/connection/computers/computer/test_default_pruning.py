from remotemanager import Computer


def test_nochange():
    test = Computer(template="")

    data = test.to_dict()
    assert "python" not in data
    assert "template" in data

    assert len(data) < 4


def test_changed():
    test = Computer(template="", python="foo")

    data = test.to_dict()
    assert data["python"] == "foo"
    assert "template" in data


def test_set_value():
    test = Computer(template="")

    test.python = "foo"

    data = test.to_dict()
    assert data["python"] == "foo"
    assert "template" in data
