import os.path

import pytest

from remotemanager import Dataset
from remotemanager.connection.computers.computer import Computer
from remotemanager.utils import random_string
from tests.utils.base_class import BaseTestClass


def test_load_warning():
    with pytest.raises(ValueError, match="Please provide a file path to file.*"):
        Computer.from_yaml()


@pytest.mark.parametrize("mode", ["pack", "yaml"])
class TestComputerStorage:
    def test_simple_template(self, mode: str):
        template = "#foo\na = #a#"
        test = Computer(template=template)

        if mode == "pack":
            data = test.pack()
            new = Computer.unpack(data)
        elif mode == "yaml":
            data = test.to_yaml()
            new = Computer.from_yaml(data=data)

        assert test.script() == "#foo"
        assert new.script() == "#foo"

    def test_simple_default(self, mode: str):
        template = "a = #a:default=10#"
        test = Computer(template=template)

        if mode == "pack":
            data = test.pack()
            new = Computer.unpack(data)
        elif mode == "yaml":
            data = test.to_yaml()
            new = Computer.from_yaml(data=data)

        assert test.script() == "a = 10"
        assert new.script() == "a = 10"

    def test_value_set(self, mode: str):
        template = "a = #a#"
        test = Computer(template=template)
        test.a = 10

        if mode == "pack":
            data = test.pack(collect_values=False)
            assert "value=10" not in data["template"]
            new = Computer.unpack(data)
        if mode == "yaml":
            data = test.to_yaml(collect_values=False)
            assert "value=10" not in data
            new = Computer.from_yaml(data=data)

        assert test.script() == "a = 10"
        assert new.script() != "a = 10"  # without value collection, should not exist

    def test_value_collect(self, mode: str):
        template = "a = #a#"
        test = Computer(template=template)
        test.a = 10

        if mode == "pack":
            data = test.pack(collect_values=True)
            assert "value=10" in data["template"]
            new = Computer.unpack(data)
        if mode == "yaml":
            data = test.to_yaml(collect_values=True)
            assert "value=10" in data
            new = Computer.from_yaml(data=data)

        assert test.script() == "a = 10"
        assert new.script() == "a = 10"


@pytest.mark.parametrize("mode", ["pack", "dict", "yaml"])
class TestModifiedUnpack:
    def test_override_host(self, mode: str):
        template = "a = #a#"
        test = Computer(template=template, host="remote.host")

        if mode == "pack":
            data = test.pack()
            new = Computer.unpack(data, host="new.host")
        elif mode == "dict":
            data = test.to_dict()
            new = Computer.from_dict(data, host="new.host")
        elif mode == "yaml":
            data = test.to_yaml()
            new = Computer.from_yaml(data=data, host="new.host")

        assert test.host == "remote.host"
        assert new.host == "new.host"


def foo(a):
    return a


class TestDataset(BaseTestClass):
    _dumppath = f"{random_string()}.yaml"

    def tearDown(self):
        if os.path.exists(self._dumppath):
            os.remove(self._dumppath)
        super().tearDown()

    def test_unpack_run(self):
        template = "# a = #a:default=10#"
        conn = Computer(template=template, ssh_insert="test")

        conn.a = 20

        self.create_dataset(foo, url=conn)
        self.ds.append_run({"a": 1})

        self.run_ds(dry_run=True)

        assert "a = 20" in self.ds.runners[0].jobscript.content

        self.ds.pack(file=self._dumppath)

        new = Dataset.from_file(file=self._dumppath, url=conn)

        assert not isinstance(new.url, dict)

        assert new.url.ssh_insert == "test"

        new.run(dry_run=True)
        assert "a = 20" in new.runners[0].jobscript.content

    def test_restore_run(self):
        template = "# a = #a:default=10#"
        conn = Computer(template=template, ssh_insert="test")

        conn.a = 20

        self.create_dataset(foo, url=conn)
        self.ds.append_run({"a": 1})

        self.run_ds(dry_run=True)

        assert "a = 20" in self.ds.runners[0].jobscript.content

        self.ds.pack(file=self._dumppath)

        new = self.recreate_previous_dataset()

        assert not isinstance(new.url, dict)

        assert new.url.ssh_insert == "test"

        new.run(dry_run=True)
        assert "a = 20" in new.runners[0].jobscript.content

    def test_restore_run_value_on_ds(self):
        template = "# a = #a:default=10#"
        conn = Computer(template=template, ssh_insert="test")

        self.create_dataset(foo, url=conn, a=20)
        self.ds.append_run({"a": 1})

        self.run_ds(dry_run=True)

        assert "a = 20" in self.ds.runners[0].jobscript.content

        self.ds.pack(file=self._dumppath)

        new = self.recreate_previous_dataset()

        assert not isinstance(new.url, dict)

        assert new.url.ssh_insert == "test"

        new.run(dry_run=True)
        assert "a = 20" in new.runners[0].jobscript.content
