from remotemanager import Computer
from tests.utils.base_class import BaseTestClass


def func(x, y):
    # test_run.py
    return x * y


template = """
#SBATCH --tasks-per-node=#mpi#
#SBATCH --cpus-per-task=#omp#
#SBATCH --nodes=#nodes:default={mpi*omp/cores_per_node}#

# using cores_per_node = #cores_per_node:default=64#
"""


class TestComputerRuns(BaseTestClass):
    def test_in_run(self):
        url = Computer(template=template)

        self.create_dataset(func, url=url)
        self.ds.append_run({"x": 10, "y": 7})

        self.run_ds(dry_run=True, mpi=64, omp=4)
        js = self.ds.runners[0].jobscript.content
        assert "#SBATCH --nodes=4" in js

    def test_in_ds(self):
        url = Computer(template=template)

        self.create_dataset(func, url=url, mpi=64, omp=4)
        self.ds.append_run({"x": 10, "y": 7})

        self.run_ds(dry_run=True)
        js = self.ds.runners[0].jobscript.content
        assert "#SBATCH --nodes=4" in js

    def test_run_args_update(self):
        url = Computer(template=template)

        self.create_dataset(func, url=url)
        self.ds.append_run({"x": 10, "y": 7})

        self.ds.set_run_arg("mpi", 64)
        self.ds.set_run_arg("omp", 4)

        self.run_ds(dry_run=True)
        js = self.ds.runners[0].jobscript.content
        assert "#SBATCH --nodes=4" in js

    def test_run_args_list_update(self):
        url = Computer(template=template)

        self.create_dataset(func, url=url)
        self.ds.append_run({"x": 10, "y": 7})

        self.ds.set_run_args(["mpi", "omp"], [64, 4])

        self.run_ds(dry_run=True)
        js = self.ds.runners[0].jobscript.content
        assert "#SBATCH --nodes=4" in js
