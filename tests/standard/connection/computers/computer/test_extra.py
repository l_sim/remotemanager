from remotemanager import Computer


def test_extra():
    template = """
#foo#
#extra#
"""
    url = Computer(template=template)

    url.foo = "foo"

    url.extra = "extra content"

    assert "extra content" in url.script()
