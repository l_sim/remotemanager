from remotemanager.script import Script


def test_non_eval():
    template = """
float = #a:default=100.0:format=float#
non_eval = #b:default=10+{a}#
"""
    script = Script(template)

    assert script.script() == """
float = 100.0
non_eval = 10+100.0
"""
