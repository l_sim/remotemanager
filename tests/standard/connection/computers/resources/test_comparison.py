from remotemanager import Computer


def test_min():
    template = """
min = #minval:default={min(a, b, c)}#

a = #a:default=1#
b = #b:default=2#
c = #c:default=1#
"""
    url = Computer(template=template)

    url._link_subs()

    assert url.minval.value == 1


def test_max():
    template = """
min = #maxval:default={max(a, b, c)}#

a = #a:default=1#
b = #b:default=2#
c = #c:default=1#
"""
    url = Computer(template=template)

    url._link_subs()

    assert url.maxval.value == 2


def test_cmp_basic():
    template = """
min = #minval:default={min(a, b, c)}#
max = #maxval:default={max(a, b, c)}#

a = #a:default=1#
b = #b:default=2#
c = #c:default=3#
"""
    url = Computer(template=template)

    assert "min = 1" in url.script()
    assert "max = 3" in url.script()


def test_cmp_increase():
    template = """
min = #minval:default={min(a, b, c)}#
max = #maxval:default={max(a, b, c)}#

a = #a:default=1#
b = #b:default=2#
c = #c:default=3#
"""
    url = Computer(template=template)

    url.a = 10

    assert "min = 2" in url.script()
    assert "max = 10" in url.script()
