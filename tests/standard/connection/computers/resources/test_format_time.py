"""
Tests to ensure that Computer can convert time formats properly
"""

from remotemanager.connection.computers.utils import format_time, time_to_s


def test_int():
    """check that an integer input is properly translated"""
    assert format_time(86400) == "24:00:00"
    assert format_time(3600) == "01:00:00"
    assert format_time(1) == "00:00:01"


def test_zero():
    """Check that a zero int produces a zero time"""
    assert format_time(0) == "00:00:00"


def test_none():
    """test conversion of None"""
    assert format_time(None) is None


def test_string():
    """Check that strings are still passed through"""
    assert format_time("24:00:00") == "24:00:00"


def test_semantic():
    """Check that semantic strings function as expected"""
    assert format_time("24h") == "24:00:00"
    assert format_time("30m") == "00:30:00"
    assert format_time("24h30m") == "24:30:00"
    assert format_time("30s") == "00:00:30"
    assert format_time("24h30s") == "24:00:30"
    assert format_time("24h30m30s") == "24:30:30"
    assert format_time("1800s") == "00:30:00"


def test_semantic_days():
    """Check that semantic time understands days"""
    assert format_time("1d") == "24:00:00"
    assert format_time("3d") == "72:00:00"
    assert format_time("1d30m") == "24:30:00"
    assert format_time("3d30s") == "72:00:30"


def test_missing_semantics():
    """Check combinations of missing semantic times"""
    assert format_time("1d1h1m1s") == "25:01:01"  # all
    assert format_time("1d1m1s") == "24:01:01"  # missing hours
    assert format_time("1d1h1s") == "25:00:01"  # missing mins
    assert format_time("1d1h1m") == "25:01:00"  # missing secs


def test_double_missing_semantics():
    """Check combinations of double missing semantics"""
    assert format_time("1d1h1m1s") == "25:01:01"  # all
    assert format_time("1m1s") == "00:01:01"  # d + h
    assert format_time("1h1s") == "01:00:01"  # d + m
    assert format_time("1h1m") == "01:01:00"  # d + s
    assert format_time("1d1s") == "24:00:01"  # h + m
    assert format_time("1d1m") == "24:01:00"  # h + s
    assert format_time("1d1h") == "25:00:00"  # m + s


def test_reverse():
    """Check that the reverse conversion works"""
    assert time_to_s("24:00:00") == 86400
    assert time_to_s("01:00:00") == 3600
    assert time_to_s("00:60:00") == 3600
