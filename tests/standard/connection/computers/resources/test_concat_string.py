from remotemanager.connection.computers.dynamicvalue import concat_basic
from remotemanager.connection.computers.resource import Resource


def test_normal():
    """Test usual a+b behaviour, where b is the string"""
    a = Resource(name="a", value="a")
    b = "_test"

    conc = concat_basic(a, b)

    assert conc.value == "a_test"

    a.value = "new"

    assert conc.value == "new_test"


def test_basic():
    """Test intended behaviour of a+b, where _a_ is the string"""
    a = "test_"
    b = Resource(name="a", value="a")

    conc = concat_basic(a, b)

    assert conc.value == "test_a"

    b.value = "new"

    assert conc.value == "test_new"


def test_numeric_normal():
    """Test that we can add numbers"""
    a = Resource(name="a", value=10)
    b = "_test"

    conc = concat_basic(a, b)

    assert conc.value == "10_test"

    a.value = 15.2

    assert conc.value == "15.2_test"


def test_numeric():
    """Test that we can add numbers"""
    a = "test_"
    b = Resource(name="a", value=10)

    conc = concat_basic(a, b)

    assert conc.value == "test_10"

    b.value = 15.2

    assert conc.value == "test_15.2"


def test_blank():
    """Tests that an empty string does not break the concat"""
    a = ""
    b = Resource(name="b", value=10)

    conc = concat_basic(a, b)

    assert conc.value == 10


def test_string_string():
    """Tests adding two strings"""
    a = "test_"
    b = Resource(name="name", default="name")

    conc = concat_basic(a, b)

    assert conc.value == "test_name"
