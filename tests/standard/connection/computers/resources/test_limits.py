import pytest

from remotemanager.connection.computers.resource import Resource


def test_basic():
    res = Resource(name="res")

    res.value = 10

    assert res.value == 10


def test_min():
    res = Resource(name="res", min=1)

    with pytest.raises(ValueError):
        res.value = 0


def test_max():
    res = Resource(name="res", max=1)

    with pytest.raises(ValueError):
        res.value = 10


def test_max_chain():
    a = Resource(name="a")
    b = Resource(name="b", max=10)

    a.value = 10

    with pytest.raises(ValueError):
        b.value = a * 2


def test_max_chain_late():
    a = Resource(name="a")
    b = Resource(name="b", max=10)

    b.value = a * 2
    a.value = 10

    with pytest.raises(ValueError):
        print(b.value)
