import os
import time
import unittest

import pytest

from remotemanager.connection.cmd import CMD
from remotemanager.utils import random_string


def test_captured_by_backoff():
    """
    This should hit two stages of backoff, succeeding on the final one

    With a timeout of 1, we will wait for 1s each time then before 1, 2 and 3s before
    retrying.
    """
    long = CMD('sleep 5 && echo "done!"', timeout=1)

    long.exec()

    assert long.stdout == "done!"


def test_too_long():
    """
    This exceeds the backoff limit of 3
    """
    long = CMD('sleep 10 && echo "done!"',
               timeout=1,
               max_timeouts=3)

    with pytest.raises(RuntimeError):
        long.exec()


def test_disable_max_timeouts():
    """
    This exceeds the backoff limit of 3
    """
    long = CMD('sleep 10 && echo "done!"',
               timeout=1,
               max_timeouts=-1)

    long.exec()

    assert long.stdout == "done!"


class TestCMDRedirects(unittest.TestCase):
    files = []

    def tearDown(self):
        for file in self.files:
            try:
                os.remove(file)
            except FileNotFoundError:
                pass

    def test_file_timeout(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")
        err = os.path.join(os.getcwd(), f"{fname}.err")
        long = CMD('sleep 5 && echo "done!"', timeout=1, stdout=out, stderr=err)

        self.files += [out, err]

        long.exec()
        with pytest.raises(RuntimeError):
            assert long.stdout == "done!"

        while not long.is_finished:
            time.sleep(0.1)
        assert long.stdout == "done!"
