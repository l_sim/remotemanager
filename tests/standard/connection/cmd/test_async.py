import time

from remotemanager.connection.url import URL


def test_async_exec():
    url = URL()

    t0 = time.perf_counter()
    ret = url.cmd("sleep 1 && echo 'done!'", asynchronous=True)

    dt = time.perf_counter() - t0

    assert dt < 1.0
    assert ret.asynchronous


def test_async_operation():
    url = URL()

    ret = url.cmd("sleep 2 && echo 'done!'", asynchronous=True)
    assert not ret.is_finished
    assert ret.returncode is None
    assert ret.succeeded is None

    time.sleep(2)
    assert "done!" in ret.stdout
    assert ret.is_finished
    assert ret.returncode == 0
