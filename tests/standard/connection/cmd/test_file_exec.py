import os.path
import time

from remotemanager.connection.cmd import CMD


def test_force_file():
    cmd = CMD("echo 'foo'", force_file=True, asynchronous=True)

    cmd.exec()
    # immediately after an async call, the file should be created
    assert os.path.isfile(cmd.tempfile)
    assert cmd.redirect["execfile"] == cmd.tempfile

    assert cmd.stdout.strip() == "foo"

    # multiple reads can exhibit issues
    assert cmd.stdout.strip() == "foo"

    # file should have been cleaned up by now
    assert not os.path.isfile(cmd.tempfile)


def test_many_commands():
    cmds = [CMD(f"echo '{i}'", force_file=True, asynchronous=True) for i in range(3)]

    for cmd in cmds:
        cmd.exec()

    time.sleep(0.1)

    for idx, cmd in enumerate(cmds):
        assert cmd.stdout == f"{idx}"


def test_many_commands_delay():
    cmds = [CMD(f"sleep 1 && echo '{i}'", force_file=True, asynchronous=True) for i in range(3)]

    for cmd in cmds:
        cmd.exec()

    while not all([c.is_finished for c in cmds]):
        time.sleep(0.1)

    for idx, cmd in enumerate(cmds):
        assert cmd.stdout == f"{idx}"
