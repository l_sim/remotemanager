"""If a command is too long, a Subprocess exec will fail. Test the fexec failover"""
import os
import shutil
import unittest

from remotemanager.connection.cmd import CMD
from remotemanager.connection.url import URL


class TestLongCommand(unittest.TestCase):
    testdir = os.path.join(os.getcwd(), "testing_dir")

    def tearDown(self):
        try:
            shutil.rmtree(self.testdir)
        except FileNotFoundError:
            pass

    def test_long_command(self):
        # create the command. The easiest way to do this is to create a lot of files
        nfiles = 10000
        url = URL()

        url.cmd(f'mkdir -p {self.testdir}')
        assert len(url.utils.ls(self.testdir)) == 0

        cmd = ['touch {']
        for i in range(nfiles):
            cmd.append(f'{self.testdir}/file{i},')

        cmd = ''.join(cmd)[:-1] + '}'

        create = CMD(cmd)

        create.exec()
        assert create.redirect["execfile"] == create.tempfile

        assert create.stdout == ""
        assert len(url.utils.ls(self.testdir)) == nfiles
