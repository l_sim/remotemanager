import pytest

from remotemanager.connection.cmd import CMD


def test_stream(capsys):
    cmd = """
    for ((i=0; i < 10; i++)); do
        echo $i
        sleep 0.25
    done
    """

    expected = "\n".join(str(char) for char in range(10))

    test = CMD(cmd, stream=True)
    test.exec()

    assert capsys.readouterr().out.strip() == expected

    assert test.stdout.strip() == expected


def test_stream_error(capsys):
    cmd = """
    for ((i=0; i < 10; i++)); do
        if [[ $i == '3' ]]; then
            echo >&2 "exit early"
            exit 1
        fi;
        echo $i
        sleep 0.25
    done
    """
    expected_out = "0\n1\n2"

    test = CMD(cmd, stream=True)

    with pytest.raises(RuntimeError, match=".*exit early.*"):
        test.exec()

    assert capsys.readouterr().out.strip() == expected_out

    assert test.stdout.strip() == expected_out
    assert "exit early" in test.stderr.strip()
