import os.path

from remotemanager.connection.cmd import CMD


def test_basic():
    cmd = CMD("echo 'foo'")

    cmd.exec()

    assert cmd.stdout == "foo"


def test_file_exec():
    cmd = CMD("echo 'foo'", force_file=True)

    cmd.exec()

    assert cmd.stdout == "foo"


def test_file_redirect():
    cmd = CMD("echo 'foo'", stdout="test.out")

    cmd.exec()

    assert cmd.stdout == "foo"
    assert os.path.exists("test.out")

    try:
        os.remove("test.out")
    except FileNotFoundError:
        pass
