from remotemanager.connection.cmd import CMD


def test_cached_output():
    cmd = CMD("echo 'foo'", asynchronous=True)
    cmd.exec()

    assert not cmd.cached
    assert cmd.stdout == "foo"

    assert cmd.cached
    assert cmd.stdout == "foo"
