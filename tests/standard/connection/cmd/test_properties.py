from remotemanager.connection.cmd import CMD


def test_uuid_diff():
    a = CMD("echo 'a'")
    b = CMD("echo 'b'")

    assert a.uuid != b.uuid
    assert a.short_uuid != b.short_uuid


def test_initial_cmd():
    content = "sleep 5 && echo 'foo'"
    cmd = CMD(content)

    assert cmd.cmd == content
    assert cmd.sent == content


def test_get_user():
    test = CMD("echo 'foo'")
    test.exec()

    user = CMD("whoami")
    user.exec()
    user = user.stdout

    assert test.whoami == user


def test_on_non_exec():
    test = CMD("echo 'foo'")

    assert test.stdout is None


def test_kill_non_exec():
    test = CMD("echo 'foo'")

    test.kill()  # should not raise an exception
