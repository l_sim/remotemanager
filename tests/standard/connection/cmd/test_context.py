from remotemanager.connection.cmd import CMD
from tests.utils import pid_exists


def test_context_kill():
    with CMD("sleep 300 && echo 'foo'", asynchronous=True, timeout=False) as c:
        c.exec()

        pid = c.pid

        c.__exit__(None, None, None)

    assert not pid_exists(pid)
    assert not c.stdout == 'foo'
