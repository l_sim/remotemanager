import pytest

from remotemanager.connection.cmd import CMD


class TestTimeoutDisable:

    def setup(self, **kwargs):
        return CMD(cmd="sleep 2 && echo 'foo'", **kwargs)

    def test_timeout_disable_control(self):
        cmd = self.setup(timeout=1, max_timeouts=1)
        with pytest.raises(RuntimeError):
            cmd.exec()
            assert cmd.stdout != "foo"

    @pytest.mark.parametrize("timeout", [0, -1, False])
    def test_timeout_disable(self, timeout):
        cmd = self.setup(timeout=timeout, max_timeouts=1)
        cmd.exec()
        assert cmd.stdout == "foo"
