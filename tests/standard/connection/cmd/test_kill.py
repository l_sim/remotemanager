import random
import time

import pytest

from remotemanager.connection.cmd import CMD
from tests.utils import pid_exists


def get_nonexistent_pid() -> int:
    # get a pid that does not exist
    pid = random.randint(1024, 99999)
    while pid_exists(pid):
        pid = random.randint(1024, 99999)

    return pid


def test_self_kill(capsys):
    cmd = CMD("sleep 30 && echo 'foo'", asynchronous=True)

    t0 = time.perf_counter()
    cmd.exec()
    cmd.kill()
    pid = cmd.pid

    assert not pid_exists(pid)
    assert (time.perf_counter() - t0) < 3
    assert "Done." in capsys.readouterr().out


def test_self_kill_by_pid(capsys):
    cmd = CMD("sleep 300 && echo 'foo'", asynchronous=True)
    cmd.exec()

    cmd.kill(cmd.pid)

    assert not pid_exists(cmd.pid)
    assert cmd.stdout != 'foo'
    assert "Done." in capsys.readouterr().out


def test_kill_other(capsys):
    """Check that we can kill another PID owned by the same user"""
    cmd_a = CMD("sleep 300 && echo 'foo'", asynchronous=True)
    cmd_a.exec()

    cmd_b = CMD("echo 'foo'")
    cmd_b.kill(cmd_a.pid)

    assert not pid_exists(cmd_a.pid)
    assert cmd_a.stdout != 'foo'
    assert "Done." in capsys.readouterr().out


def test_kill_nonexistent(capsys):
    pid = get_nonexistent_pid()

    cmd = CMD("echo 'foo'")
    cmd.kill(pid)

    assert "Process not found." in capsys.readouterr().out


def test_kill_non_int(capsys):
    cmd = CMD("echo 'foo'")
    with pytest.raises(TypeError):
        cmd.kill("foo")

    assert "Error." in capsys.readouterr().out


@pytest.mark.parametrize("generate_pid", [True, False])
@pytest.mark.parametrize("verbose_in_init", [True, False])
@pytest.mark.parametrize("verbose", [0, False, -1])
def test_kill_nonverbose(capsys, generate_pid, verbose, verbose_in_init):
    if generate_pid:
        tmp = CMD("sleep 300 && echo 'foo'", asynchronous=True)
        tmp.exec()
        pid = tmp.pid
    else:
        pid = get_nonexistent_pid()

    if verbose_in_init:
        cmd = CMD("ls", verbose=verbose)
        cmd.kill(pid)
    else:
        cmd = CMD("ls")
        cmd.kill(pid, verbose=verbose)

    assert capsys.readouterr().out == ""
