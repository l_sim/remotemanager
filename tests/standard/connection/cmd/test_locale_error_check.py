from remotemanager.connection.cmd import detect_locale_error

pos_strings = [
    """received the following stderr: 
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LANG = "C.UTF-8"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").""",
    "/etc/profile.d/lang.sh: line 19: warning: setlocale: LC_CTYPE: cannot change locale (UTF-8): No such file or directory",
    "SetLocale",
    "setLocale",
    "Setlocale",
    "setting locale",
    "settingLocale",
]

neg_strings = ["'locale' could not be found",
               "file 'locale' exists",
               "locale: command not found",
               "set"]


def test_pos():
    for string in pos_strings:
        assert detect_locale_error(string)


def test_neg():
    for string in neg_strings:
        assert not detect_locale_error(string)
