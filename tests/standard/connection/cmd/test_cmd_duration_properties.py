from remotemanager.connection.cmd import CMD


def test_duration_noasync():
    cmd = CMD("sleep 1 && echo 'done!'", asynchronous=False)
    cmd.exec()

    assert "done!" in cmd.stdout

    assert cmd.duration["communicate"] >= 1
    assert cmd.duration["exec"] < 1
    assert cmd.latency < 1


def test_duration_async():
    cmd = CMD("sleep 1 && echo 'done!'", asynchronous=True)
    cmd.exec()

    assert "done!" in cmd.stdout

    assert cmd.duration["communicate"] >= 1
    assert cmd.duration["exec"] < 1
    assert cmd.latency < 1
