from remotemanager.connection.cmd import CMD
from remotemanager.connection.url import URL


def test_verbose_url_bool(capsys):
    url = URL(verbose=False, timeout=1)
    cmd = url.cmd("sleep 1 && echo 'foo'")

    assert capsys.readouterr().out == ""

    assert cmd.stdout == "foo"


def test_verbose_cmd_bool(capsys):
    cmd = CMD("sleep 1 && echo 'foo'", verbose=False, timeout=1)
    cmd.exec()

    assert capsys.readouterr().out == ""

    assert cmd.stdout == "foo"


def test_verbose_url_bool_exec(capsys):
    url = URL(timeout=1)
    cmd = url.cmd("sleep 1 && echo 'foo'", verbose=False)

    assert capsys.readouterr().out == ""

    assert cmd.stdout == "foo"


def test_verbose_cmd_bool_exec(capsys):
    cmd = CMD("sleep 1 && echo 'foo'", timeout=1)
    cmd.exec(verbose=False)

    assert capsys.readouterr().out == ""

    assert cmd.stdout == "foo"


def test_verbose_cmd_bool_fexec(capsys):
    cmd = CMD("sleep 1 && echo 'foo'", timeout=1, force_file=True, verbose=False)
    cmd.exec()

    assert capsys.readouterr().out == ""

    assert cmd.stdout == "foo"


def test_verbose_cmd_bool_fexec_exec(capsys):
    cmd = CMD("sleep 1 && echo 'foo'", timeout=1, force_file=True)
    cmd.exec(verbose=False)

    assert capsys.readouterr().out == ""

    assert cmd.stdout == "foo"


def test_verbose_communicate(capsys):
    # control, make sure we're actually timing out with this cmd
    cmd = CMD("sleep 1 && echo 'foo'", verbose=True, timeout=1, asynchronous=True)
    cmd.exec()
    cmd.communicate()
    assert "communication attempt failed after 1s" in capsys.readouterr().out

    # ensure the readout is wiped on read
    assert capsys.readouterr().out == ""

    cmd = CMD("sleep 1 && echo 'foo'", verbose=False, timeout=1, asynchronous=True)
    cmd.exec()
    cmd.communicate()

    # assuming the readout is not wiped, we should have no errors present
    assert capsys.readouterr().out == ""


def test_verbose_communicate_exec(capsys):
    cmd = CMD("sleep 1 && echo 'foo'", timeout=1, asynchronous=True)
    cmd.exec()
    cmd.communicate(verbose=False)
    assert capsys.readouterr().out == ""
