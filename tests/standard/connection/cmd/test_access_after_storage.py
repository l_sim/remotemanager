import os

from remotemanager.connection.cmd import CMD


def test_run_store():
    test = CMD("echo 'foo'")

    test.exec()
    try:
        test.pack(file="test_cmd_store.yaml")
        new = CMD.unpack(file="test_cmd_store.yaml")
    finally:
        os.remove("test_cmd_store.yaml")

    assert new.stdout == 'foo'
