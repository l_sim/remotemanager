import pytest

from remotemanager.connection.cmd import CMD


def test_silent_error():
    """
    ssh to localhost causes connection problems.
    Doing this silently simulates a silent error
    """
    cmd = CMD("ssh -q localhost 'foo'")
    cmd.exec()

    assert cmd.stdout == ""
    assert cmd.stderr == ""
    assert cmd.returncode == 255


def test_standard_error():
    cmd = CMD("foo")
    with pytest.raises(RuntimeError, match=".*command not found.*"):
        cmd.exec()

    assert cmd.returncode == 127
