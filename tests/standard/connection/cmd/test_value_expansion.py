from remotemanager.connection.url import URL


def test_non_expand():
    url = URL(user="user", host="remote.host")
    test = url.cmd("ls $HOME", dry_run=True)

    assert test.sent == "ssh -p 22 -q user@remote.host 'ls $HOME'"
