import os
import time
import unittest

import pytest

from remotemanager.connection.cmd import CMD
from remotemanager.utils import random_string


class TestCMDRedirects(unittest.TestCase):
    files = []

    def tearDown(self):
        for file in self.files:
            try:
                os.remove(file)
            except FileNotFoundError:
                pass

    def test_redirect(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")
        err = os.path.join(os.getcwd(), f"{fname}.err")

        print(f"outfile: {out}")
        print(f"errfile: {err}")

        self.files += [out, err]

        cmd = CMD("echo 'foo!'", stdout=out, stderr=err)
        assert not os.path.exists(out)
        assert not os.path.exists(err)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)
        assert os.path.exists(err)

        with open(out, mode="r", encoding="utf8") as o:
            out_content = o.read().strip()
        with open(err, mode="r", encoding="utf8") as e:
            err_content = e.read().strip()

        assert not cmd.cached
        assert out_content == cmd.stdout == "foo!"
        assert err_content == cmd.stderr == ""

        assert cmd.cached

    def test_redirect_out_only(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")

        print(f"outfile: {out}")

        self.files += [out]

        cmd = CMD("echo 'foo!'", stdout=out)
        assert not os.path.exists(out)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)

        with open(out, mode="r", encoding="utf8") as o:
            out_content = o.read().strip()

        assert not cmd.cached
        assert out_content == cmd.stdout == "foo!"
        assert cmd.stderr is None  # strange, perhaps this should be ""?

        assert cmd.cached

    def test_redirect_err_only(self):
        fname = random_string()
        err = os.path.join(os.getcwd(), f"{fname}.err")

        print(f"outfile: {err}")

        self.files += [err]

        cmd = CMD("aaaa", stderr=err, raise_errors=False)
        assert not os.path.exists(err)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(err)

        with open(err, mode="r", encoding="utf8") as e:
            err_content = e.read().strip()

        assert not cmd.cached
        assert cmd.stdout is None  # strange, perhaps this should be ""?
        assert err_content == cmd.stderr != ""

        assert cmd.cached

    def test_redirect_err(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")
        err = os.path.join(os.getcwd(), f"{fname}.err")

        print(f"outfile: {out}")
        print(f"errfile: {err}")

        self.files += [out, err]

        cmd = CMD("aaaaa", stdout=out, stderr=err)
        assert not os.path.exists(out)
        assert not os.path.exists(err)

        cmd.exec()
        with pytest.raises(RuntimeError):
            cmd.communicate()

        cmd = CMD("aaaaa", stdout=out, stderr=err, raise_errors=False)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)
        assert os.path.exists(err)

        with open(out, mode="r", encoding="utf8") as o:
            out_content = o.read().strip()
        with open(err, mode="r", encoding="utf8") as e:
            err_content = e.read().strip()

        assert not cmd.cached
        assert out_content == cmd.stdout == ""
        assert err_content == cmd.stderr != ""

        assert cmd.cached

    def test_redirect_missing_out(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")

        print(f"outfile: {out}")

        self.files += [out]

        cmd = CMD("echo 'foo!'", stdout=out)
        assert not os.path.exists(out)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)

        os.remove(out)

        with pytest.raises(FileNotFoundError):
            assert cmd.stdout == "foo!"

    def test_redirect_overwrite(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")

        print(f"outfile: {out}")

        self.files += [out]

        cmd = CMD("echo 'foo!'", stdout=out)
        assert not os.path.exists(out)

        with open(out, mode="w+", encoding="utf8") as o:
            o.write("overwrite me!")

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)
        assert cmd.stdout == "foo!"

    def test_redirect_overwrite_err(self):
        fname = random_string()
        err = os.path.join(os.getcwd(), f"{fname}.err")

        print(f"outfile: {err}")

        self.files += [err]

        cmd = CMD("aaaa", stderr=err, raise_errors=False)
        assert not os.path.exists(err)

        with open(err, mode="w+", encoding="utf8") as e:
            e.write("overwrite me!")

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(err)
        assert "command not found" in cmd.stderr

    def test_same_target(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")

        print(f"outfile: {out}")

        self.files += [out]

        cmd = CMD("echo 'foo!'", stdout=out, stderr=out)
        assert not os.path.exists(out)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)

        with open(out, mode="r", encoding="utf8") as o:
            out_content = o.read().strip()

        assert not cmd.cached
        assert out_content == cmd.stdout == "foo!"
        assert out_content == cmd.stderr == "foo!"

        assert cmd.cached

    def test_same_target_error(self):
        fname = random_string()
        out = os.path.join(os.getcwd(), f"{fname}.out")

        print(f"outfile: {out}")

        self.files += [out]

        cmd = CMD("aaa", stdout=out, stderr=out, raise_errors=False)
        assert not os.path.exists(out)

        cmd.exec()
        time.sleep(0.1)
        assert os.path.exists(out)

        with open(out, mode="r", encoding="utf8") as o:
            out_content = o.read().strip()

        assert not cmd.cached
        assert out_content == cmd.stdout
        assert out_content == cmd.stderr

        assert "command not found" in cmd.stdout
        assert "command not found" in cmd.stderr

        assert cmd.cached
