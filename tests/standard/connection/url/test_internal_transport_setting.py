import pytest

from remotemanager.connection.url import URL
from remotemanager.transport import rsync
from remotemanager.transport.transport import Transport


def test_default():
    url = URL()

    assert isinstance(url.transport, Transport)


def test_set():
    url = URL()

    url.transport = rsync()

    assert isinstance(url.transport, rsync)


def test_set_init():
    url = URL(transport=rsync())

    assert isinstance(url.transport, rsync)


def test_set_invalid():
    url = URL()

    with pytest.raises(ValueError):
        url.transport = "foo"


def test_set_invalid_init():
    with pytest.raises(ValueError):
        URL(transport="foo")
