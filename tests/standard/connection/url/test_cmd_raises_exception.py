import re

import pytest

from remotemanager import URL
from tests.utils.base_class import BaseTestClass


class TestErrorCases(BaseTestClass):

    def test_raise_errors(self):
        url = URL(raise_errors=False)

        assert not url.raise_errors
        assert url.ignore_errors

        cmd = url.cmd("echo foo 1>&2")
        assert cmd.stderr == "foo"

        url = URL(raise_errors=True)

        assert url.raise_errors
        assert not url.ignore_errors

        match = re.compile(".*received the following stderr:.*", re.MULTILINE)
        with pytest.raises(RuntimeError, match=match):
            url.cmd("echo foo 1>&2")

    def test_raise_errors_set(self):
        url = URL()
        url.raise_errors = False

        assert not url.raise_errors
        assert url.ignore_errors

        cmd = url.cmd("echo foo 1>&2")
        assert cmd.stderr == "foo"

        url = URL()
        url.raise_errors = True

        assert url.raise_errors
        assert not url.ignore_errors

        match = re.compile(".*received the following stderr:.*", re.MULTILINE)
        with pytest.raises(RuntimeError, match=match):
            url.cmd("echo foo 1>&2")

    def test_ignore_errors(self):
        url = URL(ignore_errors=True)

        assert not url.raise_errors
        assert url.ignore_errors

        cmd = url.cmd("echo foo 1>&2")
        assert cmd.stderr == "foo"

        url = URL(ignore_errors=False)

        assert url.raise_errors
        assert not url.ignore_errors

        match = re.compile(".*received the following stderr:.*", re.MULTILINE)
        with pytest.raises(RuntimeError, match=match):
            url.cmd("echo foo 1>&2")

    def test_ignore_errors_set(self):
        url = URL()
        url.ignore_errors = True

        assert not url.raise_errors
        assert url.ignore_errors

        cmd = url.cmd("echo foo 1>&2")
        assert cmd.stderr == "foo"

        url = URL()
        url.ignore_errors = False

        assert url.raise_errors
        assert not url.ignore_errors

        match = re.compile(".*received the following stderr:.*", re.MULTILINE)
        with pytest.raises(RuntimeError, match=match):
            url.cmd("echo foo 1>&2")
