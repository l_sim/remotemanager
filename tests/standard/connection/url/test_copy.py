import copy

from remotemanager.connection.url import URL


def test_deepcopy():
    url = URL()

    new = copy.deepcopy(url)

    assert url is not new


def test_deepcopy_vals():
    userhost = "foo@remote.host"
    url = URL(userhost)

    new = copy.deepcopy(url)

    assert url is not new

    assert url.userhost == userhost
    assert new.userhost == userhost


def test_deepcopy_cmd():
    url = URL()
    url.cmd("pwd")

    new = copy.deepcopy(url)

    assert url is not new
