from remotemanager.connection.url import URL


def test_history():
    url = URL(cmd_history_depth=10)
    assert url.cmd_history_depth == 10
    for i in range(10):
        url.cmd(f"echo '{i}'")

    for idx, cmd in enumerate(url.cmd_history):
        assert cmd.stdout == str(idx)


def test_shorter_history():
    url = URL(cmd_history_depth=5)
    assert url.cmd_history_depth == 5
    for i in range(10):
        url.cmd(f"echo '{i}'")

    assert url.cmd_history[0].stdout == '5'
    assert url.cmd_history[-1].stdout == '9'


def test_shorter_history_set():
    url = URL()
    url.cmd_history_depth = 5
    assert url.cmd_history_depth == 5
    for i in range(10):
        url.cmd(f"echo '{i}'")

    assert url.cmd_history[0].stdout == '5'
    assert url.cmd_history[-1].stdout == '9'


def test_shorter_history_set_after_run():
    url = URL(cmd_history_depth=10)
    assert url.cmd_history_depth == 10
    for i in range(10):
        url.cmd(f"echo '{i}'")

    url.cmd_history_depth = 5

    assert url.cmd_history[0].stdout == '5'
    assert url.cmd_history[-1].stdout == '9'


def test_history_reset():
    url = URL(cmd_history_depth=10)
    assert url.cmd_history_depth == 10
    for i in range(10):
        url.cmd(f"echo '{i}'")

    assert len(url.cmd_history) != 0

    url.reset_cmd_history()
    assert len(url.cmd_history) == 0
    assert url.cmd_history.maxlen == 10


def test_history_reset_short():
    url = URL(cmd_history_depth=5)
    assert url.cmd_history_depth == 5
    for i in range(10):
        url.cmd(f"echo '{i}'")

    assert len(url.cmd_history) != 0

    url.reset_cmd_history()
    assert len(url.cmd_history) == 0
    assert url.cmd_history.maxlen == 5
