import os

import pytest

from remotemanager.connection.url import URL
from remotemanager.utils import random_string


def test_force_local():
    url = URL(user='test', host='remote.addr')

    assert url.cmd("test", local=True, dry_run=True).cmd == "test"
    assert url.cmd("test", local=False, dry_run=True).cmd == "ssh -p 22 -q test@remote.addr 'test'"


def test_passfile():
    passfile = random_string()
    url = URL(user='test', host='remote.addr', passfile=passfile)

    with open(passfile, mode="w+", encoding="utf8") as o:
        o.write("")
    try:
        assert url.cmd("test", dry_run=True).cmd == f"sshpass -f {passfile} ssh -p 22 -q test@remote.addr 'test'"
    finally:
        os.remove(passfile)


def test_keyfile():
    keyfile = random_string()
    url = URL(user='test', host='remote.addr', keyfile=keyfile)

    with open(keyfile, mode="w+", encoding="utf8") as o:
        o.write("")
    try:
        assert url.cmd("test", dry_run=True).cmd == f"ssh -p 22 -q -i {keyfile} test@remote.addr 'test'"
    finally:
        os.remove(keyfile)


def test_both_files():
    passfile = random_string()
    keyfile = random_string()
    url = URL(user='test', host='remote.addr', keyfile=keyfile, passfile=passfile)

    with open(keyfile, mode="w+", encoding="utf8") as o:
        o.write("")
    with open(passfile, mode="w+", encoding="utf8") as o:
        o.write("")
    try:
        assert url.cmd("test", dry_run=True).cmd == f"sshpass -f {passfile} ssh -p 22 -q -i {keyfile} test@remote.addr 'test'"
    finally:
        os.remove(keyfile)
        os.remove(passfile)


def test_both_files_set_after():
    passfile = random_string()
    keyfile = random_string()
    url = URL(user='test', host='remote.addr')
    url.keyfile = keyfile
    url.passfile = passfile

    with open(keyfile, mode="w+", encoding="utf8") as o:
        o.write("")
    with open(passfile, mode="w+", encoding="utf8") as o:
        o.write("")
    try:
        assert url.cmd("test",
                       dry_run=True).cmd == f"sshpass -f {passfile} ssh -p 22 -q -i {keyfile} test@remote.addr 'test'"
    finally:
        os.remove(keyfile)
        os.remove(passfile)


def test_both_files_delete():
    passfile = random_string()
    keyfile = random_string()
    url = URL(user='test', host='remote.addr', keyfile=keyfile, passfile=passfile)

    with open(keyfile, mode="w+", encoding="utf8") as o:
        o.write("")
    with open(passfile, mode="w+", encoding="utf8") as o:
        o.write("")

    assert url.passfile is not None
    assert url.keyfile is not None

    os.remove(keyfile)
    os.remove(passfile)

    with pytest.raises(RuntimeError, match=f"could not find ssh key file at {keyfile}"):
        assert url.keyfile
    with pytest.raises(RuntimeError, match=f"could not find password file at {passfile}"):
        assert url.passfile


def test_invalid_passfile():
    url = URL(user='test', host='remote.addr', passfile="foo")

    with pytest.raises(RuntimeError, match="could not find password file at foo"):
        url.cmd("test")


def test_invalid_keyfile():
    url = URL(user='test', host='remote.addr', keyfile="foo")

    with pytest.raises(RuntimeError, match="could not find ssh key file at foo"):
        url.cmd("test")


def test_quiet_ssh():
    url = URL("foo@bar")

    assert "-q" in url.cmd("ls", dry_run=True).cmd


def test_nonquiet_ssh():
    url = URL("foo@bar", quiet_ssh=False)

    assert "-q" not in url.cmd("ls", dry_run=True).cmd


def test_ssh_insert():
    url = URL(user="test", host="remote.machine", ssh_insert="-o StrictHostKeyChecking=no")

    assert url.cmd("ls", dry_run=True).cmd == "ssh -o StrictHostKeyChecking=no -p 22 -q test@remote.machine 'ls'"


def test_ssh_insert_add():
    url = URL(user="test", host="remote.machine")
    url.ssh_insert="-o StrictHostKeyChecking=no"

    assert url.cmd("ls", dry_run=True).cmd == "ssh -o StrictHostKeyChecking=no -p 22 -q test@remote.machine 'ls'"
