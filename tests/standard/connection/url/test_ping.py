import sys

import pytest

from remotemanager.connection.url import URL

versionskip = sys.version_info.major <= 3 and sys.version_info.minor <= 7


@pytest.mark.parametrize("line,value", [
    ("64 bytes from host: icmp_seq=2 ttl=51 time=25.8 ms", 0.0258),
    ("64 bytes from host: icmp_seq=1 ttl=63 time=0.361 ms", 0.000361)
    ]
)
def test_ping_line_parse(line, value):
    url = URL()
    assert url._process_ping_line(line) == value


def test_ping_line_wrong_units():
    url = URL()
    with pytest.raises(ValueError):
        url._process_ping_line("64 bytes from host: icmp_seq=1 ttl=63 time=1.00 s")


def test_ping_no_route():
    url = URL(host="foo")

    assert url.ping(timeout=1) == -1


@pytest.mark.skipif(versionskip,
                    reason="Python 3.7 seems to have issues with connecting to gitlab")
def test_ping_gitlab():
    url = URL(host="gitlab.com")

    ping = url.ping()

    assert ping < 1
    assert ping > 0


def test_ping_nonverbose(capsys):
    url = URL()
    url.ping(verbose=False, n=5)

    assert capsys.readouterr().out == ""


def test_ping_nonverbose_init(capsys):
    url = URL(verbose=False)
    url.ping(n=5)

    assert capsys.readouterr().out == ""


def test_ping_verbose(capsys):
    url = URL()
    url.ping(verbose=2, n=5)

    assert capsys.readouterr().out != ""


def test_ping_verbose_init(capsys):
    url = URL(verbose=2)
    url.ping(n=5)

    assert capsys.readouterr().out != ""
