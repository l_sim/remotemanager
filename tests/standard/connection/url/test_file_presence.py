import os
import shutil
import unittest

from remotemanager.connection.url import URL
from remotemanager.utils import random_string


class TestFilePresence(unittest.TestCase):

    def setUp(self):
        """Create 5 files for testing with"""
        self.testing_dir = f"file_mtime_test_{random_string()}"
        self.testing_dir_2 = f"file_mtime_test_{random_string()}_2"

        os.makedirs(self.testing_dir)
        os.makedirs(self.testing_dir_2)

        self.files_created = []
        for i in range(5):
            fname = f'{self.testing_dir}/file-{i}'

            with open(fname, mode="w+", encoding="utf8") as o:
                o.write("")
                self.files_created.append(fname)
        # set up a second directory for multi-dir searches
        self.files_created_2 = []
        for i in range(2):
            # prepend a "1" to prevent name clashes
            fname = f'{self.testing_dir_2}/file-1{i}'

            with open(fname, mode="w+", encoding="utf8") as o:
                o.write("")
                self.files_created_2.append(fname)

    def tearDown(self):
        shutil.rmtree(self.testing_dir)
        shutil.rmtree(self.testing_dir_2)

    def test_search(self):
        url = URL()

        search = url.utils.search_folder(self.files_created, self.testing_dir)

        for file, presence in search.items():
            assert presence

    def test_search_not_present(self):
        url = URL()

        search = url.utils.search_folder(self.files_created, self.testing_dir_2)

        for file, presence in search.items():
            assert not presence

    def test_presence(self):
        url = URL()

        search = url.utils.file_presence(self.files_created)

        for file, presence in search.items():
            assert presence

    def test_presence_multi_dir(self):
        url = URL()

        search = url.utils.file_presence(self.files_created + self.files_created_2)

        for file, presence in search.items():
            assert presence

    def test_not_present(self):
        url = URL()

        file = os.path.join(self.testing_dir, "file-42")
        search = url.utils.file_presence([file])

        assert not search[file]

    def test_empty_search(self):
        url = URL()

        assert url.utils.file_presence([]) == {}

    def test_empty_presence(self):
        url = URL()

        assert url.utils.search_folder([], self.testing_dir) == {}
