from remotemanager.connection.url import URL
from tests.utils.base_class import BaseTestClass


def f(x):
    # test_shebang_update
    return x


class TestShebangUpdate(BaseTestClass):

    def test_in_ds_init(self):
        self.create_dataset(f, shebang="new")

        assert self.ds.shebang == "new"
        assert self.ds.url.shebang == "new"

    def test_set_ds(self):
        self.create_dataset(f)

        self.ds.shebang = "new"

        assert self.ds.shebang == "new"
        assert self.ds.url.shebang == "new"

    def test_in_url_init(self):
        url = URL(shebang="new")
        self.create_dataset(f, url=url)

        assert self.ds.shebang == "new"
        assert self.ds.url.shebang == "new"

    def test_set_url(self):
        url = URL()
        self.create_dataset(f, url=url)

        self.ds.url.shebang = "new"

        assert self.ds.shebang == "new"
        assert self.ds.url.shebang == "new"
