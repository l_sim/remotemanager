from remotemanager import URL


def test_defaults():
    url = URL()

    assert url.submitter == "bash"
    assert url.shell == "bash"


def test_update():
    """Check that we can update the properties"""
    url = URL(shell="foo", submitter="sub")

    assert url.submitter == "sub"
    assert url.shell == "foo"


def test_update_set():
    """Check that we can update the properties"""
    url = URL()

    url.shell = "foo"
    url.submitter = "sub"

    assert url.submitter == "sub"
    assert url.shell == "foo"


def test_shell_updates_submitter():
    """Updating shell should also update a default submitter"""
    url = URL()
    url.shell = "foo"

    assert url.submitter == "foo"
    assert url.shell == "foo"


def test_shell_updates_submitter_at_init():
    """Updating shell should also update a default submitter"""
    url = URL(shell="foo")

    assert url.submitter == "foo"
    assert url.shell == "foo"


def test_none_submitter():
    """If submitter is somehow None, shell should replace it"""
    url = URL(shell="shell", submitter=None)

    assert url.submitter == "shell"
    assert url.shell == "shell"
