"""
Can't test the functionality of the tunnel without a remote test suite, but we
can at least check the surrounding attributes
"""
import pytest

from remotemanager import URL


def test_command():
    url = URL("remote.host")

    tunnel = url.tunnel(local_port=9999, remote_port=8888, dry_run=True)

    assert "-L" in tunnel.cmd
    assert "-N" in tunnel.cmd
    assert "9999:remote.host:8888" in tunnel.cmd


def test_change_local_address():
    url = URL("remote.host")

    tunnel = url.tunnel(local_port=9999, remote_port=8888, local_address="127.1.1.1", dry_run=True)

    assert "-L" in tunnel.cmd
    assert "-N" in tunnel.cmd
    assert "127.1.1.1:9999:remote.host:8888" in tunnel.cmd


@pytest.mark.parametrize("port", [1, 100, 1024, 8000.0, True, False, None])
@pytest.mark.parametrize("local", [True, False])
def test_invalid_port(port: int, local: bool):
    url = URL("remote.host")

    with pytest.raises(ValueError):
        if local:
            url.tunnel(local_port=port, remote_port=8888, dry_run=True)
        else:
            url.tunnel(local_port=9999, remote_port=port, dry_run=True)
