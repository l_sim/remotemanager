import os.path

import pytest

from remotemanager.connection.url import URL

home = os.path.expandvars("$HOME")
user = os.path.expandvars("$USER")


def test_home():
    url = URL()

    assert url.expandvars("$HOME") == home


def test_home_shortcut():
    url = URL()

    assert url.expandvars("~") == home


def test_home_property():
    url = URL()

    assert url.home == home


def test_gethome():
    url = URL()

    assert url.gethome() == home


def test_update_home():
    url = URL()
    url._home = "foo"

    assert url.home == "foo"

    url.clearhome()
    assert url.home == home


def test_update_home_gethome():
    url = URL()
    url._home = "foo"

    assert url.home == "foo"

    url.clearhome()
    assert url.gethome() == home
    assert url.home == home


@pytest.mark.skipif(user == "$USER", reason="user is unset in CI, skip if this is the case")
def test_user():
    url = URL()

    assert url.expandvars("$USER") == user


@pytest.mark.skipif(user == "$USER", reason="user is unset in CI, skip if this is the case")
def test_in_path():
    url = URL()
    # drop the leading slash, otherwise we have to expand into an abspath
    assert url.expandvars("home/$USER/test") == os.path.join("home", user, "test")


@pytest.mark.skipif(user == "$USER", reason="user is unset in CI, skip if this is the case")
def test_in_path_braces():
    url = URL()

    assert url.expandvars("home/${USER}/test") == os.path.join("home", user, "test")
