import os

from remotemanager.connection.url import URL


def test_default():
    url = URL()

    assert url.landing_dir == "$HOME"


def test_landing_dir_update():
    url = URL()

    assert url.cmd("pwd").stdout == os.getcwd()

    url.landing_dir = "/"
    assert url.cmd("pwd").stdout == "/"


def test_landing_dir_set():
    url = URL(landing_dir="/")

    assert url.cmd("pwd").stdout == "/"
