import os
import random
import shutil
import time
import unittest

from remotemanager.connection.url import URL
from remotemanager.utils import random_string


class TestFileMtime(unittest.TestCase):

    def setUp(self):
        """Create 5 files at random intervals, then check that their mtime matches"""
        self.testing_dir = f"file_mtime_test_{random_string()}"
        os.makedirs(self.testing_dir)

        self.files_created = {}

        for i in range(5):

            # sleep a random time between 0 and 3s
            time.sleep(random.random() * 3)

            fname = f'{self.testing_dir}/file-{i}'

            with open(fname, mode="w+", encoding="utf8") as o:
                o.write("foo")
                self.files_created[fname] = int(time.time())

        fname = f'{self.testing_dir}/file-5'
        with open(fname, mode="w+", encoding="utf8") as o:
            o.write("")
            self.files_created[fname] = int(time.time())

    def tearDown(self):
        shutil.rmtree(self.testing_dir)

    def test_run(self):
        url = URL()

        filenames = list(self.files_created.keys())
        files_found = url.utils.file_mtime(filenames)

        for file in filenames:
            # allows a second of "wiggle room" for the tests to be more consistent
            # this means that the test is less likely to capture minor issues,
            # but anything major would still be caught
            assert abs(files_found[file] - self.files_created[file]) <= 1

    def test_run_python(self):
        url = URL()

        filenames = list(self.files_created.keys())
        files_found = url.utils.file_mtime(filenames, python=True)

        for file in filenames:
            # again with 1s leeway
            assert abs(files_found[file] - self.files_created[file]) <= 1

    def test_none(self):
        url = URL()
        files_found = url.utils.file_mtime(["nonefile"])

        assert files_found == {"nonefile": None}

    def test_none_python(self):
        url = URL()
        files_found = url.utils.file_mtime(["nonefile"], python=True)

        assert files_found == {"nonefile": None}

    def test_empty(self):
        url = URL()

        file = os.path.join(self.testing_dir, "file-5")
        files_found = url.utils.file_mtime([file], ignore_empty=True)

        assert files_found[file] is None

    def test_empty_python(self):
        url = URL()

        file = os.path.join(self.testing_dir, "file-5")
        files_found = url.utils.file_mtime([file], python=True, ignore_empty=True)

        assert files_found[file] is None
