import pytest

from remotemanager.connection.url import URL


@pytest.mark.parametrize("address,is_local",
    [
        (None, True),
        ("127.0.0.1", True),
        ("localhost", True),
        ("remote.host", False),
    ]
)
def test_local(address, is_local):
    url = URL(address)

    assert url.is_local == is_local
    if is_local:
        assert url.ssh == ""


def test_userhost():
    url = URL(user='test', host='generic.host.name')

    assert url.userhost == "test@generic.host.name"


def test_userhost_split():
    url = URL('test@generic.host.name')

    assert url.user == "test"
    assert url.host == "generic.host.name"


def test_userhost_nouser():
    url = URL(host='generic.host.name')

    assert url.userhost == "generic.host.name"


def test_set_host_none():
    url = URL(user='test', host='generic.host.name')

    assert url.userhost == "test@generic.host.name"

    url.host = None
    assert url.userhost == "test@localhost"


def test_set_user_none():
    url = URL(user='test', host='generic.host.name')

    assert url.userhost == "test@generic.host.name"

    url.user = None
    assert url.userhost == "generic.host.name"


def test_port():
    url = URL(host="remote.host")
    assert url.port == 22

    url.port = 23
    assert url.port == 23

    assert "-p 23" in url.cmd("ls", dry_run=True).cmd


def test_override_ssh():
    url = URL(user='test', host='generic.host.name')

    assert url.ssh == "ssh -p 22 -q test@generic.host.name"

    url.ssh = "ssh override"
    assert url.ssh == "ssh override"


def test_override_ssh_init_set():
    url = URL(user='test', host='generic.host.name', ssh_override="ssh override")
    assert url.ssh == "ssh override"


def test_clear_override():
    url = URL(user='test', host='generic.host.name', ssh_override="ssh override")
    assert url.ssh == "ssh override"

    url.clear_ssh_override()
    assert url.ssh != "ssh override"


def test_uuid():
    a = URL(user="test", host="generic.a")
    b = URL(user="test", host="generic.b")

    assert a.uuid != b.uuid
    assert a.short_uuid != b.short_uuid


def test_silent_user_failure():
    """Checks for a silent failure where the user accidentally sets the user instead of host"""
    with pytest.raises(ValueError, match="user is set to test, but host is unset.*"):
        URL(user="test")


def test_script():
    url = URL()
    with pytest.raises(NotImplementedError):
        url.script()
