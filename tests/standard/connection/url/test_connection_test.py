from remotemanager.connection.url import URL


def test_connection_test():
    url = URL()

    assert url.connection_test is None
    assert url.connection_data == {}
    assert url.latency is None

    url.test_connection()

    assert url.connection_test is not None
    assert url.connection_test.passed

    assert url.connection_data["latency"] < 0.1
    assert url.connection_test.latency < 0.1
    assert url.latency < 0.1
