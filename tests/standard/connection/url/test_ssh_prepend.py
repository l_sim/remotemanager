import pytest

from remotemanager.connection.url import URL


def generate_url(host=None, prepend=None, init=False):
    if prepend is None:
        return URL(host=host)

    if init:
        return URL(host=host, ssh_prepend=prepend)

    url = URL(host=host)
    url.ssh_prepend = prepend

    return url


def test_basic_none():
    url = generate_url()

    assert "PREPEND" not in url.cmd("ls", dry_run=True)


def test_force_no_prepend():
    url = generate_url()

    assert "PREPEND" not in url.cmd("ls", dry_run=True, prepend=True)


@pytest.mark.parametrize("init", [True, False])
def test_basic(init):
    url = generate_url(prepend="PREPEND", init=init)

    assert "PREPEND" not in url.cmd("ls", dry_run=True)


@pytest.mark.parametrize("init", [True, False])
def test_local(init):
    url = generate_url(prepend="PREPEND", init=init)

    assert "PREPEND" not in url.cmd("ls", dry_run=True, local=True)


@pytest.mark.parametrize("init", [True, False])
def test_force(init):
    url = generate_url(prepend="PREPEND", init=init)

    assert "PREPEND" in url.cmd("ls", dry_run=True, prepend=True)


@pytest.mark.parametrize("init", [True, False])
def test_force_both(init):
    url = generate_url(prepend="PREPEND", init=init)

    assert "PREPEND" in url.cmd("ls", dry_run=True, local=True, prepend=True)


def test_host_basic_none():
    url = generate_url(host="remote.host")

    assert "PREPEND" not in url.cmd("ls", dry_run=True)


def test_host_force_no_prepend():
    url = generate_url(host="remote.host")

    assert "PREPEND" not in url.cmd("ls", dry_run=True, prepend=True)


@pytest.mark.parametrize("init", [True, False])
def test_host_basic(init):
    url = generate_url(host="remote.host", prepend="PREPEND", init=init)

    assert "PREPEND" in url.cmd("ls", dry_run=True)


@pytest.mark.parametrize("init", [True, False])
def test_host_local(init):
    url = generate_url(host="remote.host", prepend="PREPEND", init=init)

    assert "PREPEND" not in url.cmd("ls", dry_run=True, local=True)


@pytest.mark.parametrize("init", [True, False])
def test_host_force(init):
    url = generate_url(host="remote.host", prepend="PREPEND", init=init)

    assert "PREPEND" in url.cmd("ls", dry_run=True, prepend=True)


@pytest.mark.parametrize("init", [True, False])
def test_host_force_both(init):
    url = generate_url(host="remote.host", prepend="PREPEND", init=init)

    assert "PREPEND" in url.cmd("ls", dry_run=True, local=True, prepend=True)
