import pytest

from remotemanager import URL


class TestTimeoutDisable:

    def setup(self, timeout_init=1):
        return URL(timeout=timeout_init, max_timeouts=1)

    def test_timeout_disable_control(self):
        url = self.setup()

        with pytest.raises(RuntimeError):
            url.cmd("sleep 2 && echo 'foo'")

    @pytest.mark.parametrize("timeout", [0, -1, False])
    def test_timeout_disable(self, timeout):
        url = self.setup()

        assert url.cmd("sleep 2 && echo 'foo'", timeout=timeout).stdout == "foo"

    @pytest.mark.parametrize("timeout", [0, -1, False])
    def test_timeout_disable_init(self, timeout):
        url = self.setup(timeout_init=timeout)

        assert url.cmd("sleep 2 && echo 'foo'").stdout == "foo"
