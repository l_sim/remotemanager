import os

import pytest

from remotemanager.connection.url import URL
from remotemanager.utils import random_string


def test_sshpass_override():
    url = URL(host="remote.machine", sshpass_override="sshpass -P override -e ENV")

    assert url.cmd("ls", dry_run=True).cmd == "sshpass -P override -e ENV ssh -p 22 -q remote.machine 'ls'"


def test_sshpass_override_add():
    url = URL(host="remote.machine")
    url.sshpass_override = "sshpass -P override -e ENV"

    assert url.cmd("ls", dry_run=True).cmd == "sshpass -P override -e ENV ssh -p 22 -q remote.machine 'ls'"


def test_sshpass_env():
    passfile = random_string()
    try:
        os.environ["SSHPASSFILE"] = passfile

        with open(passfile, mode="w+", encoding="utf8") as o:
            o.write("foo")

        url = URL(host="remote.machine", envpass="SSHPASSFILE")

        assert url.cmd("ls", dry_run=True).cmd == f"sshpass -f {passfile} ssh -p 22 -q remote.machine 'ls'"
    finally:
        os.remove(passfile)
        os.unsetenv("SSHPASSFILE")


@pytest.mark.skip("not implemented")
def test_sshpass_env_set():
    passfile = random_string()
    try:
        os.environ["SSHPASSFILE"] = passfile

        with open(passfile, mode="w+", encoding="utf8") as o:
            o.write("foo")

        url = URL(host="remote.machine")
        url.envpass = "SSHPASSFILE"

        assert url.cmd("ls", dry_run=True).cmd == f"sshpass -f {passfile} ssh -p 22 -q remote.machine 'ls'"
    finally:
        os.remove(passfile)
        os.unsetenv("SSHPASSFILE")
