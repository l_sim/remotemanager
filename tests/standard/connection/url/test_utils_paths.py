import os

import pytest

from remotemanager.connection.url import URL


@pytest.mark.parametrize("url,expected", [(URL(), True), (URL(user="user", host="host"), False)])
def test_mkdir(url, expected):
    exists = os.getcwd() in url.utils.mkdir('test', dry_run=True).cmd
    assert exists == expected


@pytest.mark.parametrize("url,expected", [(URL(), True), (URL(user="user", host="host"), False)])
def test_touch(url, expected):
    exists =  os.getcwd() in url.utils.touch('test', dry_run=True).cmd
    assert exists == expected


@pytest.mark.parametrize("url,expected", [(URL(), True), (URL(user="user", host="host"), False)])
def test_ls(url, expected):
    exists =  os.getcwd() in url.utils.ls('test', dry_run=True).cmd
    assert exists == expected


@pytest.mark.parametrize("url,expected", [(URL(), True), (URL(user="user", host="host"), False)])
def test_search(url, expected):
    exists =  os.getcwd() in url.utils.search_folder(['file'], 'test', dry_run=True).cmd
    assert exists == expected


@pytest.mark.parametrize("url,expected", [(URL(), True), (URL(user="user", host="host"), False)])
def test_search_single(url, expected):
    exists = os.getcwd() in url.utils.search_folder('file', 'test', dry_run=True).cmd
    assert exists == expected


def test_presence():
    url = URL()
    assert url.utils.file_presence(['file'], dry_run=True).cmd == "stat -c %n,%Y,%s file"


def test_presence_nonlocal():
    url = URL(user="user", host="host")
    assert url.utils.file_presence(['file'], dry_run=True).cmd == "ssh -p 22 -q user@host 'stat -c %n,%Y,%s file'"



