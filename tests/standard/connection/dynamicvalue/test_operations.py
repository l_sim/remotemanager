import pytest

from remotemanager.connection.computers.dynamicvalue import DynamicValue


def test_basic_setup():
    a = DynamicValue(10)

    assert a.value == 10


@pytest.mark.parametrize("a, b, operation, expected", [
    (DynamicValue(4), DynamicValue(10), "a + b", 4 + 10),  # add
    (DynamicValue(4), DynamicValue(10), "a - b", 4 - 10),  # sub
    (DynamicValue(4), DynamicValue(10), "a * b", 4 * 10),  # mul
    (DynamicValue(4), DynamicValue(10), "a / b", 4 / 10),  # div
    (DynamicValue(4), 10, "a + b", 4 + 10),  # add, a dynamic
    (DynamicValue(4), 10, "a - b", 4 - 10),  # sub, a dynamic
    (DynamicValue(4), 10, "a * b", 4 * 10),  # mul, a dynamic
    (DynamicValue(4), 10, "a / b", 4 / 10),  # div, a dynamic
    # While DV(10) + 10 works, 10 + DV(10) does NOT, thanks to using the __op__ of the int, not DV
    # (4, DynamicValue(10), "a + b", 4 + 10),  # add, b dynamic
    # (4, DynamicValue(10), "a - b", 4 - 10),  # sub, b dynamic
    # (4, DynamicValue(10), "a * b", 4 * 10),  # mul, b dynamic
    # (4, DynamicValue(10), "a / b", 4 / 10),  # div, b dynamic
])
@pytest.mark.parametrize("implicit", [True, False])
def test_dynamic_ops(a, b, operation, expected, implicit):

    if implicit:
        c = eval(operation)
    else:
        c = DynamicValue(eval(operation))

    assert c.value == expected


def test_complex_dynamic():
    a = DynamicValue(4)
    b = DynamicValue(10)
    c = DynamicValue( (a + b) / (a - b) )

    assert c.value == (4 + 10) / (4 - 10)


def test_direct_setup():
    a = DynamicValue(1, 2, "add")

    assert a.value == 3


def test_direct_setup_none():
    a = DynamicValue(1, None, None)

    assert a.value == 1


def test_direct_setup_default():
    a = DynamicValue(None, None, None, 10)

    assert a.value == 10
    assert a.default == 10


def test_improper_instantiation():
    """
    This test is copied as-is from the notebooks suite

    The result is... questionable, since we're trying to evaluate
    None * 4
    we'd expect an error
    """
    a = DynamicValue(None, 4, "mul")

    assert a.value is None


def test_direct_setup_dynamics():
    a = DynamicValue(1)
    b = DynamicValue(2)

    c = DynamicValue(a, b, "add")

    assert c.value == 3


def test_incorrect_setup_no_b():
    with pytest.raises(ValueError):
        DynamicValue(1, None, "add")


def test_incorrect_setup_no_op():
    with pytest.raises(ValueError):
        DynamicValue(1, 2, None)
