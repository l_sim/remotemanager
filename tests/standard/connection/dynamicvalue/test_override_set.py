from typing import Tuple

from remotemanager.connection.computers.substitution import Substitution


class TestSubstitutionInternals:

    def setUp(self) -> Tuple[Substitution, Substitution, Substitution]:
        a = Substitution(target="a", name="a")
        b = Substitution(target="b", name="b")
        c = Substitution(target="c", name="c")

        return a, b, c

    def test_set_link(self):

        a, b, c = self.setUp()

        a.value = 1
        b.value = 2
        assert c.value is None

        c.value = a + b
        assert c.value == 3

    def test_set_override(self):

        a, b, c = self.setUp()

        c.value = 10
        assert c.value == 10

        c.value = a + b
        assert c.value is None

    def test_set_link_override(self, capsys):

        a, b, c = self.setUp()

        a.value = 1
        b.value = 2
        c.value = a + b
        assert c.value == 3

        c.value = 10
        assert c.value == 10
        assert "WARNING!" in capsys.readouterr().out

    def test_set_link_override_none(self, capsys):

        a, b, c = self.setUp()

        c.value = a + b
        assert c.value is None

        c.value = 10
        assert c.value == 10
        assert "WARNING!" in capsys.readouterr().out
