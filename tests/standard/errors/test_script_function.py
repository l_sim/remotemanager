"""
Collection Dataset script based tests
"""
from remotemanager import Computer
from tests.utils.base_class import BaseTestClass


class TestDatasetScriptsError(BaseTestClass):

    def test_none_function(self):
        template = """
echo >&2 "#FOO#"
"""

        url = Computer(template=template)

        self.create_dataset(function=None, url=url)

        self.ds.append_run({"foo": "bar"})

        results = self.run_ds()

        assert results[0] is None
        assert self.ds.errors[0] == "bar"
