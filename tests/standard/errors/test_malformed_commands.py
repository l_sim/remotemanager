"""
Test suite to ensure that malformed commands fail in an expected way
"""
import re

import pytest

from remotemanager import URL
from tests.utils.base_class import BaseTestClass


def add(a, b):
    # test_malformed_commands
    return a + b


class TestMalformedCommands(BaseTestClass):

    def test_wrong_python(self):
        url = URL(python="foo")

        self.create_dataset(add, url=url)
        self.ds.append_run({"a": 1, "b": 2})
        self.ds.run()

        self.ds.wait(1, 10)

        self.ds.fetch_results()

        assert "foo: command not found" in self.ds.errors[0]

    def test_wrong_submitter(self):
        url = URL(submitter="foo")

        self.create_dataset(add, url=url)
        self.ds.append_run({"a": 1, "b": 2})
        self.ds.run()

        self.ds.wait(1, 10)

        self.ds.fetch_results()

        assert "foo: command not found" in self.ds.errors[0]

    def test_wrong_shell(self):
        url = URL(shell="foo")

        self.create_dataset(add, url=url)
        self.ds.append_run({"a": 1, "b": 2})

        match = re.compile(r"Dataset encountered an issue:\n.*foo: command not found", re.MULTILINE)
        with pytest.raises(RuntimeError, match=match):
            self.run_ds()
