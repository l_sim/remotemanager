"""Ensures that manifest error reporting is done correctly"""

from tests.utils.base_class import BaseTestClass


def raise_exception(exception):
    # test_manifest_reporting
    raise RuntimeError(exception)


class TestRuntimeErrors(BaseTestClass):

    def test_indented_error(self):
        error = "first error line\n    indented line"

        self.create_dataset(
            raise_exception,
            skip=False,
        )

        self.ds.append_run({"exception": error})

        self.run_ds()

        full_error = self.ds.runners[0].full_error
        clipped_error = "\n".join(full_error.strip("\n").split("\n")[-2:])

        print("full error:")
        print(full_error)
        print("clipped error:")
        print(clipped_error)

        assert f"RuntimeError: {error}" == clipped_error
