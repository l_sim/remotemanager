import os.path

from tests.utils.base_class import BaseTestClass


def fn(inp):
    # test_malformed_extra.py
    return inp


class TestMalformedExtra(BaseTestClass):
    def test_basic(self):
        self.create_dataset(fn)

        self.ds.append_run({"inp": "test"}, extra="foo")

        self.run_ds()

        assert "foo: command not found" in self.ds.errors[0]

    def test_run_dir(self):
        self.create_dataset(fn, run_dir="inner_dir")

        self.ds.append_run({"inp": "test"}, extra="foo")

        self.run_ds()

        assert "foo: command not found" in self.ds.errors[0]

        assert os.path.exists(os.path.join(self.ds.remote_dir, self.ds.run_dir))
