import os.path

from remotemanager.dataset.runner import RunnerFailedError
from tests.utils.base_class import BaseTestClass


def fail_on_file(inp):
    # test_rerun_fail
    import os

    if os.path.exists("fail.flag"):
        raise RuntimeError("failing as requested by presence of fail.flag file")

    return inp


class TestFailAndRerun(BaseTestClass):
    def test_rerun(self):
        self.create_dataset(fail_on_file)

        self.ds.append_run({"inp": 1})

        # initial run should fail
        file = os.path.join(self.ds.remote_dir, "fail.flag")
        os.makedirs(self.ds.remote_dir)
        with open(file, "w+") as o:
            o.write("")

        self.run_ds()

        assert self.ds.failed

        try:
            os.remove(file)
        except FileNotFoundError:
            pass

        self.run_ds(force=False)
        assert self.ds.results == [
            RunnerFailedError("RuntimeError: failing as requested by presence of fail.flag file")]

        self.run_ds(force=True)
        assert self.ds.results == [1]
