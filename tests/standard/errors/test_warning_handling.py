from remotemanager.dataset.runner import RunnerFailedError
from tests.utils.base_class import BaseTestClass


def warntest(warn):
    import warnings
    # test_warning_handling
    if warn:
        warnings.warn("this is a warning")
    else:
        raise RuntimeError("this is an error")


class TestWarningVsError(BaseTestClass):
    def test_run(self):
        self.create_dataset(warntest)

        self.ds.append_run({"warn": True})
        self.ds.append_run({"warn": False})

        self.run_ds()

        assert self.ds.results == [None, RunnerFailedError('RuntimeError: this is an error')]
        assert self.ds.errors == ['warnings.warn("this is a warning")', 'RuntimeError: this is an error']
