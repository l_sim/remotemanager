import os

import numpy as np
import pytest

from remotemanager.serialisation import serialjson, serialdill, serialyaml, serialjsonpickle
from remotemanager.utils import random_string

objects = [
    "foo",
    1,
    -10,
    3.14,
    [1, 2, 3],
    {1, 2, 3},
    (1, 2, 3),
    {"a": 1, "b": 2, "c": 3},
    {},
    True,
    False,
    None,
]


adv_objects = [
    [np.float32(12), np.int32(42)],
    np.array([1, 2, 3]),
    np.double(3),
]


class NonSerialisable:
    """Simple class that should fail when being serialised by "basic" serialisers"""

    __slots__ = ["_str"]

    def __init__(self):
        self._str = random_string()

    def __eq__(self, other: "NonSerialisable") -> bool:
        return self._str == other._str


def test_fail_to_dump():
    """
    Ensure that our "non-serialisable" object actually is non serialisable

    We have to use json, since yaml doesn't raise an error
    """

    srl = serialjson()

    obj = NonSerialisable()

    with pytest.raises(TypeError):
        srl.dumps(obj)


@pytest.mark.parametrize("test_class", [serialjson, serialdill, serialyaml, serialjsonpickle])
@pytest.mark.parametrize("obj", objects)
class TestSerialiser:

    @staticmethod
    def get_file(extension: str) -> str:
        return os.path.abspath(f"test_serial_{random_string()}.{extension}")

    def test_dump_load(self, test_class, obj):
        srl = test_class()
        file = self.get_file(srl.extension)

        srl.dump(obj, file)

        new = srl.load(file)

        assert obj == new
        assert type(obj) == type(new)

        os.remove(file)


@pytest.mark.parametrize("test_class", [serialdill, serialjsonpickle])
@pytest.mark.parametrize("obj", adv_objects)
class TestAdvancedSerialiser:

    @staticmethod
    def get_file(extension: str) -> str:
        return os.path.abspath(f"test_serial_{random_string()}.{extension}")

    def test_dump_load(self, test_class, obj):
        srl = test_class()
        file = self.get_file(srl.extension)

        srl.dump(obj, file)

        new = srl.load(file)

        if isinstance(obj, np.ndarray):
            assert np.array_equal(obj, new)
        else:
            assert obj == new
        assert type(obj) == type(new)

        os.remove(file)


@pytest.mark.parametrize("test_class", [serialdill, serialjsonpickle])
class TestCustomObj:

    @staticmethod
    def get_file(extension: str) -> str:
        return os.path.abspath(f"test_serial_{random_string()}.{extension}")

    def test_dump_load_advanced(self, test_class):
        srl = test_class()
        file = self.get_file(srl.extension)

        obj = NonSerialisable()

        srl.dump(obj, file)

        new = srl.load(file)

        assert obj == new
        assert type(obj) == type(new)

        os.remove(file)
