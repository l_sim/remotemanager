import copy
from io import StringIO

import pytest
from BigDFT.IO import read_xyz

from remotemanager.serialisation import serialjsonpickle, serialdill, serialjson
from tests.utils.base_class import BaseTestClass

istr = """2 angstroemd0
free
N 0.0000 0.0000 0.5488
N 0.0000 0.0000 -0.5488"""
sys = read_xyz(StringIO(istr))


def f(x):
    return x


class TestRunComplex(BaseTestClass):

    def test_run_with_complex_pickle(self):
        self.create_dataset(f, serialiser=serialjsonpickle())

        for _ in range(3):
            self.ds.append_run({"x": sys})
        self.ds.append_run({"x": copy.deepcopy(sys)})  # simulate a notebook restart

        assert len(self.ds.runners) == 1

        assert self.run_ds() == [sys]

    def test_run_with_complex_dill(self):
        self.create_dataset(f, serialiser=serialdill())

        for _ in range(3):
            self.ds.append_run({"x": sys})
        self.ds.append_run({"x": copy.deepcopy(sys)})  # simulate a notebook restart

        assert len(self.ds.runners) == 1

        assert self.run_ds() == [sys]

    def test_run_with_complex_json(self):
        self.create_dataset(f, serialiser=serialjson())

        with pytest.raises(TypeError):
            self.ds.append_run({"x": sys})
