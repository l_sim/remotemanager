import pytest

from remotemanager import Dataset


def remote_load(foo):
    return foo

def remote_dump(foo):
    return foo


def test_reserved_function_name_load():
    with pytest.raises(ValueError):
        Dataset(remote_load)


def test_reserved_function_name_dump():
    with pytest.raises(ValueError):
        Dataset(remote_dump)
