"""Checks for an issue where Dataset would behave strangely when unable to connect to the remote"""
from remotemanager import Dataset, URL
from remotemanager.serialisation.serialdill import serialdill
from tests.utils.base_class import BaseTestClass


def f(string: str):
    class Test:

        __slots__ = ["_str"]

        def __init__(self, string: str):
            self._str = string

        def __eq__(self, other) -> bool:
            return other._str == self._str

    return Test(string)


class TestDisconnect(BaseTestClass):

    def test_dc(self):
        self.create_dataset(f, serialiser=serialdill(), url=URL("127.0.0.1"))

        self.ds.append_run({"string": "foo"})

        expected = f("foo")

        assert self.run_ds() == [expected]

        # a disconnect can be simulated by substituting the url with one that goes nowhere
        test_ds = Dataset(f, name=self.ds.name, serialiser=serialdill(), url=URL("none"))
        test_ds.append_run({"string": "foo"})

        assert test_ds.url.ping(n=1, timeout=1) == -1

        assert self.run_ds() == [expected]
