import unittest

import pytest

from remotemanager.serialisation.serial import serial


def test_dump():
    obj = serial()

    with pytest.raises(NotImplementedError):
        obj.dump("foo", "file.ext")

def test_dumps():
    obj = serial()

    with pytest.raises(NotImplementedError):
        obj.dumps("foo")

def test_load():
    obj = serial()

    with pytest.raises(NotImplementedError):
        obj.load("file.ext")

def test_loads():
    obj = serial()

    with pytest.raises(NotImplementedError):
        obj.loads("foo")


class TestListWrapping(unittest.TestCase):

    def setUp(self):
        self.obj = serial()

    def test_set(self):
        test = {1, 2, 3, 4}

        wrapped = self.obj.wrap_to_list(test)

        assert "~SERIALISEDSET~" in wrapped
        assert isinstance(wrapped, list)

        recover = self.obj.wrap_to_list(wrapped)

        assert recover == test

    def test_tuple(self):
        test = (1, 2, 3, 4)

        wrapped = self.obj.wrap_to_list(test)

        assert "~SERIALISEDTUPLE~" in wrapped
        assert isinstance(wrapped, list)

        recover = self.obj.wrap_to_list(wrapped)

        assert recover == test

    def test_list(self):
        test = [1, 2, 3, 4]

        wrapped = self.obj.wrap_to_list(test)

        assert "~SERIALISEDSET~" not in wrapped
        assert "~SERIALISEDTUPLE~" not in wrapped
        assert isinstance(wrapped, list)

        recover = self.obj.wrap_to_list(wrapped)

        assert recover == test


class TestProperties(unittest.TestCase):

    def setUp(self):
        self.obj = serial()

    def test_callstring(self):
        with pytest.raises(NotImplementedError):
            assert self.obj.callstring

    def test_extension(self):
        with pytest.raises(NotImplementedError):
            assert self.obj.extension

    def test_importstring(self):
        with pytest.raises(NotImplementedError):
            assert self.obj.importstring

    def test_bytes_default(self):
        assert not self.obj.bytes

    def test_writemode_default(self):
        assert self.obj.write_mode == "w+"

    def test_readmode_default(self):
        assert self.obj.read_mode == "r"

    def test_load_func_default(self):
        assert self.obj.loadstring == "load"
        assert self.obj.loadfunc_name == "remote_load"

    def test_dump_func_default(self):
        assert self.obj.dumpstring == "dump"
        assert self.obj.dumpfunc_name == "remote_dump"

    def test_loadfunc(self):
        with pytest.raises(NotImplementedError):
            assert self.obj.loadfunc()

    def test_dumpfunc(self):
        with pytest.raises(NotImplementedError):
            assert self.obj.dumpfunc()
