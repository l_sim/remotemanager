"""
This checks for a bug where the connection_test object would contain the url
containing it. This would cause a serial-loop, breaking things
"""


from remotemanager import URL
from tests.utils.base_class import BaseTestClass


def f(a):
    return a


class TestNonLoop(BaseTestClass):


    def test_non_loop(self):
        self.create_dataset(f, url=URL())

        self.ds.url.test_connection()

        assert self.ds.url.connection_test is not None

        self.ds.append_run({"a": 1})
        assert self.run_ds() == [1]
