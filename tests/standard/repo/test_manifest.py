import os
import random
import unittest
from datetime import datetime, timezone
from unittest import mock

from remotemanager.dataset.repo import Manifest, date_format
from remotemanager.utils import integer_time_wait
from remotemanager.utils.uuid import generate_uuid

base_path = os.path.split(__file__)[0]


def test_path_handling():
    """Check that the path is properly sanitised"""
    nofile = Manifest("foo", path_override="/test/path")
    withfile = Manifest("bar", path_override="/test/path/file.py")

    assert nofile.basepath == withfile.basepath


def test_now():
    """Check that the now function actually returns the current time"""
    manifest = Manifest("foo")

    now = datetime.now().astimezone(timezone.utc)

    assert datetime.strftime(now, date_format) == manifest.now()


class TestManifest(unittest.TestCase):
    def setUp(self):
        """Create a local manifest file"""

        integer_time_wait()

        ids = ["a", "b", "c", "d", "e"]

        self.manifest_filename = f"{generate_uuid(str(random.random))}.manifest"

        self.instances = {
            idx: Manifest(
                f"uuid_{idx}", path_override=base_path, name_override=self.manifest_filename
            )
            for idx in ids
        }

        for idx, manifest in self.instances.items():
            self.apply_mock(manifest)

        self.timestring = self.instances["a"].now()
        assert self.timestring == "1970-01-01 00:00:00 UTC"

        self.ts0 = self.instances["a"].to_timestamp(self.timestring)

    def apply_mock(self, manifest):
        manifest.now = mock.MagicMock(
            return_value=datetime.strftime(
                datetime.fromtimestamp(0).astimezone(tz=timezone.utc), date_format
            )
        )

    def get_reader(self, name="dataset_placeholder"):
        """Helper tool to get a reader at instance uuid name"""
        reader = Manifest(
            instance_uuid=name, path_override=base_path, name_override=self.manifest_filename
        )
        self.apply_mock(reader)
        return reader

    def test_mock(self):
        """meta test to make sure that the mock is working"""
        assert self.instances["a"].now() == "1970-01-01 00:00:00 UTC"

    def test_path(self):
        """Check that the file path is sensible when overridden"""
        assert self.instances["a"].path == os.path.join(
            base_path, self.instances["a"].filename
        )

    def test_write(self):
        """Check that we can write to the file"""
        self.instances["a"].write("foo")

        with open(self.instances["a"].path, encoding="utf8") as o:
            content = o.read()

        assert content == f"{self.timestring} [uuid_a] foo\n"

    def test_runner_mode(self):
        """Make sure runner mode is default False"""
        assert not self.instances["a"].runner_mode

    def test_get_data_one(self):
        """Test get_data on one call"""
        self.instances["a"].write("foo")

        assert len(self.instances["a"].get_data()) == 1
        assert self.instances["a"].get_data()[0] == f"{self.timestring} [uuid_a] foo\n"

    def test_get_data_many(self):
        """
        Test get data on many writes
        """
        expected_output = []
        for idx, manifest in self.instances.items():
            line = f"report from idx {idx}"
            manifest.write(line)
            expected_output.append(f"{self.timestring} [uuid_{idx}] {line}\n")

        reader = self.get_reader()

        assert reader.get_data() == expected_output

    def test_get_data_empty(self):
        """Test that we don't error out if there's no data on get"""
        assert self.get_reader().get_data() == []

    def test_parse_string(self):
        """
        Check that we can properly parse a string,
        since this is how Dataset uses it
        """
        inpstr = [
            "2000-01-01 00:00:00 UTC [uuid_a] foo",
            "2000-01-01 00:00:00 UTC [uuid_b] foo",
            "2000-01-01 00:00:00 UTC [uuid_c] foo",
            "2000-01-01 00:00:01 UTC [uuid_a] bar",
        ]
        inpstr = "\n".join(inpstr)

        reader = self.get_reader()

        ts = int(datetime(2000, 1, 1, 0, 0).timestamp())

        assert reader.parse_log(string=inpstr)["log"]["uuid_a"] == [
            (ts, "foo"),
            (ts + 1, "bar"),
        ]

    def test_get(self):
        """Check that get works as expected"""
        self.instances["a"].write("foo")

        reader = self.get_reader()

        assert reader.get("uuid_a") == [f"{self.timestring} [uuid_a] foo"]
        assert not reader.get("uuid_b")
        assert not reader.get("uuid_z")

    def test_last_time(self):
        """
        Test last time on many writes
        """
        expected_output = {}
        for idx, manifest in self.instances.items():
            line = "state"
            manifest.write(line)
            expected_output[f"uuid_{idx}"] = self.ts0

        reader = self.get_reader()
        assert reader.last_time("state") == expected_output

    def test_multiline(self):
        """test get on a multiline string"""
        inp = "multiline\nstring\nfor testing\npurposes"
        self.instances["a"].write(inp)

        reader = self.get_reader()

        assert reader.get("uuid_a") == [f"{self.timestring} [uuid_a] {inp}"]

    def test_parse_multiline(self):
        """test parse on multiline string"""
        inp = "multiline\nstring\nfor testing\npurposes"
        self.instances["a"].write(inp)

        reader = self.get_reader()

        print(reader.parse_log())

        assert reader.parse_log()["log"] == {"uuid_a": [(self.ts0, inp)]}

    def test_cutoff(self):
        """Check that the cutoff correctly ignores entries"""
        inpstr = [
            "2000-01-01 00:00:00 UTC [uuid_a] foo",
            "2000-01-01 00:00:00 UTC [uuid_a] foo",
            "2000-01-01 00:00:00 UTC [uuid_a] foo",
            "2000-01-01 00:01:00 UTC [uuid_a] bar",
        ]
        cutoff = int(
            datetime.strptime("2000-01-01 00:00:30 UTC", date_format).timestamp()
        )

        reader = self.get_reader()
        assert (
            len(
                reader.parse_log(string="\n".join(inpstr), cutoff=cutoff)["log"][
                    "uuid_a"
                ]
            )
            == 1
        )
        assert len(reader.parse_log(string="\n".join(inpstr))["log"]["uuid_a"]) == 4

    def tearDown(self):
        """Clear up the testing paths"""
        try:
            os.remove(self.instances["a"].path)
        except FileNotFoundError:
            pass
