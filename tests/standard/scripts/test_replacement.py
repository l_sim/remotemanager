import unittest

import pytest

from remotemanager.script.script import Script


class TestTemplates(unittest.TestCase):

    def test_basic(self):
        template = "foo"

        script = Script(template=template)

        assert script.script() == template

    def test_no_input(self):
        with pytest.raises(TypeError, match=r".*missing.*positional argument.*"):
            Script()

    def test_param(self):
        template = "foo=#FOO#"

        script = Script(template=template)

        assert script.script() == ""

        script.foo = "bar"

        assert script.script() == "foo=bar"

    def test_default(self):
        template = "foo=#FOO:default=4#"

        script = Script(template=template)

        assert script.script() == "foo=4"

        script.foo = 10

    def test_negative(self):
        template = """foo = #foo:default=10#
#foo#
#foo is a value#
# foo#
#foo #
# foo #
"""

        script = Script(template=template)

        assert script.script() == """foo = 10
10
#foo is a value#
# foo#
#foo #
# foo #
"""

    def test_on_init(self):
        template = "foo=#FOO#"

        script = Script(template=template, foo=4)

        assert script.script() == "foo=4"

    def test_on_init_mod(self):
        template = "foo=#FOO#"

        script = Script(template=template, foo=4)

        script.foo = 10

        assert script.script() == "foo=10"
