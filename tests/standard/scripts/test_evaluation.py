import pytest

from remotemanager.script.script import Script


class TestTemplates:

    def test_arithmetic(self):
        template = """
foo=#FOO:default={a*b}#
a=#A:default=1#
b=#B:default=4#
"""
        script = Script(template=template)

        assert script.script() == """
foo=4
a=1
b=4
"""

    def test_bool(self):
        template = """
foo=#FOO:default={a>b}#
a=#A:default=1#
b=#B:default=4#
"""
        script = Script(template=template)

        assert script.script() == """
foo=False
a=1
b=4
"""

        script.a = 20

        assert script.script() == """
foo=True
a=20
b=4
"""

    def test_recall(self):
        template = """foo = #foo:default=bar#"""

        script = Script(template=template)

        assert script.script() == "foo = bar"
        assert script.script() == "foo = bar"

    def test_recall_update(self):
        template = """foo = #foo:default=bar#"""

        script = Script(template=template)

        assert script.script() == "foo = bar"

        script.foo = "baz"
        assert script.script() == "foo = baz"

    def test_recall_calc(self):
        template = """
a=#a:default=1#
b=#b:default=4#
c=#c:default={a+b}#
"""

        script = Script(template=template)

        assert script.script() == "\na=1\nb=4\nc=5\n"
        assert script.script() == "\na=1\nb=4\nc=5\n"

    def test_recall_calc_update(self):
        template = """
a=#a:default=1#
b=#b:default=4#
c=#c:default={a+b}#
"""

        script = Script(template=template)

        assert script.script() == "\na=1\nb=4\nc=5\n"

        script.b = 10
        assert script.script() == "\na=1\nb=10\nc=11\n"

    def test_divide(self):
        template = """
a=#a:default=1#
b=#b:default=5#
c=#c:default={a/b}#
"""

        script = Script(template=template)

        assert script.script() == "\na=1\nb=5\nc=1\n"

        script.a = 10
        assert script.script() == "\na=10\nb=5\nc=2\n"

    def test_divide_float(self):
        template = """
a=#a:default=1#
b=#b:default=5#
c=#c:default={a/b}:format=float#
"""

        script = Script(template=template)

        assert script.script() == "\na=1\nb=5\nc=0.2\n"

        script.a = 10
        assert script.script() == "\na=10\nb=5\nc=2.0\n"

    def test_calc_before(self):
        template = """
c=#c:default={a+b}#
a=#a:default=1#
b=#b:default=4#
"""

        script = Script(template=template)

        assert script.script() == "\nc=5\na=1\nb=4\n"

    def test_multi_link(self):
        """
        Checks for a bug where calling _link_subs repeatedly would raise an exception
        """
        template = """
foo=#FOO:default={a*b}#
a=#A:default=1#
b=#B:default=4#
"""
        script = Script(template=template)

        script._link_subs()
        script._link_subs()
        script._link_subs()


@pytest.mark.parametrize("source,match", [("1*None", ".*unsupported operand type.*")])
def test_errors(source: str, match: str):
    template = f"#a:default={{{source}}}#"
    print(f"evaluating template: {template}")

    with pytest.raises(ValueError, match=match):
        Script(template=template).script()


def test_recursion():
    """linking values to each other is dangerous, check that a proper error is raised"""
    template = """a=#a:default={b}#\nb=#b:default={a}#"""

    with pytest.raises(RecursionError):
        Script(template=template).script()


def test_recursion_setting():
    """recursion is avoided if a value is set"""
    template = """a=#a:default={b}#\nb=#b:default={a}#"""

    assert Script(template=template).script(a=10) == "a=10\nb=10"


def test_recursion_set_both():
    """if both values are set, ignore the links"""
    template = """a=#a:default={b}#\nb=#b:default={a}#"""

    assert Script(template=template).script(a=10, b=20) == "a=10\nb=20"
