from remotemanager.connection.computers.computer import Computer
from remotemanager.script.script import Script
from tests.utils.base_class import BaseTestClass


def test_extra():
    template = "echo #foo#\n#extra#"

    script = Script(template=template)

    script.foo = "foo"
    script.extra = "extra content"

    assert script.script() == "echo foo\nextra content"


def func(a):
    return a


class TestExtraDataset(BaseTestClass):

    def test_extra_dataset(self):
        self.create_dataset(func, extra="# extra_content")
        self.ds.append_run({"a": 1})

        self.ds.run(dry_run=True)

        assert "# extra_content" in self.ds.runners[0].jobscript.content

    def test_extra_append(self):
        self.create_dataset(func)
        self.ds.append_run({"a": 1}, extra="# extra_content")

        self.ds.run(dry_run=True)

        assert "# extra_content" in self.ds.runners[0].jobscript.content

    def test_extra_run(self):
        self.create_dataset(func)
        self.ds.append_run({"a": 1})

        self.ds.run(dry_run=True, extra="# extra_content")

        assert "# extra_content" in self.ds.runners[0].jobscript.content


class TestExtraDatasetComputer(BaseTestClass):

    def test_extra_dataset(self):
        url = Computer(template="#foo#\n#extra#")

        self.create_dataset(func, url=url, extra="# extra_content")
        self.ds.append_run({"a": 1})

        self.ds.run(dry_run=True)

        assert "# extra_content" in self.ds.runners[0].jobscript.content

    def test_extra_append(self):
        url = Computer(template="#foo#\n#extra#")

        self.create_dataset(func, url=url)
        self.ds.append_run({"a": 1}, extra="# extra_content")

        self.ds.run(dry_run=True)

        assert "# extra_content" in self.ds.runners[0].jobscript.content

    def test_extra_run(self):
        url = Computer(template="#foo#\n#extra#")

        self.create_dataset(func, url=url)
        self.ds.append_run({"a": 1}, extra="# extra_content")

        self.ds.run(dry_run=True)

        assert "# extra_content" in self.ds.runners[0].jobscript.content
