from remotemanager.script import Script


def test_case():
    template = """
value = #value:default="foo"#

# value is #value#

#Value#
#VALUE#
#VaLuE#
"""

    test = Script(template=template)
    assert test.script() ==  """
value = "foo"

# value is "foo"

"foo"
"foo"
"foo"
"""
