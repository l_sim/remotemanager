import pytest

from remotemanager.connection.computers.computer import Computer
from remotemanager.script import Script


@pytest.mark.parametrize("cls", [Script, Computer])
def test_default(cls):
    template = "a = #a#"

    script = cls(template=template)

    assert script.a.empty_treatment == "line"


@pytest.mark.parametrize("style", ["line", "local", "ignore"])
@pytest.mark.parametrize("cls", [Script, Computer])
def test_set(style: str, cls):
    template = f"a = #a:empty_treatment={style}#"

    script = cls(template=template)

    assert script.a.empty_treatment == style


@pytest.mark.parametrize("cls", [Script, Computer])
def test_invalid(cls):
    template = f"a = #a:empty_treatment=foo#"

    with pytest.raises(ValueError, match=r"must be one of the available styles"):
        cls(template=template)


@pytest.mark.parametrize(
    "style,expected",
    [
        ("line", ""),
        ("local", "a = 10"),
        ("ignore", "a = 10#b:empty_treatment=ignore#"),
    ]
)
@pytest.mark.parametrize("cls", [Script, Computer])
def test_output(style: str, expected: str, cls):
    template = f"a = #a:empty_treatment={style}##b:empty_treatment={style}#"

    script = cls(template=template)
    script.a = 10
    assert script.script() == expected


@pytest.mark.parametrize(
    "style,expected",
    [
        ("line", ""),
        ("local", "a = \nb = "),
        ("ignore", "a = #a:empty_treatment=local#\nb = #b:empty_treatment=ignore#"),
    ]
)
@pytest.mark.parametrize("cls", [Script, Computer])
def test_exec_override(style, expected, cls):
    template = "a = #a:empty_treatment=local#\nb = #b:empty_treatment=ignore#"

    script = cls(template=template)

    assert script.script(empty_treatment=style) == expected


@pytest.mark.parametrize("cls", [Script, Computer])
def test_invalid_exec_override(cls):
    template = f"a = #a#"

    script = cls(template=template)

    with pytest.raises(ValueError, match=r"must be one of the available styles"):
        script.script(empty_treatment="foo")


@pytest.mark.parametrize(
    "style,expected",
    [
        ("line", ""),
        ("local", "a = \nb = "),
        ("ignore", "a = #a:empty_treatment=local#\nb = #b:empty_treatment=ignore#"),
    ]
)
@pytest.mark.parametrize("cls", [Script, Computer])
def test_global_override(style, expected, cls):
    template = f"a = #a:empty_treatment=local#\nb = #b:empty_treatment=ignore#"

    script = cls(template=template, empty_treatment=style)

    assert script.script() == expected


@pytest.mark.parametrize("cls", [Script, Computer])
def test_invalid_global_override(cls):
    template = f"a = #a#"

    with pytest.raises(ValueError, match=r"must be one of the available styles"):
        cls(template=template, empty_treatment="foo")


@pytest.mark.parametrize(
    "style,diffstyle,expected",
    [
        ("line", "local", ""),
        ("local", "ignore", "a = \nb = "),
        ("ignore", "line", "a = #a:empty_treatment=local#\nb = #b:empty_treatment=ignore#"),
    ]
)
@pytest.mark.parametrize("cls", [Script, Computer])
def test_exec_overrides_global(style, diffstyle, expected, cls):
    template = f"a = #a:empty_treatment=local#\nb = #b:empty_treatment=ignore#"

    script = cls(template=template, style=diffstyle)

    assert script.script(empty_treatment=style) == expected
