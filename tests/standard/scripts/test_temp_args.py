import unittest

from remotemanager.script.script import Script


class TestTemplates(unittest.TestCase):

    def test_param(self):
        template = "foo=#FOO:default=10#"

        script = Script(template=template)

        assert script.script() == "foo=10"

        assert script.script(foo=20) == "foo=20"

    def test_calc(self):
        template = "foo=#FOO:default=10#\nbar=#bar:default={foo*2}#"

        script = Script(template=template)

        assert script.script() == "foo=10\nbar=20"

        assert script.script(foo=20) == "foo=20\nbar=40"
