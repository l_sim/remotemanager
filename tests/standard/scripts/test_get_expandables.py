import pytest

from remotemanager.script.script import _get_expandables


@pytest.mark.parametrize(
    "i,o",
    [
        ("{a}", ["a"]),
        ("{a}+{a}", ["a", "a"]),
        ("{a+a}", ["a+a"]),
        ("{ {'a': 1, 'b': 2} }", ["{'a': 1, 'b': 2}"]),
        ("{{'a': 1, 'b': 2}}", ["{'a': 1, 'b': 2}"]),
    ]
)
def test_expandables_extraction(i: str, o: list):
    assert _get_expandables(i) == o
