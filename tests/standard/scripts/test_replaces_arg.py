import unittest

from remotemanager import Computer


class TestTemplate(unittest.TestCase):

    def generate_computer(self, template_lines: list, **kwargs) -> Computer:
        """
        Generates a computer based on a list of lines,
        which will be taken as a template
        """
        template = "\n".join(template_lines)

        return Computer(template, **kwargs)

    def test_replaces(self):
        template = ["foo=#foo:default={bar}:replaces=bar#", "bar=#bar#"]

        computer = self.generate_computer(template_lines=template)

        assert "foo" not in computer.required
        assert "bar" not in computer.required

    def test_replaces_oneway(self):
        """
        a is not optional, and replaces b

        in no case should b be required, only a
        """
        template = ["#a:optional=False:replaces=b#", "#b#"]

        computer = self.generate_computer(template_lines=template)

        assert computer.required == ["a"]

    def test_replaces_oneway_both(self):
        """
        a replaces b, but b is required (not a)

        In this case, both should be "required", but disappear when one is specified
        """
        template = ["#a:replaces=b#", "#b:optional=False#"]

        computer = self.generate_computer(template_lines=template)

        assert sorted(computer.required) == ["a", "b"]

        computer.a = "test"
        assert computer.missing == []

        computer = self.generate_computer(template_lines=template)
        computer.b = "test"
        assert computer.missing == []

    def test_replaces_pair(self):
        """
        Two "linked" variables

        Both are required, but since they replace one another, the list can be cleared when one is specified
        """
        template = ["#a:optional=False:replaces=b#", "#b:optional=False:replaces=a#"]

        computer = self.generate_computer(template_lines=template)

        assert "a" in computer.required
        assert "b" in computer.required

        computer.a = "test"
        assert computer.missing == []

        computer = self.generate_computer(template_lines=template)
        computer.b = "test"
        assert computer.missing == []

    def test_replaces_required(self):
        template = ["#a:default={bar}:replaces=b#", "#b:optional=False#"]

        computer = self.generate_computer(template_lines=template)

        assert "a" in computer.required
        assert "b" in computer.required

        computer.a = "test"

        assert computer.missing == []
