from remotemanager.script.script import Script


def test_force_static():
    template = """
a = #a:default=10#

b = #b:default={a}#
c = #c:default={a}:static=True#
"""
    script = Script(template=template)

    assert script.script() == """
a = 10

b = 10
c = {a}
"""
