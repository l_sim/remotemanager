import pytest

from remotemanager.script.script import Script


@pytest.mark.parametrize("style", ["default", "value"])
@pytest.mark.parametrize("reverse", [True, False])
class TestTemplates:

    def test_dict(self, style, reverse):
        spec = 'd = #d:{style}={{"a": 1, "b": 2}}:hidden=True#'.replace("{style}", style)
        template = """a = #a:default={d['a']}#
b = #b:default={d['b']}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == """a = 1
b = 2"""

    def test_dict_spaced(self, style, reverse):
        spec = 'd = #d:{style}={ {"a": 1, "b": 2} }:hidden=True#'
        template = """a = #a:default={d['a']}#
b = #b:default={d['b']}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == """a = 1
b = 2"""

    def test_list(self, style, reverse):
        spec = 'd = #d:{style}={[1, 2]}:hidden=True#'
        template = """a = #a:default={d[0]}#
b = #b:default={d[1]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == """a = 1
b = 2"""

    def test_list_spaced(self, style, reverse):
        spec = 'd = #d:{style}={ [1, 2] }:hidden=True#'
        template = """a = #a:default={d[0]}#
b = #b:default={d[1]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == """a = 1
b = 2"""

    def test_tuple(self, style, reverse):
        spec = 'd = #d:{style}={(1, 2)}:hidden=True#'
        template = """a = #a:default={d[0]}#
b = #b:default={d[1]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == """a = 1
b = 2"""

    def test_tuple_spaced(self, style, reverse):
        spec = 'd = #d:{style}={ (1, 2) }:hidden=True#'
        template = """a = #a:default={d[0]}#
b = #b:default={d[1]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == """a = 1
b = 2"""

    def test_dict_arg(self, style, reverse):
        """
        Check that iterables behave when we address with an arg, rather than hardcoded
        """
        spec = '#d:{style}={{"a": 1, "b": 2}}:hidden=True#'
        template = """#a:default=a:hidden=True#
val = #b:default={d[a]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == "val = 1"
        assert script.script(a="b") == "val = 2"

    def test_list_arg(self, style, reverse):
        """
        Check that iterables behave when we address with an arg, rather than hardcoded
        """
        spec = '#d:{style}={[1, 2]}:hidden=True#'
        template = """#a:default=0:hidden=True#
val = #b:default={d[a]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == "val = 1"
        assert script.script(a=1) == "val = 2"

    def test_tuple_arg(self, style, reverse):
        """
        Check that iterables behave when we address with an arg, rather than hardcoded
        """
        spec = '#d:{style}={(1, 2)}:hidden=True#'
        template = """#a:default=0:hidden=True#
val = #b:default={d[a]}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        assert script.script() == "val = 1"
        assert script.script(a=1) == "val = 2"

    def test_multi_link(self, style, reverse):
        """
        Checks for a bug where calling _link_subs repeatedly would raise an exception
        """
        spec = 'd = #d:{style}={{"a": 1, "b": 2}}:hidden=True#'.replace("{style}", style)
        template = """a = #a:default={d['a']}#
b = #b:default={d['b']}#"""

        if reverse:
            combo = f"{template}\n{spec.replace('{style}', style)}"
        else:
            combo = f"{spec.replace('{style}', style)}\n{template}"
        script = Script(template=combo)

        script._link_subs()
        script._link_subs()
        script._link_subs()


@pytest.mark.parametrize(
    "a,b,expected",
    [
        ("a", "a", "out = 00"),
        ("a", "b", "out = 01"),
        ("b", "a", "out = 10"),
        ("b", "b", "out = 11"),
    ]
)
def test_dict_nesting(a, b, expected):
    template = """#src:default={ {'a': {'a': '00', 'b': '01'}, 'b': {'a': '10', 'b': '11'}} }:hidden=True#
a = #a:hidden=True#
b = #b:hidden=True#
out = #out:default={src[a][b]}#"""

    script = Script(template=template)

    assert script.script(a=a, b=b) == expected


@pytest.mark.parametrize("a", [0, 1])
@pytest.mark.parametrize("b", [0, 1])
def test_list_nesting(a, b):
    template = """#src:default={ [ ['00', '01'], ['10', '11'] ] }:hidden=True#
a = #a:hidden=True#
b = #b:hidden=True#
out = #out:default={src[a][b]}#"""

    script = Script(template=template)

    assert script.script(a=a, b=b) == f"out = {a}{b}"


@pytest.mark.parametrize(
    "a,b,expected",
    [
        ("a", 0, "out = 00"),
        ("a", 1, "out = 01"),
        ("b", 0, "out = 10"),
        ("b", 1, "out = 11"),
    ]
)
def test_mixed_nesting(a, b, expected):
    template = """#src:default={ {'a': ['00', '01'], 'b': ['10', '11']} }:hidden=True#
a = #a:hidden=True#
b = #b:hidden=True#
out = #out:default={src[a][b]}#"""

    script = Script(template=template)

    assert script.script(a=a, b=b) == expected
