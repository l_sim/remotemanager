import unittest

import pytest

from remotemanager.script.script import Script


class TestTemplates(unittest.TestCase):

    def test_basic(self):
        template = "foo = #foo#"

        script = Script(template=template)

        assert script.subs == ["foo"]

        assert hasattr(script, "foo")

    def test_double(self):
        template = """foo = #foo#
foo = #foo#
"""

        script = Script(template=template)

        assert script.subs == ["foo"]

        assert hasattr(script, "foo")

    def test_extra_arg_error(self):
        template = """foo = #foo:default=bar#
foo = #foo:hidden=True#
"""
        with pytest.raises(ValueError):
            Script(template=template)
