import unittest

from remotemanager.script import Script


class TestHiddenValues(unittest.TestCase):
    def test_basic(self):
        template = "a = #a#\nb = #b:hidden=True#"

        script = Script(template=template)
        script.a = 10
        script.b = 10
        assert script.script() == "a = 10"

    def test_hidden_dependent(self):
        template = "a = #a:default={b}#\nb = #b:hidden=True#"

        script = Script(template=template)
        script.b = 10
        assert script.script() == "a = 10"
