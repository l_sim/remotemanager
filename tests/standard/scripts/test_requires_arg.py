import unittest

import pytest

from remotemanager import Computer


class TestTemplate(unittest.TestCase):

    def generate_computer(self, template_lines: list, **kwargs) -> Computer:
        """
        Generates a computer based on a list of lines,
        which will be taken as a template
        """
        template = "\n".join(template_lines)

        return Computer(template, **kwargs)

    def test_requires(self):
        template = ["a=#a:requires=b:optional=False#", "#b#"]

        computer = self.generate_computer(template_lines=template)

        assert "a" in computer.required
        assert "b" in computer.required

        with pytest.raises(ValueError):
            computer.script()

        computer.a = "test"
        # should still raise, since we require b
        with pytest.raises(ValueError):
            computer.script()

        computer.b = "test"
        assert "a=test" in computer.script()

    def test_requires_multi(self):
        template = ["foo=#foo:requires=a,b,c:optional=False#", "a=#a#", "b=#b#", "c=#c#"]

        computer = self.generate_computer(template_lines=template)

        assert "a" in computer.required
        assert "b" in computer.required
        assert "c" in computer.required

    def test_default_override(self):
        template = ["a=#a:default={b}:requires=b:optional=False#", "#b#"]

        computer = self.generate_computer(template_lines=template)

        assert "b" in computer.required
