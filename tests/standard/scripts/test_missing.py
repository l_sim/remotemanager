import unittest

import pytest

from remotemanager.script import Script


class TestMissing(unittest.TestCase):
    def basic(self):
        template = "foo = #foo:optional=False#"

        script = Script(template=template)

        assert script.required == ["foo"]
        assert script.missing == ["foo"]
        assert not script.valid

    def test_set(self):
        template = "foo = #foo:optional=False#"

        script = Script(template=template)

        script.foo = 10

        assert script.required == ["foo"]
        assert script.missing == []
        assert script.valid

    def test_temp_args(self):
        template = "foo = #foo:optional=False#"

        script = Script(template=template)

        assert script.script(foo=10) == "foo = 10"

    def test_invalid(self):
        template = "foo = #foo:optional=False#"

        script = Script(template=template)

        with pytest.raises(ValueError, match="foo"):
            script.script()
