from remotemanager.script.script import Script


class TestScriptStorage:
    def test_basic(self):
        template = "foo"

        script = Script(template=template)

        assert script.script() == script.unpack(script.pack()).script()

    def test_default(self):
        template = "foo = #foo:default=10#"

        script = Script(template=template)

        assert script.script() == script.unpack(script.pack()).script()
        assert script.script() == "foo = 10"

    def test_val(self):
        template = "foo = #foo:default=10#"

        script = Script(template=template)

        script.foo = 20

        assert script.script() == script.unpack(script.pack()).script()
        assert script.script() == "foo = 20"

    def test_multiple_copies(self):
        template = """a = #a#
a = #a#
a = #a#
"""

        script = Script(template=template)

        assert script.script() == script.unpack(script.pack()).script()
        assert script.script() == ""

    def test_multiple_defaults(self):
        template = """a = #a:default=10#
a = #a#
a = #a#
"""

        script = Script(template=template)

        pack_content = script.pack()

        assert pack_content.count("default") == 1
        assert script.script() == script.unpack(pack_content).script()
        assert script.script() == """a = 10
a = 10
a = 10
"""

    def test_multiple_values(self):
        template = """a = #a#
a = #a#
a = #a#
"""

        script = Script(template=template)

        script.a = 30

        pack_content = script.pack()

        assert pack_content.count("value") == 1
        assert script.script() == script.unpack(pack_content).script()
        assert script.script() == """a = 30
a = 30
a = 30
"""

    def test_no_value_collect(self):
        template = """a = #a:default=10#
a = #a#
a = #a#
"""

        script = Script(template=template)

        script.a = 30
        pack_content = script.pack(values=False)

        assert script.unpack(pack_content).script() == """a = 10
a = 10
a = 10
"""
        assert script.script() == """a = 30
a = 30
a = 30
"""

    def test_escape_storage_raw(self):
        template = r"""a = #a:default="a:a"#
b = #b:default=a\:a#
"""
        script = Script(template=template)

        assert script.script() == script.unpack(script.pack()).script()

    def test_escape_storage_double(self):
        template = "a = #a:default=a\\:a#"
        script = Script(template=template)

        assert script.script() == script.unpack(script.pack()).script()

    def test_escape_storage_raw_eval(self):
        template = r"""a = #a:default="a:a"#
b = #b:default={a}\:{a}#
"""
        script = Script(template=template)

        a = script.script(a=10)
        b = script.unpack(script.pack()).script(a=10)

        assert a == b
        assert "10:10" in a
        assert "10:10" in b

    def test_escape_storage_double_eval(self):
        template = """a = #a:default="a:a"#
b = #b:default={a}\\:{a}#
"""
        script = Script(template=template)

        a = script.script(a=10)
        b = script.unpack(script.pack()).script(a=10)

        assert a == b
        assert "10:10" in a
        assert "10:10" in b

    def test_store_dict(self):
        template = """d = #d:default={{"a": 1, "b": 2}}#

a = #a:default={d['a']}#
b = #b:default={d['b']}#
"""

        script = Script(template=template)

        a = script.script()
        b = script.unpack(script.pack()).script()

        assert a == b == """d = {'a': 1, 'b': 2}

a = 1
b = 2
"""

    def test_store_dict_spaced(self):
        template = """d = #d:default={ {"a": 1, "b": 2} }#

a = #a:default={d['a']}#
b = #b:default={d['b']}#
"""

        script = Script(template=template)

        a = script.script()
        b = script.unpack(script.pack()).script()

        assert a == b == """d = {'a': 1, 'b': 2}

a = 1
b = 2
"""

    def test_store_list(self):
        template = """
d = #d:default={[1, 2]}#

a = #a:default={d[0]}#
b = #b:default={d[1]}#
"""

        script = Script(template=template)

        a = script.script()
        b = script.unpack(script.pack()).script()

        assert a == b == """
d = [1, 2]

a = 1
b = 2
"""

    def test_store_list_spaced(self):
        template = """
d = #d:default={ [1, 2] }#

a = #a:default={d[0]}#
b = #b:default={d[1]}#
"""

        script = Script(template=template)

        a = script.script()
        b = script.unpack(script.pack()).script()

        assert a == b == """
d = [1, 2]

a = 1
b = 2
"""
