import os.path

from remotemanager import get_package_root
from remotemanager.script import Script

path = os.path.join(get_package_root(), "tests", "standard", "scripts", "template", "template.txt")

with open(path, mode="r", encoding="utf8") as o:
    content = o.read()


def test_extract():
    file = Script(template=path)
    direct = Script(template=content)

    file.ntasks = 64
    direct.ntasks = 64

    file.cpuspertask = 4
    direct.cpuspertask = 4

    fscript = file.script()
    dscript = direct.script()

    assert fscript == dscript
    assert "nodes=4" in fscript


def test_kwargs():
    file = Script(template=path)
    direct = Script(template=content)

    fscript = file.script(ntasks=64, cpuspertask=4)
    dscript = direct.script(ntasks=64, cpuspertask=4)

    assert fscript == dscript
    assert "nodes=4" in fscript
