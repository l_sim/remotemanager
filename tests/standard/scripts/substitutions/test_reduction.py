from typing import Tuple

import pytest

from remotemanager.connection.computers.substitution import Substitution

ops = ["+", "-", "/", "*", "**", "==", "!=", ">", ">=", "<", "<="]


class TestSubstitutionInternals:

    def setUp(self) -> Tuple[Substitution, Substitution, Substitution]:
        a = Substitution(target="a", name="a")
        b = Substitution(target="b", name="b")
        c = Substitution(target="c", name="c")

        return a, b, c

    @pytest.mark.parametrize("operation", ops)
    def test_single_op(self, operation):
        a, b, _ = self.setUp()
        c = eval(f"a {operation} b")
        assert c.reduced == f"(a {operation} b)"

    @pytest.mark.parametrize("op_a", ops)
    @pytest.mark.parametrize("op_b", ops)
    def test_double_op(self, op_a, op_b):
        """
        Test a double set of operations, explicitly bracketed

        Allowing DynamicValue to choose the bracketing for itself is hard to test
        in this format, though we may return to it at a later date
        """
        a, b, c = self.setUp()
        d = eval(f"(a {op_a} b) {op_b} c")
        assert d.reduced == f"((a {op_a} b) {op_b} c)"

    def test_complex(self):
        a, b, _ = self.setUp()
        c = (a + b) / (a - b)

        assert c.reduced == "((a + b) / (a - b))"
