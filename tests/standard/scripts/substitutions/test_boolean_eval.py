from remotemanager import Script

template = """value = #VALUE#
setpoint = #SETPOINT#

eq = #TEST_EQ:default={value == setpoint}#
ne = #TEST_NE:default={value != setpoint}#
ge = #TEST_GE:default={value >= setpoint}#
gt = #TEST_GT:default={value > setpoint}#
le = #TEST_LE:default={value <= setpoint}#
lt = #TEST_LT:default={value < setpoint}#
"""


def test_eq():
    script = Script(template=template)

    script.value = 10
    script.setpoint = 10

    script._link_subs()

    assert isinstance(script.test_eq.value, bool)

    assert script.test_eq.value
    assert not script.test_ne.value
    assert script.test_ge.value
    assert not script.test_gt.value
    assert script.test_le.value
    assert not script.test_lt.value


def test_lt():
    script = Script(template=template)

    script.value = 5
    script.setpoint = 10

    script._link_subs()

    assert not script.test_eq.value
    assert script.test_ne.value
    assert not script.test_ge.value
    assert not script.test_gt.value
    assert script.test_le.value
    assert script.test_lt.value


def test_gt():
    script = Script(template=template)

    script.value = 15
    script.setpoint = 10

    script._link_subs()

    assert not script.test_eq.value
    assert script.test_ne.value
    assert script.test_ge.value
    assert script.test_gt.value
    assert not script.test_le.value
    assert not script.test_lt.value


def test_str():
    script = Script(template=template)

    script.value = "foo"
    script.setpoint = 10

    script._link_subs()

    assert not script.test_eq.value
    assert script.test_ne.value


def test_none():
    script = Script(template=template)

    script.value = None
    script.setpoint = 10

    script._link_subs()

    assert not script.test_eq.value
    assert script.test_ne.value
