import pytest

from remotemanager.script import Script


@pytest.mark.parametrize("input", ["foo", 0, False])
def test_vals(input):
    script = Script("value = #value#")
    script.value = input

    assert script.script() == f"value = {input}"
