"""
get_target_kwargs is an internal function of the substitution
objects that's used to extract kwargs from a string input
"""
import pytest

from remotemanager.connection.computers.substitution import Substitution


@pytest.mark.parametrize(
    "i,o",
    [
        ("", {}),  # case with no arguments
        ("default=10", {"default": '10'}),
        ("default='10'", {"default": "'10'"}),
        ('default="10"', {"default": '"10"'}),
        ('default={nodes*cores/cores_per_node}', {"default": '{nodes*cores/cores_per_node}'}),
        ('format=time:default=3600', {'format': 'time', 'default': '3600'}),
        ('replaces=a,b,c', {'replaces': 'a,b,c'}),
        ('replaces=a, b, c', {'replaces': 'a, b, c'}),
        ('replaces=a b c', {'replaces': 'a b c'}),
        ('format=time:default=3600:replaces=a,b,c', {'format': 'time', 'default': '3600', 'replaces': 'a,b,c'}),
        ('default={ntasks}:requires=ntasks', {"default": '{ntasks}', 'requires': 'ntasks'}),
        ('ratio="a:b"', {'ratio': '"a:b"'}),  # quote :
        (r'ratio=a\:b', {'ratio': 'a\\:b'}),  # escape :
        ('ratio=a\\:b', {'ratio': 'a\\:b'}),
        (r'ratio=a\=b', {'ratio': 'a\\=b'}),  # escape =
        ('ratio=a\\=b', {'ratio': 'a\\=b'}),
        (r'escape=\\hello', {"escape": '\\\\hello'}),  # raw with \
    ]
)
def test_extraction(i: str, o: dict):
    test = Substitution("test", "test")

    assert test.get_target_kwargs(f"test:{i}") == o


def test_raise():
    test = Substitution("test", "test")

    with pytest.raises(ValueError):
        assert test.get_target_kwargs("test:arg:withcolon=foo") == ""
