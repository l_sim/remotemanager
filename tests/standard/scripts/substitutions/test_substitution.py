"""
Unit tests for Substitution objects
"""

import unittest

from remotemanager import Computer
from remotemanager.connection.computers.substitution import Substitution


def test_string_create():
    """Check that we can create a valid sub from string"""

    string = "#mpi#"
    sub = Substitution.from_string(string)

    assert sub.target == string
    assert sub.name == "mpi"


def test_string_create_with_default():
    """Test that string creation properly respects kwargs"""

    string = "#mpi:default=128#"
    sub = Substitution.from_string(string)

    assert sub.target == string
    assert sub.name == "mpi"
    assert sub.default == 128
    assert sub.value == 128


def test_string_create_with_format():
    """Test that string creation properly respects multiple kwargs"""

    string = "#walltime:default=3600:format=time#"
    sub = Substitution.from_string(string)

    assert sub.target == string
    assert sub.name == "walltime"
    assert sub.default == "01:00:00"
    assert sub.value == "01:00:00"

    sub.value = 1800

    assert sub.value == "00:30:00"


def test_string_create_override_args():
    """Make sure we can override kwargs"""

    string = "#mpi:default=128#"
    sub = Substitution.from_string(string, default=256)

    assert sub.target == string
    assert sub.name == "mpi"
    assert sub.default == 256
    assert sub.value == 256


def test_string_create_with_multiple_args():
    """Check that we can create a valid sub from string"""

    string = "#time:default=3600:format=time#"
    sub = Substitution.from_string(string)

    assert sub.target == string
    assert sub.name == "time"
    assert sub.default == "01:00:00"


def test_string_create_with_equivalence():
    """Check that we can create a valid sub from string"""

    string = "#isfoo:default={value=='foo'}#"
    sub = Substitution.from_string(string)

    assert sub.target == string
    assert sub.name == "isfoo"
    assert sub.default == "{value=='foo'}"


def test_non_updatable():
    """Some kwargs are invalid, make sure they're properly handled"""

    string = "#mpi:default=128#"
    sub = Substitution.from_string(string, name="foo", target="bar", warn_invalid=False)

    assert sub.target == string
    assert sub.name == "mpi"
    assert sub.default == 128
    assert sub.value == 128


def test_target_kwargs():
    """Check that the target kwargs are valid"""

    string = "#walltime:default=3600:format=time#"
    sub = Substitution.from_string(string)

    assert sub.target_kwargs["default"] == "3600"
    assert sub.target_kwargs["format"] == "time"


def test_assert_no_dict():
    string = "#foo#"
    sub = Substitution.from_string(string)

    assert not hasattr(sub, "__dict__")


class TestTemplate(unittest.TestCase):

    def generate_computer(self, template_lines: list, **kwargs) -> Computer:
        """
        Generates a computer based on a list of lines,
        which will be taken as a template
        """
        template = "\n".join(template_lines)

        return Computer(template, **kwargs)

    def test_copy_evaluation(self):
        """Tests the most basic linking, a copy"""
        template = ["#VALUE#", "#COPY#"]

        computer = self.generate_computer(template)

        computer.copy = computer.value

        assert computer.script() == ""
        computer.value = "foo"
        assert computer.script() == "foo\nfoo"

    def test_default_copy_evaluation(self):
        """Tests the most basic linking, a copy, but using a default"""
        template = ["#VALUE#", "#COPY:default={value}#"]

        computer = self.generate_computer(template)

        assert computer.script() == ""
        computer.value = "foo"
        assert computer.script() == "foo\nfoo"

    def test_default_copy_evaluation_with_default_source(self):
        """Tests the most basic linking, a copy, but using defaults on both ends"""
        template = ["#VALUE:default=foo#", "#COPY:default={value}#"]

        computer = self.generate_computer(template)

        assert computer.script() == "foo\nfoo"

    def test_math_evaluation(self):
        """Tests the most basic linking, a copy"""
        template = ["#VALUE#", "#CALC#"]

        computer = self.generate_computer(template)

        computer.calc = computer.value * 10

        assert computer.script() == ""
        computer.value = 10
        assert computer.script() == "10\n100"

    def test_default_math_evaluation(self):
        """Tests the most basic linking, a copy"""
        template = ["#VALUE#", "#CALC:default={value*20}#"]

        computer = self.generate_computer(template)

        assert computer.script() == ""
        computer.value = 10
        assert computer.script() == "10\n200"

        computer.calc = computer.value * 10
        assert computer.script() == "10\n100"

    def test_default_math_evaluation_with_default_source(self):
        """Tests the most basic linking, a copy"""
        template = ["#VALUE:default=5#", "#CALC:default={value*20}#"]

        computer = self.generate_computer(template)

        assert computer.script() == "5\n100"
        computer.value = 10
        assert computer.script() == "10\n200"

    def test_bool_evaluation(self):
        """Tests the most basic linking, a copy"""
        template = ["#VALUE#", '#ISFOO:default={value == "foo"}#']

        computer = self.generate_computer(template)

        assert computer.script() == "False"
        computer.value = 10
        assert computer.script() == "10\nFalse"

    def test_temporary_evaluation(self):
        """Tests the most basic linking, a copy"""
        template = ["#A#", "#B#", "#CALC:default={a / b}#"]

        computer = self.generate_computer(template)

        assert computer.script(a=10, b=5) == "10\n5\n2"
