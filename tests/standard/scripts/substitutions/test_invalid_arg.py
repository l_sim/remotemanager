import pytest

from remotemanager.connection.computers.substitution import Substitution


def test_typo():
    string = "#a:defaultt=10#"

    with pytest.raises(TypeError, match=".*unexpected keyword argument 'defaultt'"):
        sub = Substitution.from_string(string)
