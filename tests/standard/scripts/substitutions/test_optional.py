import pytest

from remotemanager import Script


def generate_script(template_lines: list, **kwargs) -> Script:
    """
    Generates a computer based on a list of lines,
    which will be taken as a template
    """
    template = "\n".join(template_lines)

    return Script(template, **kwargs)


def test_optional_sub():
    script = Script(template="foo=#foo:optional=False#")

    with pytest.raises(ValueError, match="Missing values for parameters:.*"):
        script.script()


@pytest.mark.parametrize("optional", ["False", "false"])
class TestNonOptional:

    def test_direct(self, optional):
        template = [f"a=#a:optional={optional}#"]

        computer = generate_script(template_lines=template)

        assert "a" in computer.required

    def test_with_default(self, optional):
        template = [f"a=#a:default=foo:optional={optional}#"]

        computer = generate_script(template_lines=template)

        assert "a" not in computer.required


@pytest.mark.parametrize("optional", ["True", "true"])
class TestOptional:

    def test_direct(self, optional):
        template = [f"a=#a:optional={optional}#"]

        computer = generate_script(template_lines=template)

        assert "a" not in computer.required

    def test_with_default(self, optional):
        template = [f"a=#a:default=foo:optional={optional}#"]

        computer = generate_script(template_lines=template)

        assert "a" not in computer.required
