"""
Checks for a bug where attempting to concat ints with _ would give strange
(but explainable) behaviour.

For example, if we concat two ints {a}_{b} like such, it will actually generate
a new int `ab`
"""
from remotemanager import Script


def test_concat():
    template = """
a = #a:default=1#
b = #b:default=2#

c = #c:default={a}_{b}#
"""
    script = Script(template=template)

    assert "1_2" in script.script()
