import pytest

from remotemanager.script import Script


@pytest.mark.parametrize(
    "i,o",
    [
        (r"a=#a:default=a\:a#", "a=a:a"),
        ("a=#a:default=a\\:a#", "a=a:a"),
        ("a=#a:default='a:a'#", "a='a:a'"),
        ('a=#a:default="a:a"#', 'a="a:a"'),
        ("a=#a:default='a\:a'#", "a='a:a'"),
        ('a=#a:default="a\:a"#', 'a="a:a"'),
        (r"a=#a:default='a\:a'#", "a='a:a'"),
        (r'a=#a:default="a\:a"#', 'a="a:a"'),
    ]
)
def test_io(i, o):
    script = Script(template=i)

    assert script.script() == o


class TestEvalEscape:

    def generate_script(self, template_lines: list, **kwargs) -> Script:
        """
        Generates a Script based on a list of lines,
        which will be taken as a template
        """
        template = "\n".join(template_lines)

        return Script(template, **kwargs)

    def test_non_usage(self):
        """Test that a script is produced with escapes removed"""
        lines = [r"a = \{foo\}"]

        assert self.generate_script(lines).script() == "a = {foo}"

    def test_non_usage_within_val(self):
        """As before, but checking that it's done within a value"""
        lines = [r"a = #a:default=\{foo\}#"]

        assert self.generate_script(lines).script() == "a = {foo}"

    def test_non_usage_other_val_nonuse(self):
        """Ensure that having another, unrelated, value nearby doesn't break things"""
        lines = [r"a = #a:default=\{foo\}#", "b = #b:default=test#"]

        assert self.generate_script(lines).script() == "a = {foo}\nb = test"

    def test_non_usage_other_val(self):
        """Calculating values other than the escaped one should not break things"""
        lines = [r"a = #a:default=\{foo\}#", "b = #b:default=test#", "c = #c:default={b}#"]

        assert self.generate_script(lines).script() == "a = {foo}\nb = test\nc = test"

    def test_non_usage_other_val_nonuse_samename(self):
        """Does having a value of the same name that would otherwise be captured still work?"""
        lines = [r"a = #a:default=\{foo\}#", "foo = #foo:default=test#"]

        assert self.generate_script(lines).script() == "a = {foo}\nfoo = test"

    def test_non_usage_other_val_samename(self):
        """Evaluate that same name, see if it somehow gets picked up in the escape"""
        lines = [r"a = #a:default=\{foo\}#", "foo = #foo:default=test#", "c = #c:default={foo}#"]

        assert self.generate_script(lines).script() == "a = {foo}\nfoo = test\nc = test"

    def test_usage(self):
        lines = [r"a = #a:default=\{foo\}_{b}#", "b = #b:default=test#"]

        assert self.generate_script(lines).script() == "a = {foo}_test\nb = test"

    def test_usage_kwarg(self):
        lines = [r"a = #a:default=\{foo\}_{b}#", "b = #b:default=test#"]

        assert self.generate_script(lines).script(b="new") == "a = {foo}_new\nb = new"

    def test_usage_samename(self):
        lines = [r"a = #a:default=\{foo\}_{foo}#", "b = #foo:default=test#"]

        assert self.generate_script(lines).script() == "a = {foo}_test\nb = test"

    def test_usage_kwarg_samename(self):
        lines = [r"a = #a:default=\{foo\}_{foo}#", "b = #foo:default=test#"]

        assert self.generate_script(lines).script(foo="new") == "a = {foo}_new\nb = new"
