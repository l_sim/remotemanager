import os.path

from remotemanager import get_package_root
from remotemanager.JUBEInterop import JUBETemplate

srcdir = os.path.join(get_package_root(), "tests", "standard", "JUBEInterop", "src")

platform_path = os.path.join(srcdir, "platform.xml")
template_path = os.path.join(srcdir, "submit.job")


def test_kwarg_passthrough():
    t = JUBETemplate(platform=platform_path, template=template_path, host="remote.host")

    assert t.host == "remote.host"


def test_kwarg_passthrough_from_repo():
    t = JUBETemplate.from_repo(repo="https://gitlab.com/l_sim/remotemanager",
                               path="tests/standard/JUBEInterop/src",
                               branch="devel",
                               local_dir="test_jube_pull",
                               host="remote.host")

    assert t.host == "remote.host"
