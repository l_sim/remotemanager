import os.path
import shutil

from remotemanager import get_package_root
from remotemanager.JUBEInterop import JUBETemplate

srcdir = os.path.join(get_package_root(), "tests", "standard", "JUBEInterop", "src")

platform_path = os.path.join(srcdir, "platform.xml")
template_path = os.path.join(srcdir, "submit.job")


def test_extract_and_run():
    test = JUBETemplate(platform=platform_path, template=template_path)
    repo = JUBETemplate.from_repo(repo="https://gitlab.com/l_sim/remotemanager",
                                  path="tests/standard/JUBEInterop/src",
                                  branch="devel",
                                  local_dir="test_jube_pull")
    shutil.rmtree("test_jube_pull")

    assert test.template == repo.template

    assert test.script() == repo.script()
