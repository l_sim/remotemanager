import os.path

import pytest

from remotemanager import get_package_root
from remotemanager.JUBEInterop import JUBETemplate

srcdir = os.path.join(get_package_root(), "tests", "standard", "JUBEInterop", "src")

platform_path = os.path.join(srcdir, "platform.xml")
template_path = os.path.join(srcdir, "submit.job")


def test_extract_and_run():
    test = JUBETemplate(platform=platform_path, template=template_path)

    default = test.script()

    assert "partition=boost_usr_prod" in default


def test_evaluation():
    """increasing nodes should change qos"""
    test = JUBETemplate(platform=platform_path, template=template_path)

    increase = test.script(nodes=128)

    assert "qos=boost_qos_bprod" in increase
    assert "nodes=128" in increase


def test_invalid_path():
    with pytest.raises(ValueError):
        JUBETemplate(platform="foo", template=template_path)


def test_value_is_valid_python():
    test = JUBETemplate(platform=platform_path, template=template_path)

    default = test.script(nodes="4*4")

    assert "nodes=16" in default
