import os

import pytest

from remotemanager.storage.trackedfile import TrackedFile
from remotemanager.utils import random_string


@pytest.mark.parametrize("mode", [777, 755, 644])
def test_chmod(mode):
    filename = random_string()
    try:
        file = TrackedFile("..", ".", filename)
        file.write("foo")

        file.chmod(mode)

        assert oct(os.stat(file.local).st_mode)[-3:] == str(mode)
    finally:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass


@pytest.mark.parametrize("mode", ["777", "foo", None, 777.0])
def test_non_int(mode):
    filename = random_string()
    try:
        file = TrackedFile("..", ".", filename)
        file.write("foo")

        with pytest.raises(ValueError, match=".*must be int-type"):
            file.chmod(mode)
    finally:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass


@pytest.mark.parametrize("mode", [77777])
def test_non_int(mode):
    filename = random_string()
    try:
        file = TrackedFile("..", ".", filename)
        file.write("foo")

        with pytest.raises(ValueError, match=".*is too long"):
            file.chmod(mode)
    finally:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass


@pytest.mark.parametrize("mode", [777, 755, 644, "777", "foo", None, 77777, 777.0])
def test_nofile(mode):
    """
    chmod first checks the file presence and stops if not found

    This means that no checks are performed, so all should go ahead
    """
    filename = random_string()
    file = TrackedFile("..", ".", filename)
    file.chmod(mode)
