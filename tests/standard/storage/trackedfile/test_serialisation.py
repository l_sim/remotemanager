import os
import time

from remotemanager.storage.trackedfile import TrackedFile
from remotemanager.utils import random_string


def test_serialise_trackedfile():
    file = TrackedFile(".", ".", random_string())

    assert not hasattr(file, "__dict__")

    packfile = f"{random_string()}.yaml"

    try:
        file.pack(file=packfile)

        new = TrackedFile.unpack(file=packfile)

        assert not hasattr(new, "__dict__")
        assert new.last_seen_remote == -1

        new.confirm_remote()
        assert (int(time.time()) - new.last_seen_remote) < 0.5
    finally:
        os.remove(packfile)


def test_serialise_trackedfile_confirm_before():
    file = TrackedFile(".", ".", random_string())
    file.confirm_remote()
    t0 = int(time.time())

    assert not hasattr(file, "__dict__")

    packfile = f"{random_string()}.yaml"

    try:
        file.pack(file=packfile)

        new = TrackedFile.unpack(file=packfile)

        assert not hasattr(new, "__dict__")

        assert (t0 - new.last_seen_remote) < 0.5
    finally:
        os.remove(packfile)
