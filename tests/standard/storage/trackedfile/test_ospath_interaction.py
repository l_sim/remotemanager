import os.path

import pytest

from remotemanager.storage.trackedfile import TrackedFile


@pytest.mark.parametrize("local_path", [".", "foo", "foo/bar", "/home/user/foo"])
def test_ospath_split(local_path):
    test = TrackedFile(local_path, ".", "test")

    assert os.path.split(test) == (local_path, "test")
