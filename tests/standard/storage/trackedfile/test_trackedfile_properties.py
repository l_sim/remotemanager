import os.path
import time
import unittest

import pytest

from remotemanager.storage.trackedfile import TrackedFile
from remotemanager.utils import random_string


class TestTrackedFileProperties(unittest.TestCase):
    file = None

    def create_file(self, local_path: str = None, remote_path: str = None, name: str = None):
        if name is None:
            name = random_string()
        if local_path is None:
            local_path = "."
        if remote_path is None:
            remote_path = "."
        self.file = TrackedFile(local_path, remote_path, name)

    def tearDown(self):
        """clean up testing material"""
        if os.path.exists(self.file.local):
            os.remove(self.file.local)

    def test_repr(self):
        self.create_file(name="test_repr")
        assert repr(self.file) == "{test_repr}"

    def test_importstr(self):
        self.create_file(name="test_importstr.txt")
        assert self.file.importstr == "test_importstr"

    def test_importstr_no_ext(self):
        self.create_file(name="test_importstr_no_ext")
        assert self.file.importstr == "test_importstr_no_ext"

    def test_remote_path(self):
        self.create_file(remote_path="foo", name="test_remote_path")

        assert self.file.remote_dir == "foo"
        assert self.file.remote == os.path.join("foo", "test_remote_path")

    def test_remote_abspath(self):
        self.create_file(remote_path="/home/foo", name="test_remote_abspath")

        assert self.file.remote_dir == "/home/foo"
        assert self.file.remote == os.path.join("/home/foo", "test_remote_abspath")

    def test_local_path(self):
        self.create_file(local_path="foo", name="test_local_path")

        assert self.file.local_dir == "foo"
        assert self.file.local == os.path.join("foo", "test_local_path")

    def test_local_abspath(self):
        self.create_file(local_path="/home/foo", name="test_local_abspath")

        assert self.file.local_dir == "/home/foo"
        assert self.file.local == os.path.join("/home/foo", "test_local_abspath")

    def test_confirm_remote(self):
        self.create_file()

        self.file.confirm_remote()
        t0 = int(time.time())

        assert t0 - self.file.last_seen_remote < 0.5  # slight 0.5s dither
        assert self.file.last_seen("remote") == self.file.last_seen_remote

    def test_confirm_remote_set(self):
        self.create_file()

        self.file.confirm_remote(t=0)

        assert self.file.last_seen_remote == 0
        assert self.file.last_seen("remote") == self.file.last_seen_remote

    @pytest.mark.skip(reason="to be reenabled after timestamps are unified")
    def test_confirm_local(self):
        self.create_file()

        self.file.confirm_local()
        t0 = int(time.time())

        assert t0 - self.file.last_seen_local < 0.5  # slight 0.5s dither
        assert self.file.last_seen("local") == self.file.last_seen_local

    def test_confirm_local_set(self):
        self.create_file()

        self.file.confirm_local(t=0)

        assert self.file.last_seen_local == 0
        assert self.file.last_seen("local") == self.file.last_seen_local

    def test_local_mtime_nofile(self):
        self.create_file()

        assert self.file.local_mtime_utc == -1

    def test_size_unset(self):
        self.create_file()

        assert self.file.size == -1

    def test_size_set(self):
        self.create_file()
        self.file.size = 100
        assert self.file.size == 100
