import os.path
import unittest

import pytest

from remotemanager.storage.trackedfile import TrackedFile
from remotemanager.utils import random_string


class TestTrackedFileSubstitutions(unittest.TestCase):
    def setUp(self):
        self.file = TrackedFile("..", ".", random_string())
        self.initial_content = "foo\n#to_sub#\nbar\n"

    def tearDown(self):
        """clean up testing material"""
        if os.path.exists(self.file.local):
            os.remove(self.file.local)

    def test_default(self):
        """Test default sub"""
        self.file.write(self.initial_content)

        line = "subbed with default"

        self.file.sub("#to_sub#", line)

        assert line in self.file.content

    def test_python(self):
        """Test python sub"""
        self.file.write(self.initial_content)

        line = "subbed with python"

        self.file.sub("#to_sub#", line, mode="python")

        assert line in self.file.content

    def test_regex(self):
        """Test regex sub"""
        self.file.write(self.initial_content)

        line = "subbed with regex"

        self.file.sub("#.*#", line, mode="regex")

        assert line in self.file.content

    def test_invalid(self):
        """Test invalid sub"""
        self.file.write(self.initial_content)

        line = "subbed with invalid"

        with pytest.raises(ValueError):
            self.file.sub("#to_sub#", line, mode="invalid")

    def test_no_content(self):
        """Test sub on an empty file"""
        if os.path.exists(self.file.local):
            os.remove(self.file.local)
        self.file.sub("#to_sub#", "empty")

        assert self.file.content is None
