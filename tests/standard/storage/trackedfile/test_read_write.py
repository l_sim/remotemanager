import os
import unittest

import pytest

from remotemanager.storage.trackedfile import TrackedFile
from remotemanager.utils import random_string


class TestWrite(unittest.TestCase):
    files = []

    def setUp(self, local_dir: str = "."):
        self.files.append(TrackedFile(local_dir, ".", random_string()))

    def tearDown(self):
        for file in self.files:
            try:
                os.remove(file.local)
            except FileNotFoundError:
                pass

    @property
    def file(self) -> TrackedFile:
        if len(self.files) == 0:
            raise ValueError("Add files before attempting to retrieve them")
        return self.files[-1]

    def test_nonexistent(self):
        self.setUp()
        assert not self.file.exists_local
        assert not os.path.isfile(self.file.local)

    def test_read_nonexistent(self):
        self.setUp()
        assert self.file.content is None

    def test_existence(self):
        self.setUp()
        self.file.write("foo")
        assert self.file.exists_local
        assert os.path.isfile(self.file.local)


@pytest.mark.parametrize(
    "content,expected",
    [
        ("foo", "foo"),
        (15, "15"),
        (["a", "b", "c"], "a\nb\nc")
    ]
)
@pytest.mark.parametrize(
    "add_newline",
    [True, False]
)
def test_writes(content, expected, add_newline):
    file = TrackedFile(".", ".", random_string())
    try:
        file.write(content, add_newline=add_newline)

        if add_newline:
            expected += "\n"

        assert file.content == expected
    finally:
        try:
            os.remove(file.local)
        except FileNotFoundError:
            pass


@pytest.mark.parametrize("a", ["foo", 15, ["a", "b", "c"]])
@pytest.mark.parametrize("b", ["foo", 15, ["a", "b", "c"]])
@pytest.mark.parametrize("newline_a", [True, False])
@pytest.mark.parametrize("newline_b", [True, False])
def test_appends(a, b, newline_a, newline_b):
    file = TrackedFile(".", ".", random_string())
    try:
        file.write(a, add_newline=newline_a)
        file.append(b, add_newline=newline_b)

        na = "\n" if newline_a else ""
        nb = "\n" if newline_b else ""

        if isinstance(a, list):
            a = "\n".join(a)
        if isinstance(b, list):
            b = "\n".join(b)
        assert file.content == f"{a}{na}{b}{nb}"

    finally:
        try:
            os.remove(file.local)
        except FileNotFoundError:
            pass
