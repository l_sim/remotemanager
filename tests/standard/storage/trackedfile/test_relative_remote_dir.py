import pytest

from remotemanager.storage.trackedfile import TrackedFile


@pytest.mark.parametrize(
    "remote,location,expected",
    [
        ("a", "a", "test"),  # same dir
        ("a", "a/b", "../test"),  # lower
        ("a", "a/b/c", "../../test"),  # even lower
        ("a/b", "a", "b/test"),  # higher
        ("a/b/c", "a", "b/c/test"),  # even higher
        ("a/b", "a/z", "../b/test"),  # in and out "valley"
        ("a/b/c", "a/z/y", "../../b/c/test"),  # deeper "valley"
        ("/abs/path/to/file", "a", "/abs/path/to/file/test"),  # abspath file
        ("/abs/path/to/file", "a/b", "/abs/path/to/file/test"),  # again
    ]
)
def test_basic(remote, location, expected):
    file = TrackedFile("..", remote, "test")

    assert file.relative_remote_path(location) == expected


def test_from_abs():
    file = TrackedFile("..", "foo", "test")

    with pytest.raises(ValueError):
        file.relative_remote_path("/abs/path/cwd")
