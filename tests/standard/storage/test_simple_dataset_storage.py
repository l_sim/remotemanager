from remotemanager.utils import random_string
from tests.utils.base_class import BaseTestClass


def fn(a, b):
    # test_simple_dataset_storage
    return a + b


class TestDatasetStorage(BaseTestClass):

    def test_basic(self):
        self.create_dataset(fn)

        self.ds.append_run({"a": 27, "b": 15})
        self.run_ds()

        packfile = f"{random_string()}.yaml"
        self.files.append(packfile)
        self.ds.pack(file=packfile)

        newds = self.ds.unpack(file=packfile)

        assert self.ds == newds

    def test_repeat(self):
        self.create_dataset(fn)

        self.ds.append_run({"a": 27, "b": 15})
        self.run_ds()

        packfile = f"{random_string()}.yaml"
        self.files.append(packfile)
        self.ds.pack(file=packfile)

        newds = self.ds.unpack(file=packfile)

        newds.pack(file=packfile)

        newds2 = self.ds.unpack(file=packfile)

        assert self.ds == newds
        assert newds == newds2
