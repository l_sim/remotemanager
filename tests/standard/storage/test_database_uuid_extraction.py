from remotemanager.storage.database import Database
from tests.utils.base_class import BaseTestClass


def f(x):
    # test_database_uuid_extraction
    return x


class TestDatabaseUUID(BaseTestClass):

    def test_uuid_eq(self):
        self.create_dataset(f)

        assert self.ds.database.stored_uuid == self.ds.uuid

    def test_empty(self):
        db = Database(file="test.yaml")
        self.files.append("test.yaml")

        assert db.stored_uuid is None

    def test_no_uuid(self):
        db = Database(file="test.yaml")
        self.files.append("test.yaml")
        db._storage = {"foo": {"a": 1}}

        assert db.stored_uuid is None
