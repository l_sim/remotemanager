import os

import pytest

from remotemanager.storage.sendablemixin import SendableMixin
from remotemanager.utils import random_string


class DummyObject(SendableMixin):
    def __init__(self, attr):
        self.attr = attr

    def __eq__(self, other) -> bool:
        return other.attr == self.attr


@pytest.mark.parametrize(
    "attr",
    ["foo", 1, 3.14, True, None]
)
@pytest.mark.parametrize(
    "recurse",
    [True, False]
)
def test_serial_loop(attr, recurse):
    if recurse:
        attr = DummyObject(attr)

    obj = DummyObject(attr)

    file = f"{random_string()}.yaml"
    try:
        obj.pack(file=file)

        new = DummyObject.unpack(file=file)

        assert new.attr == attr
    finally:
        os.remove(file)
