from remotemanager.dataset.dataset import Dataset
from tests.utils.base_class import BaseTestClass


def f(inp):
    # test_unpack_legacy_unloaded
    return inp


class TestUnloadedInjection(BaseTestClass):

    def test_unloaded_injection(self):
        """
        Legacy databases may exist that still contain Unloaded, what happens with these?

        Need to fake one by injecting Unloaded into them
        """

        self.create_dataset(f)
        self.ds.append_run({"inp": "test"})

        data = self.ds.pack()

        # simplest way is to take an existing object and "convert" it (as far as SendableMixin cares)
        data[self.ds.uuid]["_verbose"]['~serialisedclass~'] = {
            'mod': 'remotemanager.storage.sendablemixin',
            'name': 'Unloaded'
        }

        new = Dataset.unpack(data=data)

        new.run()
        new.wait(0.1, 2)
        new.fetch_results()

        assert new.results == ["test"]
