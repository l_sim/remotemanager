from remotemanager.utils.version import Version


def test_basic():
    ver = Version("0.1.2")
    assert ver.match("0.1.2")
    assert not ver.match("1.2.3")


def test_wildcard_patch():
    ver = Version("0.1.2")
    assert ver.match("0.1.x")
    assert not ver.match("0.2.x")


def test_wildcard_minor():
    ver = Version("0.1.2")
    assert ver.match("0.x.2")
    assert not ver.match("0.x.3")


def test_wildcard_major():
    ver = Version("0.1.2")
    assert ver.match("x.1.2")
    assert not ver.match("x.1.3")


def test_partial():
    ver = Version("0.1.2")
    assert ver.match("0.1")
    assert not ver.match("0.2")
