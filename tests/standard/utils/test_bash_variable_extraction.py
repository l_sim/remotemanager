from remotemanager.utils import extract_bash_variables


def test_basic():
    assert extract_bash_variables("$FOO") == ["$FOO"]


def test_home():
    assert extract_bash_variables("~") == ["~"]


def test_home_dirs():
    assert extract_bash_variables("~/$FOO") == ["~", "$FOO"]


def test_home_dirs_extra():
    assert extract_bash_variables("~/$FOO/test") == ["~", "$FOO"]


def test_dirlike():
    assert extract_bash_variables("$FOO/$BAR") == ["$FOO", "$BAR"]


def test_dirlike_extra():
    assert extract_bash_variables("$FOO/$BAR/test") == ["$FOO", "$BAR"]


def test_stringlike():
    assert extract_bash_variables("${FOO}${BAR}") == ['${FOO}', '${BAR}']


def test_stringlike_extra():
    assert extract_bash_variables("${FOO}${BAR}/test") == ['${FOO}', '${BAR}']
