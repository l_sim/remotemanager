from remotemanager.utils import recursive_dict_update


def test_none():
    d2 = {1: {11: "bar"}}
    assert recursive_dict_update(None, d2) == d2


def test_no_change():
    d2 = {1: {11: "bar"}}
    assert recursive_dict_update(d2, d2) == d2


def test_non_clobber():
    d1 = {
        1: {11: "bar"},
        2: {21: "bar"}
    }
    d2 = {
        1: {11: "bar"}
    }
    assert recursive_dict_update(d1, d2) == d1


def test_update():
    d1 = {
        1: {11: "bar"},
        2: {21: "bar"}
    }
    d2 = {
        1: {11: "bar"},
        2: {21: "foo"}
    }
    assert recursive_dict_update(d1, d2) == d2


def test_add():
    d1 = {
        2: {21: "bar"}
    }
    d2 = {
        1: {11: "bar"},
    }
    assert recursive_dict_update(d1, d2) == {
        1: {11: "bar"},
        2: {21: "bar"}
    }
