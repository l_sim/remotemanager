from remotemanager.utils import safe_divide


def test_control():
    assert safe_divide(10, 5) == 2


def test_zero():
    assert safe_divide(10, 0) == 1


def test_oneo():
    assert safe_divide(1, 1) == 1
