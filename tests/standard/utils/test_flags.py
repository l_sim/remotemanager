from remotemanager.utils.flags import Flags, strip_non_alphanumeric


def test_non_alphanumeric():
    assert strip_non_alphanumeric("abc123-+!/") == "abc123"


def test_empty():
    assert str(Flags()) == ""


def test_basic():
    flags = Flags("auv")
    assert str(flags) == "-auv"


def test_init_with_tag():
    flags = Flags("-auv")
    assert str(flags) == "-auv"


def test_add():
    flags = Flags("auv")
    flags += "xf"
    assert str(flags) == "-auvxf"


def test_add_many():
    flags = Flags("v")

    assert str(flags) == "-v"

    flags += "v"
    assert str(flags) == "-vv"

    flags += "v"
    assert str(flags) == "-vvv"


def test_sub():
    """Test subtracting flags"""
    flags = Flags("auv")
    flags -= "v"
    assert str(flags) == "-au"


def test_sub_first():
    """Test subtracting flags"""
    flags = Flags("auv")
    flags -= "a"
    assert str(flags) == "-uv"


def test_sub_many():
    """Test subtracting flags"""
    flags = Flags("auv")
    flags -= "au"
    assert str(flags) == "-v"


def test_sub_many_mixed():
    """Test subtracting flags"""
    flags = Flags("auv")
    flags -= "auxf"
    assert str(flags) == "-v"


def test_sub_missing():
    """Checks that subtracting flags that don't exist does not raise an exception"""
    flags = Flags("auv")
    flags -= "xz"
    assert str(flags) == "-auv"


def test_verbose_style():
    """Checks that we can init with a long flag"""
    flags = Flags("--verbose")
    assert str(flags) == "--verbose"


def test_verbose_add():
    """Checks that we can add with a long flag"""
    flags = Flags("--verbose")
    flags += "--add"
    assert str(flags) == "--verbose --add"


def test_verbose_sub():
    """Checks that we can subtract with a long flag"""
    flags = Flags("--verbose --sub")
    flags -= "--sub"
    assert str(flags) == "--verbose"


def test_verbose_sub_missing():
    """Checks that subtracting flags that don't exist does not raise an exception"""
    flags = Flags("--verbose")
    flags -= "--sub"
    assert str(flags) == "--verbose"


def test_double_separate():
    """Checks that we can mix tags"""
    flags = Flags("-abc", "--verbose")
    assert str(flags) == "-abc --verbose"


def test_double_combo():
    """Checks that we can mix tags"""
    flags = Flags("-abc --verbose")
    assert str(flags) == "-abc --verbose"


def test_double_separate_no_first():
    """Checks that we can mix tags"""
    flags = Flags("abc", "--verbose")
    assert str(flags) == "-abc --verbose"


def test_double_combo_no_first():
    """Checks that we can mix tags"""
    flags = Flags("abc --verbose")
    assert str(flags) == "-abc --verbose"


def test_add_basic_to_mix():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose")
    flags += "xf"
    assert str(flags) == "-abcxf --verbose"


def test_add_verbose_to_mix():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose")
    flags += "--add"
    assert str(flags) == "-abc --verbose --add"


def test_sub_basic_from_mix():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose")
    flags -= "bc"
    assert str(flags) == "-a --verbose"


def test_sub_basic_from_mix_first():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose")
    flags -= "a"
    assert str(flags) == "-bc --verbose"


def test_sub_verbose_from_mix():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose --add")
    flags -= "--add"
    assert str(flags) == "-abc --verbose"


def test_sub_verbose_from_mix_first():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose --add")
    flags -= "--verbose"
    assert str(flags) == "-abc --add"


def test_sub_basic_from_mix_missing():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose")
    flags -= "xf"
    assert str(flags) == "-abc --verbose"


def test_sub_verbose_from_mix_missing():
    """Checks that we can add to mixed tags"""
    flags = Flags("-abc", "--verbose")
    flags -= "--foo"
    assert str(flags) == "-abc --verbose"
