from remotemanager.utils import integer_time_wait

import numpy as np
import time
import math
import random


def test_deviations():
    n = 5
    times = []
    for i in range(n):
        time.sleep(random.random())

        times.append(time.time())

    std_nowait = np.std([math.fmod(t, 1) for t in times])

    times = []
    for i in range(n):
        time.sleep(random.random())
        integer_time_wait()

        times.append(time.time())

    std_wait = np.std([math.fmod(t, 1) for t in times])

    assert std_nowait > std_wait
