from remotemanager.utils.uuid import generate_uuid


def test_string_convert():
    assert generate_uuid("1") == generate_uuid(1)


def test_difference():
    assert generate_uuid("foo") != generate_uuid("bar")
