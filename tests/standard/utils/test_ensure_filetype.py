import pytest

from remotemanager.utils import ensure_filetype


tests = [
    ["test.txt", "txt", "test.txt"],
    ["test.pdf", "txt", "test.txt"],
    ["test", "txt", "test.txt"],
    ["test.foo.txt", "txt", "test.foo.txt"],
    ["test.foo.mp3", "txt", "test.foo.txt"],
    ["test.txt", None, "test"],
    ["test", None, "test"],
]


@pytest.mark.parametrize("params", tests)
def test_suite(params):
    input, target, output = params
    assert ensure_filetype(input, target) == output
