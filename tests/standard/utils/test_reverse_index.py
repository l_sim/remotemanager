from remotemanager.utils import reverse_index


def test_far_left():
    """check that we can get an index in the "first" location"""
    lst = ["a", "b", "c"]

    assert reverse_index(lst, "a") == 0


def test_singular():
    """check that single element lists work"""
    lst = ["a"]

    assert reverse_index(lst, "a") == 0


def test_nonzero():
    """basic nonzero index test case"""
    lst = ["a", "b", "c", "d"]

    assert reverse_index(lst, "c") == 2


def test_repeating():
    """test that it is not confused by repeating items"""
    lst = ["a", "a", "c", "a", "a", "c", "a", "a"]

    assert reverse_index(lst, "c") == 5
