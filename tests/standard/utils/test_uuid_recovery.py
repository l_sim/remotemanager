from remotemanager.utils import object_from_uuid
from tests.utils.base_class import BaseTestClass


def func():
    return

class TestRecovery(BaseTestClass):
    def test_dataset_recovery(self):
        self.create_dataset(func)

        assert object_from_uuid(self.ds.uuid, "Dataset") == self.ds


    def test_not_found(self):
        with self.assertRaises(ValueError):
            object_from_uuid("foo", "Dataset")
