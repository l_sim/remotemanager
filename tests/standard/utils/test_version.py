"""
Unit tests for Version handler
"""

from remotemanager.utils.version import Version


def test_properties():
    """Tests basic properties"""
    version = Version("0.0.0")

    assert version.major == 0
    assert version.minor == 0
    assert version.patch == 0
    assert version.version == "0.0.0"


def test_repr():
    """Checks that we can cast straight to string"""
    assert str(Version("5.5.5")) == "5.5.5"


def test_equal():
    """Tests that two versions can be equated"""
    ref = Version("0.0.0")

    assert ref == Version("0.0.0")
    assert ref != Version("1.0.0")
    assert ref != Version("0.1.0")
    assert ref != Version("0.0.1")
    assert ref != Version("1.1.0")
    assert ref != Version("0.1.1")
    assert ref != Version("1.1.1")


def test_equal_string():
    """Tests that two versions can be equated, using strings"""
    ref = Version("0.0.0")

    assert ref == "0.0.0"
    assert ref != "1.0.0"
    assert ref != "0.1.0"
    assert ref != "0.0.1"
    assert ref != "1.1.0"
    assert ref != "0.1.1"
    assert ref != "1.1.1"


def test_bool_major():
    """Checks that the major version triggers a greater than"""
    ref = Version("5.5.5")

    assert Version("6.5.5") >= ref
    assert Version("6.5.5") > ref

    assert Version("4.5.5") <= ref
    assert Version("4.5.5") < ref


def test_bool_minor():
    """
    Checks that the major version triggers a greater than,
    if major is equal
    """
    ref = Version("5.5.5")

    assert Version("5.6.5") >= ref
    assert Version("5.6.5") > ref

    assert Version("5.4.5") <= ref
    assert Version("5.4.5") < ref


def test_bool_patch():
    """
    Checks that the major version triggers a greater than,
    if major and minor are equal
    """
    ref = Version("5.5.5")

    assert Version("5.5.6") >= ref
    assert Version("5.5.6") > ref

    assert Version("5.5.4") <= ref
    assert Version("5.5.4") < ref
