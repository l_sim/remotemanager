from remotemanager.utils import random_string


def test_nonequal():
    a = random_string()
    b = random_string()

    assert a != b
