import os.path

from remotemanager import get_package_root


def test_get_root():
    step = [os.pardir] * 4
    assert get_package_root() == os.path.normpath(os.path.join(os.path.abspath(__file__), *step))
