from remotemanager.utils.tokenizer import Tokenizer


def test_tokenizer():
    """basic test"""
    assert Tokenizer("a*b").names == ["a", "b"]


def test_nonrepeat():
    """test that names do not repeat"""
    assert Tokenizer("a + a == a").names == ["a"]


def test_numbers():
    """Test that we properly extract numbers, and not as names"""
    tkn = Tokenizer("128")
    assert tkn.numbers == ["128"]
    assert tkn.names == []


def test_hybrid_name_numbers():
    tkn = Tokenizer("a = 256")

    assert tkn.numbers == ["256"]
    assert tkn.names == ["a"]


def test_multiline():
    """check that we can handle a multiline input"""
    inp = """def test(n):
    for i in range(n):
        print(n)
    return n
"""
    assert Tokenizer(inp).names == [
        "def",
        "test",
        "n",
        "for",
        "i",
        "in",
        "range",
        "print",
        "return",
    ]


def test_multiline_reconstruct():
    """Test reconstruction of a multiline function"""
    inp = """def test(n):
    for i in range(n):
        print(n)
    return n
"""

    clean_inp = "\n".join([line for line in inp.split("\n") if line.strip() != ""])
    clean_tknp = "\n".join(
        [line for line in Tokenizer(inp).source.split("\n") if line.strip() != ""]
    )

    assert clean_inp == clean_tknp


def test_exchange():
    inp = "a + a"

    tkn = Tokenizer(inp)

    assert tkn.names == ["a"]

    tkn.exchange_name("a", "b")

    assert tkn.names == ["b"]


def test_non_exchange():

    tkn = Tokenizer("a + b")

    tkn.exchange_name("c", "d")

    assert tkn.names == ["a", "b"]
