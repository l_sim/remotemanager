from remotemanager.utils.version import Version


def test_implicit_patch():
    assert Version("0.11") == "0.11.0"


def test_implicit_minor():
    assert Version("1") == "1.0.0"
