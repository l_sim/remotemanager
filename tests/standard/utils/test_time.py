"""
Tests the global timestamp generation function
"""

import time
from datetime import datetime

from remotemanager.utils.timing import utcnow, local_ts_to_utc


def test_now():
    """Tests that utcnow is actually at the correct offset to now()"""

    offset = round(time.localtime().tm_gmtoff)
    local_ts = int(datetime.now().timestamp())

    n = utcnow()

    assert local_ts == n + offset


def test_ts_utc_convert():
    """Test conversion of a local timestamp to utc"""
    offset = time.localtime().tm_gmtoff

    now = int(time.time())

    assert now - local_ts_to_utc(now) == offset
