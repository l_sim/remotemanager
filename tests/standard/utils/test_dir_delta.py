import os

from remotemanager.utils import dir_delta


def test_lower():
    assert dir_delta(os.getcwd(), "..") == -1


def test_higher():
    assert dir_delta(os.getcwd(), "test") == 1


def test_equal():
    assert dir_delta(os.getcwd(), ".") == 0


def test_nested():
    assert dir_delta(os.getcwd(), "test/inner") == 2


def test_trailing_slash():
    assert dir_delta(os.getcwd(), "test/") == 1


def test_equal_tree():
    base = os.path.join(os.getcwd(), 'a1/b/c')
    test = os.path.join(os.getcwd(), 'a2/b/c')

    assert dir_delta(base, test) == 0


def test_lower_tree():
    base = os.path.join(os.getcwd(), 'a1/b/c')
    test = os.path.join(os.getcwd(), 'a2/b')

    assert dir_delta(base, test) == -1


def test_higher_tree():
    base = os.path.join(os.getcwd(), 'a1/b/c')
    test = os.path.join(os.getcwd(), 'a2/b/c/d')

    assert dir_delta(base, test) == 1
