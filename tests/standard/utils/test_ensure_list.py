import numpy as np
import pytest

from remotemanager.storage import TrackedFile
from remotemanager.utils import ensure_list

file = TrackedFile(".", ".", "test.txt")  # trackedfile test object


test_suite = [
    (None, []),  # none
    ("foo", ["foo"]),  # standard string
    (["foo"], ["foo"]),  # list
    (tuple(["foo"]), ["foo"]),  # tuple
    (np.array([1, 2, 3]), [1, 2, 3]),  # array
    ({"foo"}, ["foo"]),  # set
    (7, [7]),  # int
    (True, [True]),  # bool
    (file, [file]),  # TrackedFile
]

semantic_suite = [
    ("a,b,c", ["a", "b", "c"]),
    ("a, b, c", ["a", "b", "c"]),
    ("a b c", ["a", "b", "c"]),
]


@pytest.mark.parametrize("input,expected", test_suite)
def test_ensure_list(input, expected):
    assert ensure_list(input) == expected


def test_empty():
    assert ensure_list() == []


@pytest.mark.parametrize("input,expected", test_suite + semantic_suite)
def test_ensure_semantic(input, expected):
    assert ensure_list(input, semantic=True) == expected
