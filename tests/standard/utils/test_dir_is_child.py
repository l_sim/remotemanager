import os

from remotemanager.utils import check_dir_is_child


def test_lower():
    assert not check_dir_is_child(os.getcwd(), "..")


def test_higher():
    assert check_dir_is_child(os.getcwd(), "test")


def test_equal():
    assert not check_dir_is_child(os.getcwd(), ".")


def test_nested():
    assert check_dir_is_child(os.getcwd(), "test/inner")


def test_trailing_slash():
    assert check_dir_is_child(os.getcwd(), "test/")


def test_equal_tree():
    base = os.path.join(os.getcwd(), 'a1/b/c')
    test = os.path.join(os.getcwd(), 'a2/b/c')

    assert not check_dir_is_child(base, test)


def test_lower_tree():
    base = os.path.join(os.getcwd(), 'a1/b/c')
    test = os.path.join(os.getcwd(), 'a2/b')

    assert not check_dir_is_child(base, test)


def test_higher_tree():
    base = os.path.join(os.getcwd(), 'a1/b/c')
    test = os.path.join(os.getcwd(), 'a2/b/c/d')

    assert not check_dir_is_child(base, test)
