from remotemanager import __version__
from remotemanager.utils import get_version


def test_get_version():
    assert __version__ == get_version()
