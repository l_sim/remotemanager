import os.path
import shutil

import pytest

from remotemanager.transport import rsync, cp, scp
from remotemanager.utils import random_string


def try_wipe_dir(d):
    try:
        shutil.rmtree(d)
    except FileNotFoundError:
        print(f"directory not found: {d}")


@pytest.mark.parametrize("transport", [rsync, cp, scp])
@pytest.mark.parametrize("nfiles", [1, 5])
@pytest.mark.parametrize("push", [True, False])
def test_simple_transfer(transport, nfiles, push: bool):
    sourcedir = f"transport_test_{random_string()}_src"
    targetdir = f"transport_test_{random_string()}_trg"

    os.makedirs(sourcedir)
    os.makedirs(targetdir)

    obj = transport()

    assert not obj.dir_mode

    for i in range(nfiles):
        file = f"{i}.txt"
        with open(os.path.join(sourcedir, file), mode="w+", encoding="utf8") as o:
            o.write(str(i))

        if push:
            obj.queue_for_push(file, sourcedir, targetdir)
        else:
            obj.queue_for_pull(file, targetdir, sourcedir)

    assert len(obj.transfers) == 1

    obj.transfer()

    for i in range(nfiles):
        assert f"{i}.txt" in os.listdir(targetdir)

    try_wipe_dir(sourcedir)
    try_wipe_dir(targetdir)


@pytest.mark.parametrize("transport", [rsync, cp, scp])
@pytest.mark.parametrize("push", [True, False])
def test_missing_transfer(transport, push: bool):
    sourcedir = f"transport_test_missing_{random_string()}_src"
    targetdir = f"transport_test_missing_{random_string()}_trg"

    os.makedirs(sourcedir)
    os.makedirs(targetdir)

    obj = transport()

    assert not obj.dir_mode

    with open(os.path.join(sourcedir, "1.txt"), mode="w+", encoding="utf8") as o:
        o.write("1")

    if push:
        obj.queue_for_push("1.txt", sourcedir, targetdir)
        obj.queue_for_push("2.txt", sourcedir, targetdir)
    else:
        obj.queue_for_pull("1.txt", targetdir, sourcedir)
        obj.queue_for_pull("2.txt", targetdir, sourcedir)

    assert len(obj.transfers) == 1

    with pytest.raises(RuntimeError):
        obj.transfer()

    assert "1.txt" in os.listdir(targetdir)

    try_wipe_dir(sourcedir)
    try_wipe_dir(targetdir)


@pytest.mark.parametrize("transport", [rsync, cp, scp])
@pytest.mark.parametrize("push", [True, False])
def test_nested_transfer(transport, push: bool):
    sourcedir = f"transport_test_nested_{random_string()}_src"
    targetdir = f"transport_test_nested_{random_string()}_trg"

    os.makedirs(sourcedir)
    os.makedirs(targetdir)

    targetdir_inner = os.path.join(targetdir, "inner")

    obj = transport()

    assert not obj.dir_mode

    with open(os.path.join(sourcedir, "1.txt"), mode="w+", encoding="utf8") as o:
        o.write("1")

    if push:
        obj.queue_for_push("1.txt", sourcedir, targetdir_inner)
    else:
        obj.queue_for_pull("1.txt", targetdir_inner, sourcedir)

    assert len(obj.transfers) == 1

    obj.transfer()

    assert "1.txt" in os.listdir(targetdir_inner)

    try_wipe_dir(sourcedir)
    try_wipe_dir(targetdir)


@pytest.mark.parametrize("transport", [rsync, cp, scp])
@pytest.mark.parametrize("nfiles", [1, 5])
@pytest.mark.parametrize("push", [True, False])
def test_dirmode_transfer(transport, nfiles, push: bool):
    sourcedir = f"transport_test_dirmode_{random_string()}_src"
    targetdir = f"transport_test_dirmode_{random_string()}_trg"

    os.makedirs(sourcedir)
    os.makedirs(targetdir)

    obj = transport(dir_mode=True)

    assert obj.dir_mode

    for i in range(nfiles):
        file = f"{i}.txt"
        with open(os.path.join(sourcedir, file), mode="w+", encoding="utf8") as o:
            o.write(str(i))

        if push:
            obj.queue_for_push(file, sourcedir, targetdir)
        else:
            obj.queue_for_pull(file, targetdir, sourcedir)

    assert len(obj.transfers) == 1

    obj.transfer()

    for i in range(nfiles):
        assert f"{i}.txt" in os.listdir(targetdir)

    try_wipe_dir(sourcedir)
    try_wipe_dir(targetdir)


@pytest.mark.parametrize("transport", [rsync, cp, scp])
@pytest.mark.parametrize("nfiles", [1, 5])
@pytest.mark.parametrize("push", [True, False])
def test_progress_transfer(transport, nfiles, push: bool):
    """
    Hard to check the actual output of the progress flag, so we should just check
    that it works, and doesn't raise issues where it's not functional
    """
    sourcedir = f"transport_test_progress_{random_string()}_src"
    targetdir = f"transport_test_progress_{random_string()}_trg"

    os.makedirs(sourcedir)
    os.makedirs(targetdir)

    obj = transport(progress=True)

    assert not obj.dir_mode

    for i in range(nfiles):
        file = f"{i}.txt"
        with open(os.path.join(sourcedir, file), mode="w+", encoding="utf8") as o:
            o.write(str(i))

        if push:
            obj.queue_for_push(file, sourcedir, targetdir)
        else:
            obj.queue_for_pull(file, targetdir, sourcedir)

    assert len(obj.transfers) == 1

    obj.transfer()

    for i in range(nfiles):
        assert f"{i}.txt" in os.listdir(targetdir)

    try_wipe_dir(sourcedir)
    try_wipe_dir(targetdir)
