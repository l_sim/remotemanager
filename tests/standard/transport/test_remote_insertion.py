from remotemanager.connection import URL
from remotemanager.transport import rsync

url = URL(user="username", host="remote.host")


def test_push_remote():
    t = rsync(url=url)

    t.queue_for_push("tmp_file", ".", "target")

    cmds = t.transfer(dry_run=True)

    assert "username@remote.host:target" in cmds[0]


def test_push_remote_inner():
    t = rsync(url=url)

    t.queue_for_push("tmp_file", ".", "target/inner")

    cmds = t.transfer(dry_run=True)

    assert "username@remote.host:target" in cmds[0]


def test_pull_remote():
    t = rsync(url=url)

    t.queue_for_pull("tmp_file", ".", "target")

    cmds = t.transfer(dry_run=True)

    assert "username@remote.host:target" in cmds[0]


def test_pull_remote_inner():
    t = rsync(url=url)

    t.queue_for_pull("tmp_file", ".", "target/inner")

    cmds = t.transfer(dry_run=True)

    assert "username@remote.host:target" in cmds[0]
