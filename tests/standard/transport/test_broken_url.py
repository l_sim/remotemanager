import pytest

from remotemanager import URL
from remotemanager.transport import rsync


def test_broken_url():
    url = URL("None", timeout=1, max_timeouts=1)

    trn = rsync(url=url)

    trn.queue_for_push("test.txt", ".",".")

    with pytest.raises(RuntimeError):
        trn.transfer()
