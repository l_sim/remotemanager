import pytest

from remotemanager.transport import rsync, scp, cp


@pytest.mark.parametrize("transport", [rsync, scp, cp])
class TestFlagApplication:
    def test_init(self, transport):
        t = transport(flags="aaa")

        assert str(t.flags) == "-aaa"

    def test_modify(self, transport):
        t = transport(flags="")

        t.flags += "bbb"
        assert str(t.flags) == "-bbb"

        t.flags -= "b"
        assert str(t.flags) == "-bb"
