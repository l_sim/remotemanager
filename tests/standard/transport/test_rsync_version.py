import pytest

from remotemanager.connection.cmd import CMD
from remotemanager.transport.rsync import rsync, TRANSPORT_CHANGE_DOCS_URL


def test_warning(capsys):
    t = rsync()

    with pytest.raises(RuntimeError, match="rsync version.*"):
        t.check_version(min_version="4.0.0")

    assert "modified min version" in capsys.readouterr().out


def test_link_valid():
    """This test ensures that the link in the error actually points at a live page"""
    cmd = CMD(f"wget -q {TRANSPORT_CHANGE_DOCS_URL} && rm *.html")
    cmd.exec()

    assert cmd.returncode == 0
