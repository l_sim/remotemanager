import os

import pytest

from remotemanager.transport.transport import Transport


def test_dir_mode_property():
    t = Transport(dir_mode=False)

    assert not t.dir_mode

    t.dir_mode = True

    assert t.dir_mode


def test_raw_add_transfer():
    t = Transport()

    t.add_transfer(["1", "2", "3"], "origin", "target", mode="push")
    assert len(t.transfers) == 1

    t.add_transfer(["4"], "origin", "target", mode="push")
    assert len(t.transfers) == 1

    t.add_transfer(["5"], "diff_origin", "target", mode="push")
    assert len(t.transfers) == 2

    with pytest.raises(NotImplementedError):
        t.transfer()


def test_add_transfer_invalid():
    t = Transport()

    with pytest.raises(ValueError, match="mode must be one of.*"):
        t.add_transfer(["1", "2", "3"], "origin", "target", mode="not_a_mode")


def test_raw_add_no_dirs():
    t = Transport()

    t.add_transfer(["1", "2", "3"], origin=None, target=None, mode="push")
    assert len(t.transfers) == 1

    print(t.transfers)
    journey = list(t.transfers.keys())[0]
    assert journey == f"{os.getcwd()}/>./"
    assert t.transfers[journey] == ["1", "2", "3"]
