from remotemanager.transport import rsync, scp, cp
from tests.utils.base_class import BaseTestClass


def copyto(inpfile, outfile):
    with open(inpfile, mode="r", encoding="utf8") as o:
        data = o.read()

    with open(outfile, mode="w+", encoding="utf8") as o:
        o.write(data)


class TestRun(BaseTestClass):

    def test_rsync(self):

        self.create_dataset(copyto, transport=rsync())

        assert isinstance(self.ds.transport, rsync)

        file = self.create_random_file()

        outfile = f"{self.random_string(8)}.txt"

        self.ds.append_run(
            {"inpfile": file, "outfile": outfile},
            extra_files_send=[file],
            extra_files_recv=[outfile]
        )

        self.ds.runners[0].extra_files_send[0].write("foo")

        self.run_ds()

        assert self.ds.runners[0].extra_files_recv[0].content.strip() == "foo"

    def test_scp(self):

        self.create_dataset(copyto, transport=scp())

        assert isinstance(self.ds.transport, scp)

        file = self.create_random_file()

        outfile = f"{self.random_string(8)}.txt"

        self.ds.append_run(
            {"inpfile": file, "outfile": outfile},
            extra_files_send=[file],
            extra_files_recv=[outfile]
        )

        self.ds.runners[0].extra_files_send[0].write("foo")

        self.run_ds()

        assert self.ds.runners[0].extra_files_recv[0].content.strip() == "foo"

    def test_cp(self):

        self.create_dataset(copyto, transport=cp())

        assert isinstance(self.ds.transport, cp)

        file = self.create_random_file()

        outfile = f"{self.random_string(8)}.txt"

        self.ds.append_run(
            {"inpfile": file, "outfile": outfile},
            extra_files_send=[file],
            extra_files_recv=[outfile]
        )

        self.ds.runners[0].extra_files_send[0].write("foo")

        self.run_ds()

        assert self.ds.runners[0].extra_files_recv[0].content.strip() == "foo"
